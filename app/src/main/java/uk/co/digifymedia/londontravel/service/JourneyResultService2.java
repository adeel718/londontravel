package uk.co.digifymedia.londontravel.service;

import com.squareup.okhttp.Call;
import com.squareup.okhttp.OkHttpClient;

import java.util.List;

import uk.co.digifymedia.londontravel.helpers.CustomOkHttpClient;
import uk.co.digifymedia.swagger.client.ApiCallback;
import uk.co.digifymedia.swagger.client.ApiClient;
import uk.co.digifymedia.swagger.client.ApiException;
import uk.co.digifymedia.swagger.client.DigifyApiClient;
import uk.co.digifymedia.swagger.client.api.DigifyJourneyApi;
import uk.co.digifymedia.swagger.client.api.JourneyApi;
import uk.co.digifymedia.swagger.client.model.DigifyTflApiPresentationEntitiesJourneyPlannerItineraryResult;
import uk.co.digifymedia.swagger.client.model.TflApiPresentationEntitiesJourneyPlannerItineraryResult;
import uk.co.digifymedia.swagger.client.model.TflApiPresentationEntitiesSearchResponse;

public class JourneyResultService2 {
    OkHttpClient okHttpClient = new CustomOkHttpClient();
    private final DigifyJourneyApi api = new DigifyJourneyApi(new DigifyApiClient(okHttpClient));

    public JourneyResultService2(){
    }

    public Call journeyResults(
            String from,
            String to,
            String via,
            Boolean nationalSearch,
            String date,
            String time,
            String timeIs,
            String journeyPreference,
            List<String> mode,
            List<String> accessibilityPreference,
            String fromName,
            String toName,
            String viaName,
            String maxTransferMinutes,
            String maxWalkingMinutes,
            String walkingSpeed,
            String cyclePreference,
            String adjustment,
            List<String> bikeProficiency,
            Boolean alternativeCycle,
            Boolean alternativeWalking,
            Boolean applyHtmlMarkup,
            Boolean useMultiModalCall,
            Boolean walkingOptimization,
            Boolean taxiOnlyTrip,
            ApiCallback<DigifyTflApiPresentationEntitiesJourneyPlannerItineraryResult> callback) throws ApiException {

        Call response =
                api.journeyJourneyResultsAsync(from, to, via, nationalSearch, date, time, timeIs, journeyPreference,
                        mode, accessibilityPreference, fromName, toName, viaName, maxTransferMinutes, maxWalkingMinutes,
                        walkingSpeed, cyclePreference, adjustment, bikeProficiency, alternativeCycle, alternativeWalking,
                        applyHtmlMarkup, useMultiModalCall, walkingOptimization, taxiOnlyTrip, callback);

        return response;
    }
}
