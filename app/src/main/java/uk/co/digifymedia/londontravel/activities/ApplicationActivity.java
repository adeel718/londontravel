package uk.co.digifymedia.londontravel.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.androidstudy.networkmanager.Tovuti;

import uk.co.digifymedia.londontravel.helpers.Internet;

public class ApplicationActivity extends AppCompatActivity {
    // Any base functionality for activity will be here
}
