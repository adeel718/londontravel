package uk.co.digifymedia.londontravel.interfaces;

import android.location.Location;

public interface LocationCallback {
    void execute(Location location);
}

