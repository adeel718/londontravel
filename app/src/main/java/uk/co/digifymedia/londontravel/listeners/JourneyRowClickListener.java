package uk.co.digifymedia.londontravel.listeners;

import android.app.Activity;
import android.content.ContextWrapper;
import android.content.Intent;
import android.view.View;

import com.pixplicity.easyprefs.library.Prefs;

import java.util.List;

import uk.co.digifymedia.londontravel.activities.JourneyPlannerEndActivity;
import uk.co.digifymedia.londontravel.activities.JourneyPlannerStartActivity;
import uk.co.digifymedia.londontravel.activities.JourneyResultsActivity;
import uk.co.digifymedia.londontravel.config.Constants;
import uk.co.digifymedia.londontravel.interfaces.JourneyPopupCallback;
import uk.co.digifymedia.swagger.client.model.TflApiPresentationEntitiesMatchedStop;

public class JourneyRowClickListener implements View.OnClickListener {

    Activity activity;
    TflApiPresentationEntitiesMatchedStop matchedStop;
    private JourneyPopupCallback callback;

    public JourneyRowClickListener(
            Activity activity,
            TflApiPresentationEntitiesMatchedStop matchedStop,
            JourneyPopupCallback callback) {

        this.activity = activity;
        this.matchedStop = matchedStop;
        this.callback = callback;

        // Initialize the Prefs class
        new Prefs.Builder()
                .setContext(activity.getApplicationContext())
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName("uk.co.digifymedia.londontravel")
                .setUseDefaultSharedPreference(true)
                .build();
    }


    @Override
    public void onClick(View view) {
        if(activity instanceof JourneyPlannerStartActivity) {

            List<TflApiPresentationEntitiesMatchedStop> existing =
                    TflApiPresentationEntitiesMatchedStop.find(
                            TflApiPresentationEntitiesMatchedStop.class,
                            "SAVE_TYPE = ? and MID = ?",
                            Constants.JOURNEY_SAVE_TYPE.RECENT.toString(), matchedStop.getMid());

            if (existing.size() > 0) {
                // TODO already exists

            } else {
                matchedStop.setSaveType(Constants.JOURNEY_SAVE_TYPE.RECENT.toString());
                matchedStop.save();
                callback.execute();
            }

            Intent intent = new Intent(activity, JourneyPlannerEndActivity.class);
            intent.putExtra("start-station-id", matchedStop.getIcsId());
            intent.putExtra("start-station-name", matchedStop.getName());

            Prefs.putString("start-station-id", matchedStop.getIcsId());
            Prefs.putString("start-station-name", matchedStop.getName());

            //activity.finish();
            activity.startActivity(intent);
        }
        else if(activity instanceof JourneyPlannerEndActivity) {

            List<TflApiPresentationEntitiesMatchedStop> existing =
                    TflApiPresentationEntitiesMatchedStop.find(
                            TflApiPresentationEntitiesMatchedStop.class,
                            "SAVE_TYPE = ? and MID = ?",
                            Constants.JOURNEY_SAVE_TYPE.RECENT.toString(), matchedStop.getMid());

            if (existing.size() > 0) {
                // TODO already exists

            } else {
                matchedStop.setSaveType(Constants.JOURNEY_SAVE_TYPE.RECENT.toString());
                matchedStop.save();
                callback.execute();
            }

            Intent intent = new Intent(activity, JourneyResultsActivity.class);
            intent.putExtra("end-station-id", matchedStop.getIcsId());
            intent.putExtra("end-station-name", matchedStop.getName());

            Prefs.putString("end-station-id", matchedStop.getIcsId());
            Prefs.putString("end-station-name", matchedStop.getName());

            //activity.finish();
            activity.startActivity(intent);
        }

    }
}
