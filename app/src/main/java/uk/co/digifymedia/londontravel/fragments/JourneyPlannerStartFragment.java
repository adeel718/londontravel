package uk.co.digifymedia.londontravel.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import uk.co.digifymedia.R;
import uk.co.digifymedia.londontravel.listeners.JourneySearchTextViewListener;

/**
 * Fragment for journey search {@link Fragment} subclass.
 */
public class JourneyPlannerStartFragment extends Fragment {
    SearchView editTextSearch;
    ListView listviewStations;

    public JourneyPlannerStartFragment() {
        // Required empty public constructor

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_journey_planner_start, container, false);

        bindViews(view);

        //third, we must add the textWatcher to our EditText
        SearchView.OnQueryTextListener textWatcher = new JourneySearchTextViewListener(
                editTextSearch,
                listviewStations,
                (TextView)view.findViewById(R.id.textViewHeadStations),
                (AppCompatActivity) getActivity(),
                this
        );
        editTextSearch.setOnQueryTextListener(textWatcher);

        // Inflate the layout for this fragment
        return view;
    }

    private void bindViews(View view) {
        editTextSearch = getActivity().findViewById(R.id.editTextStartJourney);
        listviewStations = (ListView) view.findViewById(R.id.listViewSearchJourneyResults);

    }

}
