package uk.co.digifymedia.londontravel.service;

import com.squareup.okhttp.Call;

import java.util.List;

import uk.co.digifymedia.londontravel.config.Constants;
import uk.co.digifymedia.swagger.client.ApiCallback;
import uk.co.digifymedia.swagger.client.ApiClient;
import uk.co.digifymedia.swagger.client.ApiException;
import uk.co.digifymedia.swagger.client.api.StopPointApi;
import uk.co.digifymedia.swagger.client.model.TflApiPresentationEntitiesSearchResponse;

public class StopPointService {
    private final StopPointApi api = new StopPointApi(new ApiClient());

    public StopPointService(){
    }

    public Call stopPointSearch(List<String> listModes, String searchQuery, ApiCallback<TflApiPresentationEntitiesSearchResponse> callback) throws ApiException {
        Call response =
                api.stopPointSearch_0Async(
                        searchQuery,
                        listModes,
                        false,
                        Constants.SEARCH_RESULT_MAX,
                        null,
                        true,
                        true,
                        callback
                        );

        return response;
    }
}
