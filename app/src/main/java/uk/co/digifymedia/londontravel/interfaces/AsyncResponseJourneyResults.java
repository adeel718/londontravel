package uk.co.digifymedia.londontravel.interfaces;

import uk.co.digifymedia.swagger.client.model.DigifyTflApiPresentationEntitiesJourneyPlannerItineraryResult;
import uk.co.digifymedia.swagger.client.model.Mode;
import uk.co.digifymedia.swagger.client.model.TflApiPresentationEntitiesJourneyPlannerItineraryResult;
import uk.co.digifymedia.swagger.client.model.TflApiPresentationEntitiesSearchResponse;

public interface AsyncResponseJourneyResults {
    void onAsyncFinish(DigifyTflApiPresentationEntitiesJourneyPlannerItineraryResult result, int statusCode);
    /*void onAsyncFinishBus(DigifyTflApiPresentationEntitiesJourneyPlannerItineraryResult result, int statusCode);
    void onAsyncFinishTube(DigifyTflApiPresentationEntitiesJourneyPlannerItineraryResult result, int statusCode);*/

}