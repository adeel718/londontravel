package uk.co.digifymedia.londontravel.adaptors;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.SphericalUtil;
import com.pixplicity.easyprefs.library.Prefs;
import com.yayandroid.locationmanager.configuration.DefaultProviderConfiguration;
import com.yayandroid.locationmanager.configuration.GooglePlayServicesConfiguration;
import com.yayandroid.locationmanager.configuration.LocationConfiguration;
import com.yayandroid.locationmanager.configuration.PermissionConfiguration;

import org.apache.commons.math3.util.Precision;

import java.util.List;

import de.mateware.snacky.Snacky;
import uk.co.digifymedia.R;
import uk.co.digifymedia.londontravel.activities.JourneyPlannerEndActivity;
import uk.co.digifymedia.londontravel.activities.JourneyPlannerStartActivity;
import uk.co.digifymedia.londontravel.activities.JourneyResultsActivity;
import uk.co.digifymedia.londontravel.config.Constants;
import uk.co.digifymedia.londontravel.interfaces.JourneyPopupCallback;
import uk.co.digifymedia.londontravel.listeners.JourneyRowClickListener;
import uk.co.digifymedia.swagger.client.model.TflApiPresentationEntitiesMatchedStop;

public class JourneySearchAdaptor extends ArrayAdapter<TflApiPresentationEntitiesMatchedStop> {

    private List<TflApiPresentationEntitiesMatchedStop> list;
    private Context context;
    private Activity activity;
    private Fragment fragment;
    private JourneyPopupCallback callback;

    private double lat;
    private double lon;

    public JourneySearchAdaptor(
            Activity activity,
            Fragment fragment,
            int textViewResourceId,
            List<TflApiPresentationEntitiesMatchedStop> list,
            JourneyPopupCallback callback,
            double lat,
            double lon

    ) {

        super(activity, textViewResourceId, list);
        this.context = activity.getApplicationContext();
        this.activity = activity;
        this.fragment = fragment;
        this.list = list;
        this.callback = callback;
        this.lat = lat;
        this.lon = lon;
    }

    // View lookup cache
    private static class ViewHolder {
        TextView txtName;

        ImageView imageViewBusIcon;
        ImageView imageViewCableCarIcon;
        ImageView imageViewCoachIcon;
        ImageView imageViewCycleHireIcon;
        ImageView imageViewDLRIcon;
        ImageView imageViewNationalRailIcon;
        ImageView imageViewOvergorundIcon;
        ImageView imageViewRiverIcon;
        ImageView imageViewTaxiIcon;
        ImageView imageViewTramIcon;
        ImageView imageViewUndergroundIcon;

        TextView textViewDistance;

        ImageButton imageButtonAction;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the item for this position
        final TflApiPresentationEntitiesMatchedStop matchedStop = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        final ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.list_item_journey_search_1, parent, false);

            // Transport icons, hidden by default in XML
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.textViewTitle);
            viewHolder.imageViewBusIcon = (ImageView) convertView.findViewById(R.id.imageViewBusIcon);
            viewHolder.imageViewCableCarIcon = (ImageView) convertView.findViewById(R.id.imageViewCableCarIcon);
            viewHolder.imageViewCoachIcon = (ImageView) convertView.findViewById(R.id.imageViewCoachIcon);
            viewHolder.imageViewCycleHireIcon = (ImageView) convertView.findViewById(R.id.imageViewCycleHireIcon);
            viewHolder.imageViewDLRIcon = (ImageView) convertView.findViewById(R.id.imageViewDLRIcon);
            viewHolder.imageViewNationalRailIcon = (ImageView) convertView.findViewById(R.id.imageViewNationalRailIcon);
            viewHolder.imageViewOvergorundIcon = (ImageView) convertView.findViewById(R.id.imageViewOvergorundIcon);
            viewHolder.imageViewRiverIcon = (ImageView) convertView.findViewById(R.id.imageViewRiverIcon);
            viewHolder.imageViewTaxiIcon = (ImageView) convertView.findViewById(R.id.imageViewTaxiIcon);
            viewHolder.imageViewTramIcon = (ImageView) convertView.findViewById(R.id.imageViewTramIcon);
            viewHolder.imageViewUndergroundIcon = (ImageView) convertView.findViewById(R.id.imageViewUndergroundIcon);

            viewHolder.textViewDistance = (TextView) convertView.findViewById(R.id.textViewDistance);

            viewHolder.imageButtonAction = (ImageButton) convertView.findViewById(R.id.actionIcon) ;

            // Action click listener
            viewHolder.imageButtonAction.setOnClickListener(actionOnClickListener(matchedStop));

            convertView.setOnClickListener(new JourneyRowClickListener(activity, matchedStop, callback));

            result = convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result = convertView;
        }

        viewHolder.txtName.setText(matchedStop.getName());

        // TODO refactoring
        // LocationManager MUST be initialized with Application context in order to prevent MemoryLeaks

        if(lat != 0) {
            double dist = Precision.round(getMiles(
                    distanceBetween(
                            lat,
                            lon,
                            matchedStop.getLat(),
                            matchedStop.getLon()
                    )), 1);
            viewHolder.textViewDistance.setText(String.valueOf(dist) + " mi");
        }
        ImageView imageViewTaxiIcon;

        if(matchedStop.getModes() != null) {
            for (String mode : matchedStop.getModes()) {
                try {
                    Constants.TRANSPORT_MODE modeEnum = Constants.TRANSPORT_MODE.of(mode);
                    switch (modeEnum) {
                        case BUS:
                            viewHolder.imageViewBusIcon.setVisibility(View.VISIBLE);
                            break;

                        case TUBE:
                            viewHolder.imageViewUndergroundIcon.setVisibility(View.VISIBLE);
                            break;
                        case NATIONAL_RAIL:
                            viewHolder.imageViewNationalRailIcon.setVisibility(View.VISIBLE);
                            break;
                        case DLR:
                            viewHolder.imageViewDLRIcon.setVisibility(View.VISIBLE);
                            break;
                        case OVERGROUND:
                            viewHolder.imageViewOvergorundIcon.setVisibility(View.VISIBLE);
                            break;
                        case TFLRAIL:
                            // TODO tfl rail icon
                            viewHolder.imageViewUndergroundIcon.setVisibility(View.VISIBLE);
                            break;
                        case RIVER_BUS:
                            viewHolder.imageViewRiverIcon.setVisibility(View.VISIBLE);
                            break;
                        case TRAM:
                            viewHolder.imageViewTramIcon.setVisibility(View.VISIBLE);
                            break;
                        case CABLE_CAR:
                            viewHolder.imageViewCableCarIcon.setVisibility(View.VISIBLE);
                            break;
                        case COACH:
                            viewHolder.imageViewCoachIcon.setVisibility(View.VISIBLE);
                            break;

                        // You can have any number of case statements.
                        default: // Optional
                            Log.d("Journey", "Unknown mode found when setting image: " + mode);
                    }
                }
                catch (Exception e){
                    Log.e("Journey", "Exception when setting transport icons: " + e.getMessage());
                }
            }
        }

        // Return the completed view to render on screen
        return convertView;
    }

    private View.OnClickListener actionOnClickListener(final TflApiPresentationEntitiesMatchedStop matchedStop) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopup(view, matchedStop);
            }
        };
    }

    public void showPopup(View v, final TflApiPresentationEntitiesMatchedStop matchedStop) {
        PopupMenu popup = new PopupMenu(getContext(), v);
        popUpInitSearch(matchedStop, popup);
    }

    private void showSnackBar(String string) {
        Snacky.builder()
                .setActivity(activity)
                .setText(string)
                .setDuration(Snacky.LENGTH_LONG)
                .setActionText(android.R.string.ok)
                .success()
                .show();
    }

    private void showSnackBarInfo(String string) {
        Snacky.builder()
                .setActivity(activity)
                .setText(string)
                .setDuration(Snacky.LENGTH_LONG)
                .setActionText(android.R.string.ok)
                .info()
                .show();
    }

    private void popUpInitSearch(final TflApiPresentationEntitiesMatchedStop matchedStop, PopupMenu popup) {
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.save:
                        List<TflApiPresentationEntitiesMatchedStop> existing =
                                TflApiPresentationEntitiesMatchedStop.find(
                                        TflApiPresentationEntitiesMatchedStop.class,
                                        "SAVE_TYPE = ? and MID = ?",
                                        Constants.JOURNEY_SAVE_TYPE.SAVED.toString(), matchedStop.getMid());

                        if(existing.size() > 0) {
                            showSnackBarInfo("Station/ Stop already exists in your saved items.");
                        }
                        else {
                            matchedStop.setSaveType(Constants.JOURNEY_SAVE_TYPE.SAVED.toString());
                            matchedStop.save();
                            callback.execute();
                            showSnackBar("Added to saved items successfully.");
                        }

                        return true;

                    default:
                        return false;
                }
            }
        });
        popup.inflate(R.menu.menu_journey_station_unsaved_2);
        popup.show();
    }

    private double distanceBetween(double lat1, double lon1, double lat2, double lon2) {
        LatLng from = new LatLng(lat1, lon1);
        LatLng to = new LatLng(lat2, lon2);
        return SphericalUtil.computeDistanceBetween(from, to);
    }

    /**
     * Returns a LocationConfiguration which tights to default definitions with given messages. Since this method is
     * basically created in order to be used in Activities, User needs to be asked for permission and enabling gps.
     */
    public static LocationConfiguration defaultConfiguration(@NonNull String rationalMessage, @NonNull String gpsMessage) {
        return new LocationConfiguration.Builder()
                .askForPermission(new PermissionConfiguration.Builder().rationaleMessage(rationalMessage).build())
                .useGooglePlayServices(new GooglePlayServicesConfiguration.Builder().build())
                .useDefaultProviders(new DefaultProviderConfiguration.Builder().gpsMessage(gpsMessage).build())
                .build();
    }

    public double getMiles(double i) {
        return i*0.000621371192;
    }
    public double getMeters(double i) {
        return i*1609.344;
    }

}
