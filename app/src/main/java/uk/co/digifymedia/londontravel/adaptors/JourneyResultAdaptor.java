package uk.co.digifymedia.londontravel.adaptors;

import android.app.Activity;
import android.support.constraint.ConstraintLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.threeten.bp.Duration;
import org.threeten.bp.ZonedDateTime;
import org.threeten.bp.format.DateTimeFormatter;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import uk.co.digifymedia.R;
import uk.co.digifymedia.londontravel.config.Constants;
import uk.co.digifymedia.swagger.client.model.TflApiPresentationEntitiesJourneyPlannerJourney;
import uk.co.digifymedia.swagger.client.model.TflApiPresentationEntitiesJourneyPlannerLeg;
import uk.co.digifymedia.swagger.client.model.TflApiPresentationEntitiesJourneyPlannerRouteOption;

public class JourneyResultAdaptor extends ArrayAdapter<TflApiPresentationEntitiesJourneyPlannerJourney> {

    public static final DateTimeFormatter ISO_LOCAL_TIME = DateTimeFormatter.ofPattern("HH:mm");
    int spinnerTimesPosition;

    public JourneyResultAdaptor(
            Activity activity,
            int textViewResourceId,
            List<TflApiPresentationEntitiesJourneyPlannerJourney> list,

            int spinnerTimesPosition) {

        super(activity, textViewResourceId, list);
        this.spinnerTimesPosition = spinnerTimesPosition;
    }

    // View lookup cache
    private static class ViewHolder {
        TextView txtMinSchedule;
        TextView txtStartTime;
        TextView txtFareTotal;
        TextView txtTimeTotal;
        TextView txtFromStationName;
        ConstraintLayout layoutTimeinfo;
        TextView txtViewArriveOrDepart;
        TextView txtViewDepArrTime;

        ViewGroup linearLayoutModeImages;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the item for this position
        final TflApiPresentationEntitiesJourneyPlannerJourney matchedResult = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        final ViewHolder viewHolder; // view lookup cache stored in tag


        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.list_item_journey_result_1, parent, false);
            viewHolder.layoutTimeinfo = (ConstraintLayout) convertView.findViewById(R.id.layoutTimeInfo);
            viewHolder.txtViewArriveOrDepart = convertView.findViewById(R.id.textViewArriveOrDepart);
            viewHolder.txtViewDepArrTime = convertView.findViewById(R.id.textViewDepArrTime);
            viewHolder.linearLayoutModeImages = (ViewGroup) convertView.findViewById(R.id.linearLayoutModeImages);

            viewHolder.txtStartTime = (TextView) convertView.findViewById(R.id.textViewStartTime);
            viewHolder.txtMinSchedule = (TextView) convertView.findViewById(R.id.textViewMinSchedule);
            viewHolder.txtFareTotal = (TextView) convertView.findViewById(R.id.textViewTotalFare);
            viewHolder.txtTimeTotal = (TextView) convertView.findViewById(R.id.textViewTotalTime);
            viewHolder.txtFromStationName = (TextView) convertView.findViewById(R.id.textViewFromStation);

            StringBuffer stringBufferTimeTotal = new StringBuffer();

            Duration diff = Duration.between(ZonedDateTime.now(), matchedResult.getStartDateTime());
            stringBufferTimeTotal.append(diff.toMinutes() + " min");

            viewHolder.txtMinSchedule.setText(stringBufferTimeTotal);
            viewHolder.txtStartTime.setText(matchedResult.getStartDateTime().format(ISO_LOCAL_TIME));
            NumberFormat n = NumberFormat.getCurrencyInstance(Locale.UK);

            viewHolder.txtStartTime.setTag(matchedResult.getStartDateTime());

            if(matchedResult.getFare() != null) {
                viewHolder.txtFareTotal.setText(n.format(matchedResult.getFare().getTotalCost() / 100.0));
            }

            viewHolder.txtTimeTotal.setText(String.valueOf(matchedResult.getDuration()));

            if((matchedResult.getLegs().size() > 0) &&
                    matchedResult.getLegs().get(0).getDeparturePoint() != null) {
                viewHolder.txtFromStationName.setText(matchedResult.getLegs().get(0).getDeparturePoint().getCommonName());
            }

            ViewGroup view = viewHolder.linearLayoutModeImages;

            int legCount = 0;
            for(TflApiPresentationEntitiesJourneyPlannerLeg leg: matchedResult.getLegs()) {
                if(leg.getMode() != null) {
                    TextView textViewBusNos;
                    boolean lastOneWasBus = false;
                    try {
                        Constants.TRANSPORT_MODE modeEnum = Constants.TRANSPORT_MODE.of(leg.getMode().getName());
                        switch (modeEnum) {
                            case BUS:
                                for(TflApiPresentationEntitiesJourneyPlannerRouteOption routeOptions: leg.getRouteOptions()) {
                                    View view1 = null;
                                    // Check if a new view needs to be added for bus
                                    if (!lastOneWasBus) {
                                        view1 = inflater.inflate(R.layout.bus_item, null);
                                        textViewBusNos = (TextView) view1.findViewById(R.id.textViewBusNumbers);
                                        textViewBusNos.setText("");
                                        textViewBusNos.append(routeOptions.getName());
                                        view.addView(view1);
                                    }
                                    // Use existing view and append bus number to existing
                                    else {
                                        view1 = viewHolder.linearLayoutModeImages;
                                        textViewBusNos = (TextView) view1.findViewById(R.id.textViewBusNumbers);
                                        textViewBusNos.append(" / " + routeOptions.getName());
                                        //view.addView(view1);
                                    }

                                    lastOneWasBus = true;
                                }

                                break;

                            case TUBE:
                                lastOneWasBus = false;
                                View view2 = inflater.inflate(R.layout.tube_item, view, false);
                                for(TflApiPresentationEntitiesJourneyPlannerRouteOption routeOptions: leg.getRouteOptions()) {
                                    switch(routeOptions.getName().toLowerCase()) {
                                        case "bakerloo":
                                            view2.setBackgroundResource(R.color.und_bakerloo);
                                            break;
                                        case "central":
                                            view2.setBackgroundResource(R.color.und_central);
                                            break;
                                        case "circle":
                                            view2.setBackgroundResource(R.color.und_circle	);
                                            break;
                                        case "district":
                                            view2.setBackgroundResource(R.color.und_district);
                                            break;
                                        case "hammersmith":
                                            view2.setBackgroundResource(R.color.und_hammersmithcity);
                                            break;
                                        case "jubilee":
                                            view2.setBackgroundResource(R.color.und_jubilee);
                                            break;
                                        case "metropolitan":
                                            view2.setBackgroundResource(R.color.und_metropolitan);
                                            break;
                                        case "northern":
                                            view2.setBackgroundResource(R.color.und_northern);
                                            break;
                                        case "piccadilly":
                                            view2.setBackgroundResource(R.color.und_piccadilly);
                                            break;
                                        case "victoria":
                                            view2.setBackgroundResource(R.color.und_victoria);
                                            break;
                                        case "waterloo":
                                            view2.setBackgroundResource(R.color.und_waterloocity);
                                            break;
                                        default: // Optional
                                            Log.d("Journey", "Unknown line found when setting image: " + routeOptions.getLineIdentifier().getId());
                                    }
                                }
                                //view2.setBackgroundResource(R.color.colorLighter);

                                view.addView(view2);
                                break;
                            case NATIONAL_RAIL:
                                lastOneWasBus = false;
                                View view3 = inflater.inflate(R.layout.national_rail_item, view, false);
                                view.addView(view3);
                                break;
                            case DLR:
                                lastOneWasBus = false;
                                View view4 = inflater.inflate(R.layout.dlr_item, view, false);
                                view.addView(view4);
                                break;
                            case OVERGROUND:
                                lastOneWasBus = false;
                                View view5 = inflater.inflate(R.layout.overground_item, view, false);
                                view.addView(view5);
                                break;
                            case TFLRAIL:
                                lastOneWasBus = false;
                                // TODO tfl rail icon
                                View view6 = inflater.inflate(R.layout.tfl_rail_item, view, false);
                                view.addView(view6);
                                break;
                            case RIVER_BUS:
                                lastOneWasBus = false;
                                View view7 = inflater.inflate(R.layout.river_bus_item, view, false);
                                view.addView(view7);
                                break;
                            case TRAM:
                                lastOneWasBus = false;
                                View view8 = inflater.inflate(R.layout.tram_item, view, false);
                                view.addView(view8);
                                break;
                            case CABLE_CAR:
                                lastOneWasBus = false;
                                View view9 = inflater.inflate(R.layout.cable_car_item, view, false);
                                view.addView(view9);
                                break;
                            case COACH:
                                lastOneWasBus = false;
                                View view10 = inflater.inflate(R.layout.coach_item, view, false);
                                view.addView(view10);
                                break;
                            case WALKING:
                                lastOneWasBus = false;
                                View view11 = inflater.inflate(R.layout.walking_item, view, false);
                                view.addView(view11);
                                break;
                            case REPLACEMENT_BUS:
                                lastOneWasBus = false;
                                View view12 = inflater.inflate(R.layout.replacement_bus_item, view, false);
                                view.addView(view12);
                                break;

                            // You can have any number of case statements.
                            default: // Optional
                                lastOneWasBus = false;
                                Log.d("Journey", "Unknown mode found when setting image: " + leg.getMode().getName());
                        }
                    }

                    catch (Exception e){
                        //throw new RuntimeException(e);
                        Log.e("Journey", "Exception when setting transport icons: " + e.getMessage());
                    }

                    if((legCount+1) != matchedResult.getLegs().size()) {
                        View view12 = inflater.inflate(R.layout.seperator_circle, view, false);
                        view.addView(view12);
                    }
                }

                legCount++;
            }

            convertView.setTag(viewHolder);

            if(spinnerTimesPosition > 0) {
                viewHolder.layoutTimeinfo.setVisibility(View.VISIBLE);


                if(spinnerTimesPosition == 1) {
                    viewHolder.txtViewArriveOrDepart.setText("Arrive");
                    viewHolder.txtViewDepArrTime.setText(matchedResult.getArrivalDateTime().format(ISO_LOCAL_TIME));

                    if(ZonedDateTime.now().isBefore(matchedResult.getArrivalDateTime().toZonedDateTime())) {
                        viewHolder.txtViewArriveOrDepart.setTextColor(getContext().getResources().getColor(R.color.green));
                        viewHolder.txtViewDepArrTime.setTextColor(getContext().getResources().getColor(R.color.green));
                    }
                    else {
                        viewHolder.txtViewArriveOrDepart.setTextColor(getContext().getResources().getColor(R.color.red));
                        viewHolder.txtViewDepArrTime.setTextColor(getContext().getResources().getColor(R.color.red));
                    }
                }
                if(spinnerTimesPosition == 2) {
                    viewHolder.txtViewArriveOrDepart.setText("Depart");
                    viewHolder.txtViewDepArrTime.setText(matchedResult.getStartDateTime().format(ISO_LOCAL_TIME));

                    if(ZonedDateTime.now().isBefore(matchedResult.getStartDateTime().toZonedDateTime())) {
                        viewHolder.txtViewArriveOrDepart.setTextColor(getContext().getResources().getColor(R.color.green));
                        viewHolder.txtViewDepArrTime.setTextColor(getContext().getResources().getColor(R.color.green));
                    }
                    else {
                        viewHolder.txtViewArriveOrDepart.setTextColor(getContext().getResources().getColor(R.color.red));
                        viewHolder.txtViewDepArrTime.setTextColor(getContext().getResources().getColor(R.color.red));
                    }
                }
            }
        } else {
        }


        // Return the completed view to render on screen
        return convertView;
    }



}
