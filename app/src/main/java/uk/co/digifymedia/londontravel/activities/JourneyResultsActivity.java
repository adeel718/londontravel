package uk.co.digifymedia.londontravel.activities;

import android.content.ContextWrapper;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment;
import com.pixplicity.easyprefs.library.Prefs;

import java.text.SimpleDateFormat;
import java.util.Date;

import uk.co.digifymedia.R;
import uk.co.digifymedia.londontravel.fragments.JourneyResultFragment;
import uk.co.digifymedia.londontravel.helpers.FragmentHelper;

public class JourneyResultsActivity extends ApplicationActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_journey_results);

        // Initialize the Prefs class
        new Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName("uk.co.digifymedia.londontravel")
                .setUseDefaultSharedPreference(true)
                .build();

        Spinner spinner = (Spinner) findViewById(R.id.spinnerTimes);

        TextView t1 = findViewById(R.id.editTextStartJourney);
        TextView t2 = findViewById(R.id.editTextToJourney);
        t1.setText(Prefs.getString("start-station-name", ""));
        t2.setText(Prefs.getString("end-station-name", ""));

        // Set up navigation listeners
        ImageView imageViewNavBack = findViewById(R.id.navigationIconBack);
        imageViewNavBack.setOnClickListener(navigationBackListener());

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if(position == 0) {
                    Prefs.remove("arrival-time");
                    Prefs.remove("arrival-date");
                    Prefs.remove("depart-time");
                    Prefs.remove("depart-date");
                }
                if(position == 1) {
                    timeDialog("Set Arrival Time", true);
                }
                else if(position == 2) {
                    timeDialog("Set Departure Time", false);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });


        FragmentHelper.switchFragmentReplace(getSupportFragmentManager(), new JourneyResultFragment());
    }

    private void timeDialog(String label, final boolean arrival) {

        // Initialize
        SwitchDateTimeDialogFragment dateTimeDialogFragment = SwitchDateTimeDialogFragment.newInstance(
                label,
                "OK",
                "Cancel"
        );

        dateTimeDialogFragment.startAtTimeView();
        dateTimeDialogFragment.set24HoursMode(true);

        dateTimeDialogFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonClickListener() {
            @Override
            public void onPositiveButtonClick(Date date) {
                // Date is get on positive button click
                // Do something
                SimpleDateFormat sdfTime = new SimpleDateFormat("HHmm");
                SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMMdd");

                Prefs.remove("arrival-time");
                Prefs.remove("arrival-date");
                Prefs.remove("depart-time");
                Prefs.remove("depart-date");

                if(arrival) {
                    Prefs.putString("arrival-time", sdfTime.format(date));
                    Prefs.putString("arrival-date", sdfDate.format(date));
                }
                else {
                    Prefs.putString("depart-time", sdfTime.format(date));
                    Prefs.putString("depart-date", sdfDate.format(date));
                }


                FragmentHelper.switchFragmentReplace(getSupportFragmentManager(), new JourneyResultFragment());
            }

            @Override
            public void onNegativeButtonClick(Date date) {
                // Date is get on negative button click
            }
        });

        dateTimeDialogFragment.show(getSupportFragmentManager(), "dialog_time");
    }

    @NonNull
    private View.OnClickListener navigationBackListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        };
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
