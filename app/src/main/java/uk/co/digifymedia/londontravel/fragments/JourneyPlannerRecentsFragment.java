package uk.co.digifymedia.londontravel.fragments;


import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListView;

import com.google.common.collect.Lists;
import com.nhaarman.listviewanimations.appearance.simple.ScaleInAnimationAdapter;

import java.util.List;

import uk.co.digifymedia.R;
import uk.co.digifymedia.londontravel.adaptors.JourneyRecentAdaptor;
import uk.co.digifymedia.londontravel.config.Constants;
import uk.co.digifymedia.londontravel.helpers.LocationHelper;
import uk.co.digifymedia.londontravel.interfaces.JourneyPopupCallback;
import uk.co.digifymedia.londontravel.interfaces.LocationCallback;
import uk.co.digifymedia.londontravel.listeners.JourneyRecentsSavedTextViewListener;
import uk.co.digifymedia.swagger.client.model.TflApiPresentationEntitiesMatchedStop;

/**
 * Fragment for Journey start recent {@link Fragment} subclass.
 */
public class JourneyPlannerRecentsFragment extends Fragment implements LocationCallback {
    SearchView editTextSearch;
    ListView listviewStations;
    JourneyRecentAdaptor adapter;
    private double lat;
    private double lon;

    public JourneyPlannerRecentsFragment() {
        // Required empty public constructor

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_journey_planner_recents, container, false);

        bindViews(view);

        LocationHelper locationHelper = new LocationHelper();
        locationHelper.callback = this;
        locationHelper.getLocation(getActivity(), getActivity(), this);

        JourneyPopupCallback myCallback = getMyCallback();
        init(myCallback);

        editTextSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean focused) {
                InputMethodManager keyboard = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                //if (focused)
                    //keyboard.showSoftInput(editTextSearch, 0);
                //else
                    //keyboard.hideSoftInputFromWindow(editTextSearch.getWindowToken(), 0);
            }
        });

        // Inflate the layout for this fragment
        return view;
    }

    @NonNull
    private JourneyPopupCallback getMyCallback() {
        return new JourneyPopupCallback() {
            @Override
            public void execute() {
                init(this);
            }
        };
    }

    private void init(JourneyPopupCallback myCallback) {
        editTextSearch.clearFocus();

        final List<TflApiPresentationEntitiesMatchedStop> list = Lists.newArrayList(
                TflApiPresentationEntitiesMatchedStop.find(TflApiPresentationEntitiesMatchedStop.class,
                        "SAVE_TYPE = ?", Constants.JOURNEY_SAVE_TYPE.RECENT.toString()));

        adapter = new JourneyRecentAdaptor(
                getActivity(),
                this,
                R.layout.list_item_journey_search_1,
                list,
                myCallback,
                lat,
                lon
                );

        setListViewStations(adapter);

        //third, we must add the textWatcher to our EditText
        editTextSearch.setOnQueryTextListener(new JourneyRecentsSavedTextViewListener(getFragmentManager()));
    }

    private void bindViews(View view) {
        editTextSearch = getActivity().findViewById(R.id.editTextStartJourney);
        listviewStations = (ListView) view.findViewById(R.id.listViewStartJourneyResults);
    }

    // TODO moved to helper class used in multiple locations
    private void setListViewStations(final JourneyRecentAdaptor adapter) {
        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                listviewStations.requestFocus();
                ScaleInAnimationAdapter animationAdapter = new ScaleInAnimationAdapter(adapter);
                animationAdapter.setAbsListView(listviewStations);
                listviewStations.setAdapter(animationAdapter);
            }
        });
    }

    @Override
    public void execute(Location location) {
        lat = location.getLatitude();
        lon = location.getLongitude();

        JourneyPopupCallback myCallback = getMyCallback();
        init(myCallback);

        adapter.notifyDataSetChanged();
    }
}
