package uk.co.digifymedia.londontravel.config;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Constants {
    public static int SEARCH_RESULT_MAX = 25;

    public enum TRANSPORT_MODE{
        BUS("bus"),
        TUBE("tube"),
        NATIONAL_RAIL("national-rail"),
        DLR("dlr"),
        OVERGROUND("overground"),
        TFLRAIL("tflrail"),
        RIVER_BUS("river-bus"),
        TRAM("tram"),
        CABLE_CAR("cable-car"),
        COACH("coach"),
        WALKING("walking"),
        REPLACEMENT_BUS("replacement-bus");

        private static final Map<String, TRANSPORT_MODE> map = new HashMap<>(values().length, 1);

        static {
            for (TRANSPORT_MODE c : values()) map.put(c.realValue, c);
        }

        TRANSPORT_MODE(String value) {
            this.realValue = value;
        }
        public String getValue() {
            return realValue;
        }

        public static TRANSPORT_MODE of(String name) {
            TRANSPORT_MODE result = map.get(name);
            if (result == null) {
                throw new IllegalArgumentException("Invalid TRANSPORT_MODE name: " + name);
            }
            return result;
        }

        private final String realValue;
    }

    public enum JOURNEY_SAVE_TYPE{
        RECENT,
        SAVED
    }

    public enum JOURNEY_FROM_FRAGMENT{
        SEARCH,
        RECENT,
        SAVED
    }

}
