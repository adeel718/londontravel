package uk.co.digifymedia.londontravel.fragments;

import android.content.ContextWrapper;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.pixplicity.easyprefs.library.Prefs;

import org.threeten.bp.Duration;
import org.threeten.bp.ZonedDateTime;
import org.threeten.bp.temporal.Temporal;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import uk.co.digifymedia.R;
import uk.co.digifymedia.londontravel.adaptors.JourneyResultAdaptor;
import uk.co.digifymedia.londontravel.callbacks.CallbackJourneyResults;
import uk.co.digifymedia.londontravel.classes.BataTime;
import uk.co.digifymedia.londontravel.classes.BataTimeCallback;
import uk.co.digifymedia.londontravel.config.Constants;
import uk.co.digifymedia.londontravel.helpers.FragmentHelper;
import uk.co.digifymedia.londontravel.helpers.Internet;
import uk.co.digifymedia.londontravel.interfaces.AsyncResponseJourneyResults;
import uk.co.digifymedia.londontravel.service.JourneyResultService2;
import uk.co.digifymedia.swagger.client.model.DigifyTflApiPresentationEntitiesJourneyPlannerItineraryResult;
import uk.co.digifymedia.swagger.client.model.TflApiPresentationEntitiesJourneyPlannerJourney;

public class JourneyResultFragment extends Fragment implements AsyncResponseJourneyResults {

    Internet internetHelper;
    Bundle savedInstanceState;
    JourneyResultService2 journeyResultService;
    CallbackJourneyResults callbackJourneyResults;
    CallbackJourneyResults callbackJourneyResultsBus;
    CallbackJourneyResults callbackJourneyResultsTube;
    JourneyResultAdaptor adapterx1;
    JourneyResultAdaptor adapterBus;
    JourneyResultAdaptor adapterTube;
    ListView listviewJourneyResults;
    ListView listviewJourneyResultsBus;
    ListView listviewJourneyResultsTube;
    LinearLayout linearLayoutLoading;
    ConstraintLayout constraintLayoutNoInternet;
    ImageView imageViewNavRefresh;
    ArrayList<String> modesBus = new ArrayList<String>();
    ArrayList<String> modesRail = new ArrayList<String>();
    int spinnerTimesPosition;

    String from;
    String to;

    int retryAll = 0;

    ProgressBar progressBarAll;
    ProgressBar progressBarBus;
    ProgressBar progressBarTube;

    int timeMilisecondsAll = 5000;

    TextView textViewSuggestedMessage;
    TextView textViewBusMessage;
    TextView textViewTubeMessage;

    Button buttonTryAgain;

    String arriveTime;
    String arriveDate;
    String departTime;
    String departDate;
    Timer timer;
    Timer timerSch;

    boolean fromTimer;

    List<TflApiPresentationEntitiesJourneyPlannerJourney> listSuggested = new ArrayList<>();
    List<TflApiPresentationEntitiesJourneyPlannerJourney> listBus = new ArrayList<>();
    List<TflApiPresentationEntitiesJourneyPlannerJourney> listTube = new ArrayList<>();

    // Animation
    Animation animFadein;
    Animation animFadeOut;


    public JourneyResultFragment() {
        arriveTime = Prefs.getString("arrival-time", null);
        arriveDate = Prefs.getString("arrival-date", null);
        departTime = Prefs.getString("depart-time", null);
        departDate = Prefs.getString("depart-date", null);

        journeyResultService = new JourneyResultService2();
        callbackJourneyResults = new CallbackJourneyResults(this, false, false);
        callbackJourneyResultsBus = new CallbackJourneyResults(this, true, false);
        callbackJourneyResultsTube = new CallbackJourneyResults(this, false, true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        internetHelper = new Internet(getActivity());

        // Initialize the Prefs class
        new Prefs.Builder()
                .setContext(getContext())
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName("uk.co.digifymedia.londontravel")
                .setUseDefaultSharedPreference(true)
                .build();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_journeyresult, container, false);

        // load the animation
        animFadein = AnimationUtils.loadAnimation(getContext(),
                R.anim.fade_in_alpha45);
        animFadeOut = AnimationUtils.loadAnimation(getContext(),
                R.anim.fade_out_alpha45);

        setModesBus();
        setModesTubeAndRail();

        this.savedInstanceState = savedInstanceState;

        bindViews(view);

        from = Prefs.getString("start-station-id", "");
        to = Prefs.getString("end-station-id", "");

        invokeResultSearch(false);

        refreshButtonAnimate();
        imageViewNavRefresh.setOnClickListener(navigationRefreshListener());


        return view;
    }

    private void timerSchedule() {
        if(timer == null) {
            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    refreshButtonAnimate();
                    invokeResultSearch(true);
                }
            }, 20000, 20000);
        }
    }

    private void timerSchedule2() {
        if(timerSch == null) {
            timerSch = new Timer();
            timerSch.schedule(new TimerTask() {
                @Override
                public void run() {
                    updateTimes(listviewJourneyResults);
                    updateTimes(listviewJourneyResultsBus);
                    updateTimes(listviewJourneyResultsTube);
                }
            }, 10000, 10000);
        }
    }

    @NonNull
    private View.OnClickListener navigationRefreshListener() {

        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refreshButtonAnimate();
                FragmentHelper.switchFragmentReplace(getActivity().getSupportFragmentManager(), new JourneyResultFragment());
            }
        };
    }

    private void refreshButtonAnimate() {
        if(imageViewNavRefresh.getAnimation() == null || imageViewNavRefresh.getAnimation().hasEnded()) {
            imageViewNavRefresh.clearAnimation();
            RotateAnimation anim = new RotateAnimation(0, 360, imageViewNavRefresh.getWidth() / 2, imageViewNavRefresh.getHeight() / 2);
            anim.setFillAfter(true);
            anim.setRepeatCount(Animation.INFINITE);
            anim.setDuration(2000);
            imageViewNavRefresh.startAnimation(anim);
        }
    }

    public static String getTime() {
        Calendar calendar = Calendar.getInstance();
        String mins = String.valueOf(calendar.get(Calendar.MINUTE));
        String hour = String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));


        if(mins.length() <= 1) {
            mins = "0" + mins;
        }
        if(hour.length() <= 1) {
            hour = "0" + hour;
        }

        return String.valueOf(hour + mins);

    }

    private void invokeResultSearch(boolean fromTimer) {
        this.fromTimer = fromTimer;
        try {
            boolean timeIsNow = false;
            if (internetHelper.check()) {
                ShowNoInternetLayoutTryAgain(false);

                spinnerTimesPosition = ((Spinner) getActivity().findViewById(R.id.spinnerTimes)).getSelectedItemPosition();
                String timels = "Departing";
                if(spinnerTimesPosition == 0){
                    arriveTime = getTime();
                    timeIsNow = true;
                }
                else if(spinnerTimesPosition ==1){
                    timels = "Arriving";
                }
                else if(spinnerTimesPosition ==2){
                    arriveTime = departTime;
                    arriveDate = departDate;
                }

                Log.d("Time2", timels);
                Log.d("Time2", arriveTime);

                listviewJourneyResults.startAnimation(animFadeOut);
                listviewJourneyResultsBus.startAnimation(animFadeOut);
                listviewJourneyResultsTube.startAnimation(animFadeOut);
                journeyResultService.journeyResults( from, to, null, false, arriveDate, arriveTime, timels,
                        null, null, null, null, null, null,
                        null, null, null, null, null, null,
                        null, null, null, null, null, null, callbackJourneyResults);
                if(timeIsNow) {
                    arriveTime = null;
                }
            }
            else {

                ShowNoInternetLayoutTryAgain(true);
            }
        }
        catch (Exception e) {
            e.printStackTrace();

        }
    }

    private void ShowNoInternetLayoutTryAgain(boolean showNoInternetLayout) {
        if(!showNoInternetLayout) {
            listviewJourneyResultsTube.setVisibility(View.VISIBLE);
            listviewJourneyResults.setVisibility(View.VISIBLE);
            listviewJourneyResultsBus.setVisibility(View.VISIBLE);
            constraintLayoutNoInternet.setVisibility(View.GONE);
        }
        else {
            listviewJourneyResultsTube.setVisibility(View.GONE);
            listviewJourneyResults.setVisibility(View.GONE);
            listviewJourneyResultsBus.setVisibility(View.GONE);
            constraintLayoutNoInternet.setVisibility(View.VISIBLE);
            imageViewNavRefresh.clearAnimation();

            // Set retry button listener
            buttonTryAgain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    invokeResultSearch(false);
                }
            });
        }

    }


    private void bindViews(View view) {
        listviewJourneyResults = (ListView) view.findViewById(R.id.listViewSearchJourneyResultsSuggested);
        listviewJourneyResultsBus = (ListView) view.findViewById(R.id.listViewSearchJourneyResultsBus);
        listviewJourneyResultsTube = (ListView) view.findViewById(R.id.listViewSearchJourneyResultsTube);
        progressBarAll = (ProgressBar) view.findViewById(R.id.progressbarResultsSuggested) ;
        progressBarBus = (ProgressBar) view.findViewById(R.id.progressbarResultsBus) ;
        progressBarTube = (ProgressBar) view.findViewById(R.id.progressbarResultsTube) ;
        textViewSuggestedMessage = (TextView) view.findViewById(R.id.textViewSuggestedMessage);
        textViewBusMessage = (TextView) view.findViewById(R.id.textViewBusMessage);
        textViewTubeMessage = (TextView) view.findViewById(R.id.textViewTubeMessage);

        constraintLayoutNoInternet = (ConstraintLayout) view.findViewById(R.id.layoutNoInternetReload);
        linearLayoutLoading = (LinearLayout) getActivity().findViewById(R.id.layoutLoading);

        buttonTryAgain = (Button) view.findViewById(R.id.buttonTryAgain);
        imageViewNavRefresh = getActivity().findViewById(R.id.navigationIconRefresh);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onAsyncFinish(DigifyTflApiPresentationEntitiesJourneyPlannerItineraryResult result, int statusCode) {
        Log.d("Journey", "Results all");

        if(statusCode == 200) {

            imageViewNavRefresh.clearAnimation();

            listSuggested= result.getSuggested().getJourneys();
            listBus= result.getBus().getJourneys() ;
            listTube= result.getTube().getJourneys();
            Log.d("results2", listSuggested.toString());
            //if(!fromTimer) {
                adapterx1 = new JourneyResultAdaptor(
                        getActivity(),
                        R.layout.list_item_journey_result_1,
                        listSuggested,
                        spinnerTimesPosition
                );
                Log.d("timerj", "Set adapter");
            //}

            setListViewStations(adapterx1, result);

            setLoadingVisibility(progressBarAll, View.GONE);
            setTextViewMessage(textViewSuggestedMessage, null, false);

            if(!fromTimer) {
                adapterBus = new JourneyResultAdaptor(
                        getActivity(),
                        R.layout.list_item_journey_result_1,
                        listBus,
                        spinnerTimesPosition);
            }

            setListViewStationsBus(adapterBus, result);

            setLoadingVisibility(progressBarBus, View.GONE);
            setTextViewMessage(textViewBusMessage, null, false);

            if(!fromTimer) {
                adapterTube = new JourneyResultAdaptor(
                        getActivity(),
                        R.layout.list_item_journey_result_1,
                        listTube,
                        spinnerTimesPosition);
            }

            setListViewStationsTube(adapterTube, result);

            setLoadingVisibility(progressBarTube, View.GONE);
            setTextViewMessage(textViewTubeMessage, null, false);

            timerSchedule();
            //timerSchedule2();
        }
        else {
            Log.d("Journey", "Results all failed, retrying..");
            retryAll++;

            if(retryAll < 3) {
                new BataTime(timeMilisecondsAll).start(new BataTimeCallback() {
                    @Override
                    public void onUpdate(int elapsed) {
                        Log.e("Journey", "All On update called...time elapsed = " + elapsed );
                        setTextViewMessage(textViewSuggestedMessage, getTextRetryingMessage(elapsed), true);
                    }

                    @Override
                    public void onComplete() {
                        Log.d("Journey", "All On complete called..." );
                        try {
                            invokeResultSearch(false);
                        } catch (Exception e) {
                            e.printStackTrace();
                            timeMilisecondsAll = timeMilisecondsAll * 3;
                            // TODO display error
                        }
                    }
                });

            }
            else {
                // TODO display error
                setTextViewPermanentFailMessage(textViewSuggestedMessage);
                setLoadingVisibility(progressBarAll, View.GONE);
                imageViewNavRefresh.clearAnimation();
            }
        }

    }

    @NonNull
    private String getTextRetryingMessage(int elapsed) {
        if(elapsed <= 1000) {
            return "Loading now...";
        }
        else {
            return "Loading in " + TimeUnit.MILLISECONDS.toSeconds(elapsed) + " seconds..";
        }

    }

    @NonNull
    private void setTextViewPermanentFailMessage(final TextView textView) {
        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                textView.setText("An error occurred when trying to load.");
            }
        });
    }

    private void setLoadingVisibility(final ProgressBar progressBar, final int visibility) {
        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                progressBar.setVisibility(visibility);
            }
        });
    }

    // TODO moved to helper class used in multiple locations
    private void setListViewStations(final JourneyResultAdaptor adapter, final DigifyTflApiPresentationEntitiesJourneyPlannerItineraryResult result) {
        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {

                listviewJourneyResults.startAnimation(animFadein);
                listviewJourneyResults.setAdapter(adapter);

            }
        });
    }

    private void setListViewStationsBus(final JourneyResultAdaptor adapter, final DigifyTflApiPresentationEntitiesJourneyPlannerItineraryResult result) {
        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                listviewJourneyResultsBus.startAnimation(animFadein);
                listviewJourneyResultsBus.setAdapter(adapter);

            }
        });
    }

    private void setListViewStationsTube(final JourneyResultAdaptor adapter, final DigifyTflApiPresentationEntitiesJourneyPlannerItineraryResult result) {
        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                listviewJourneyResultsTube.startAnimation(animFadein);
                listviewJourneyResultsTube.setAdapter(adapter);
            }
        });
    }

    private void updateTimes(ListView list){
        try {
            for (int i = 0; i < list.getCount(); i++) {
                View v = list.getChildAt(i);
                TextView t1 = v.findViewById(R.id.textViewStartTime);
                TextView t2 = v.findViewById(R.id.textViewMinSchedule);

                StringBuffer stringBufferTimeTotal = new StringBuffer();

                Duration diff = Duration.between(ZonedDateTime.now(), (Temporal) t1.getTag());
                stringBufferTimeTotal.append(diff.toMinutes() + " min");

                t2.setText(stringBufferTimeTotal);
            }
        }
        catch(Exception e) {
            Log.e("Journey", "Exception updating times", e);
        }

    }

    private void setModesBus() {
        // TODO each mode should be seperate element but API does not allow
        modesBus.add(Constants.TRANSPORT_MODE.BUS.getValue());
    }

    private void setModesTubeAndRail() {
        // TODO each mode should be seperate element but API does not allow
        modesRail.add(
                Constants.TRANSPORT_MODE.TUBE.getValue() + "," +
                Constants.TRANSPORT_MODE.NATIONAL_RAIL.getValue()+ "," +
                Constants.TRANSPORT_MODE.DLR.getValue()+ "," +
                Constants.TRANSPORT_MODE.OVERGROUND.getValue()+ "," +
                Constants.TRANSPORT_MODE.TFLRAIL.getValue()+ "," +
                Constants.TRANSPORT_MODE.TRAM.getValue()
        );

    }

    private void setTextViewMessage(final TextView textView, final String text, final boolean makeVisible){
        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                textView.setText(text);
                if(makeVisible){
                    textView.setVisibility(View.VISIBLE);
                }
                else {
                    textView.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if(timer != null) {
            timer.cancel();
        }
        timerSchedule();
    }

    @Override
    public void onPause() {
        super.onPause();
        if(timer != null)
            timer.cancel();
        if(timerSch != null)
            timerSch.cancel();
    }

    @Override
    public void onStop() {
        super.onStop();
        if(timer != null)
            timer.cancel();
        if(timerSch != null)
            timerSch.cancel();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(timer != null)
            timer.cancel();
        if(timerSch != null)
            timerSch.cancel();
    }

}
