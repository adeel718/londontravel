package uk.co.digifymedia.londontravel.helpers;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import uk.co.digifymedia.londontravel.interfaces.LocationCallback;
import com.yayandroid.locationmanager.LocationManager;
import com.yayandroid.locationmanager.listener.LocationListener;

import static com.yayandroid.locationmanager.configuration.Configurations.defaultConfiguration;

public class LocationHelper {
    public LocationCallback callback;

    private double lat;
    private double lon;

    public void getLocation(Context context, Activity activity, Fragment fragment) {
        LocationManager awesomeLocationManager = new LocationManager.Builder(activity.getApplicationContext())
                .activity(activity) // Only required to ask permission and/or GoogleApi - SettingsApi
                .fragment(fragment) // Only required to ask permission and/or GoogleApi - SettingsApi
                .configuration(defaultConfiguration("rationalMessage", "gps message"))
                .notify(new LocationListener() {
                    @Override
                    public void onProcessTypeChanged(int processType) {

                    }

                    @Override
                    public void onLocationChanged(Location location) {
                        lat = location.getLatitude();
                        lon = location.getLongitude();
                        callback.execute(location);

                    }

                    @Override
                    public void onLocationFailed(int type) {

                    }

                    @Override
                    public void onPermissionGranted(boolean alreadyHadPermission) {

                    }

                    @Override
                    public void onStatusChanged(String provider, int status, Bundle extras) {

                    }

                    @Override
                    public void onProviderEnabled(String provider) {

                    }

                    @Override
                    public void onProviderDisabled(String provider) {

                    }
                })
                .build();
        awesomeLocationManager.get();
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }
}
