package uk.co.digifymedia.londontravel.listeners;

import android.support.v4.app.FragmentManager;
import android.support.v7.widget.SearchView;

import uk.co.digifymedia.londontravel.fragments.JourneyPlannerStartFragment;
import uk.co.digifymedia.londontravel.helpers.FragmentHelper;

public class JourneyRecentsSavedTextViewListener implements SearchView.OnQueryTextListener {
    FragmentManager fragmentManager;

    public JourneyRecentsSavedTextViewListener(FragmentManager fragmentManager){
        this.fragmentManager = fragmentManager;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        JourneyPlannerStartFragment myFragment = (JourneyPlannerStartFragment) fragmentManager.findFragmentByTag("Search");

        // Only switch to the fragment first time
        //if (myFragment == null/* && myFragment.isVisible()*/) {

            FragmentHelper.switchFragmentReplace(fragmentManager, new JourneyPlannerStartFragment(), "Search");
        //}
        return false;
    }
}
