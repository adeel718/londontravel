package uk.co.digifymedia.londontravel.adaptors;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import uk.co.digifymedia.R;
import uk.co.digifymedia.londontravel.fragments.JourneyPlannerRecentsFragment;
import uk.co.digifymedia.londontravel.fragments.JourneyPlannerSavedFragment;
import uk.co.digifymedia.londontravel.fragments.JourneyPlannerStartFragment;

/**
 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {
    private Context context;
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();
    private int[] tabIcons = {
            R.drawable.ic_action_time_light,
            R.drawable.ic_action_favorite_light
    };

    public SectionsPagerAdapter(FragmentManager manager, Context context) {
        super(manager);
        this.context = context;
    }


    public View getTabView(int position) {
        View v = LayoutInflater.from(context).inflate(R.layout.tab_custom_journey, null);
        TextView textView = v.findViewById(R.id.textView);
        textView.setText(mFragmentTitleList.get(position));
        ImageView imageView = v.findViewById(R.id.imageView);
        imageView.setImageResource(tabIcons[position]);
        return v;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFragment(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }

    // we need to override this so that fragment can be refreshed
    @Override
    public int getItemPosition(Object object) {

        return POSITION_NONE;
    }
}
