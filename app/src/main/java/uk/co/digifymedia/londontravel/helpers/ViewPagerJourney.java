package uk.co.digifymedia.londontravel.helpers;

import android.support.design.widget.TabLayout;

import uk.co.digifymedia.londontravel.adaptors.SectionsPagerAdapter;

public class ViewPagerJourney {
    public static void setupTabIcons(SectionsPagerAdapter pagerAdapter, TabLayout tabLayout) {
        for (int i=0;i<tabLayout.getTabCount();i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            if (tab != null) tab.setCustomView(pagerAdapter.getTabView(i));
        }
    }
}
