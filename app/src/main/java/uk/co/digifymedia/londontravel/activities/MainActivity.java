package uk.co.digifymedia.londontravel.activities;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.facebook.stetho.Stetho;
import com.orm.SugarDb;

import uk.co.digifymedia.R;
import uk.co.digifymedia.myoystercontactless.FragmentCardsSummary;
import uk.co.digifymedia.myoystercontactless.FragmentLogin;
import uk.co.digifymedia.myoystercontactless.Logging;
import uk.co.digifymedia.myoystercontactless.helpers.Generic;
import uk.co.digifymedia.myoystercontactless.model.Login;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    Logging l = new Logging();
    NavigationView navigationView;
    Boolean loggedIn;
    AppCompatActivity activity;
    Generic generic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setTheme(R.style.AppThemeDark);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //setHasOptionsMenu(true);

        l.logEvent("MainActivity", "Started application");

        Login login = new Login();
        try {
            login = Login.findById(Login.class, 1);
        }
        catch(Exception e){
            Logging.logError(this.getClass().getSimpleName() + "/1", e);
            e.printStackTrace();
        }

        // update the main content by replacing fragments
        Fragment fragment = new FragmentLogin();
        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager(); // For AppCompat use getSupportFragmentManager
        generic = new Generic(fragmentManager, this);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        activity = this;

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerView = navigationView.inflateHeaderView(R.layout.nav_header_main);

        TextView textViewNavAccountEmail = (TextView) headerView.findViewById(R.id.textViewNavAccountEmail);
        TextView textViewNavAccountName = (TextView) headerView.findViewById(R.id.textViewNavAccountName);

        // Initiate Sugar DB
        SugarDb db = new SugarDb(this);
        db.onCreate(db.getDB());


        if(login!=null) {
            loggedIn = true;
            if (login.getEmail() != null) {
                textViewNavAccountEmail.setText(login.getEmail());
            }
            if (login.getName() != null) {
                textViewNavAccountName.setText(login.getName());
            }
            if(login.getEmail()!=null && login.getPassword()!=null){
                fragment = new FragmentCardsSummary().newInstance("oyster");
            }
            navigationView.getMenu().getItem(1).setChecked(true);
        }
        else {
            loggedIn = false;
            generic.setNavigationMenuUnauthorised(navigationView);
            navigationView.getMenu().getItem(0).setChecked(true);
        }


        //FragmentHelper.switchFragmentReplace(getSupportFragmentManager(), fragment);
        fragmentManager.beginTransaction()
                .replace(R.id.content_main_oyster, fragment)
                //.addToBackStack("Main")
                .commit();

        Stetho.newInitializerBuilder(this)
                .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this))
                .build();

        Stetho.initializeWithDefaults(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        return generic.onNavigationItemSelectedListener(item, navigationView, loggedIn);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

}
