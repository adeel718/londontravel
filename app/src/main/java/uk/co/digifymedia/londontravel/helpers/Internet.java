package uk.co.digifymedia.londontravel.helpers;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.androidadvance.topsnackbar.TSnackbar;
import com.androidstudy.networkmanager.Monitor;
import com.androidstudy.networkmanager.Tovuti;

import de.mateware.snacky.Snacky;
import uk.co.digifymedia.R;

public class Internet {

    private Activity activity;
    private boolean snackBarShown = false;

    public Internet(Activity activity){
        this.activity = activity;

    }

    public boolean isConnectedToInternet() {
        ConnectivityManager cm =
                (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public boolean check() {
        if(!isConnectedToInternet()){
            showSnackBar("No internet connection. Please check your connection settings and try again.");
            return false;
        }
        return true;
    }

    public void showSnackBar(String string)
    {
        if(!snackBarShown) {
            Snacky.builder()
                    .setActivity(activity)
                    .setText(string)
                    .setDuration(Snacky.LENGTH_INDEFINITE)
                    .setActionText(android.R.string.ok)
                    .error()
                    .addCallback(new Snackbar.Callback() {
                        @Override
                        public void onShown(Snackbar sb) {
                            snackBarShown = true;
                        }

                        @Override
                        public void onDismissed(Snackbar sb, int event) {
                            snackBarShown = false;
                        }
                    })
                    .show();
        }

    }
}
