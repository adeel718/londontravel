package uk.co.digifymedia.londontravel.callbacks;

import android.util.Log;

import java.util.List;
import java.util.Map;

import uk.co.digifymedia.londontravel.interfaces.AsyncResponseJourneyResults;
import uk.co.digifymedia.swagger.client.ApiCallback;
import uk.co.digifymedia.swagger.client.ApiException;
import uk.co.digifymedia.swagger.client.model.DigifyTflApiPresentationEntitiesJourneyPlannerItineraryResult;
import uk.co.digifymedia.swagger.client.model.TflApiPresentationEntitiesJourneyPlannerItineraryResult;

public class CallbackJourneyResults implements ApiCallback<DigifyTflApiPresentationEntitiesJourneyPlannerItineraryResult>{
    private AsyncResponseJourneyResults asyncResponse;
    private boolean busOnly;
    private boolean tubeOnly;

    public CallbackJourneyResults(AsyncResponseJourneyResults asyncResponse, boolean busOnly, boolean tubeOnly) {
        this.asyncResponse = asyncResponse;
        this.busOnly = busOnly;
        this.tubeOnly = tubeOnly;
    }

    @Override
    public void onFailure(ApiException e, int statusCode, Map<String, List<String>> responseHeaders) {
        // TODO handle failures properly
        // TODO will come here if 404
        Log.d("failed response", e.toString());
        /*if(busOnly){
            asyncResponse.onAsyncFinishBus(null, statusCode);
        }
        else if(tubeOnly) {
            asyncResponse.onAsyncFinishTube(null, statusCode);
        }
        else {*/
            asyncResponse.onAsyncFinish(null, statusCode);
        //}
    }

    @Override
    public void onSuccess(DigifyTflApiPresentationEntitiesJourneyPlannerItineraryResult result, int statusCode, Map<String, List<String>> responseHeaders) {
        // TODO handle success properly
        //Log.d("response", result.toString());
        /*if(busOnly){
            asyncResponse.onAsyncFinishBus(result, statusCode);
        }
        else if(tubeOnly) {
            asyncResponse.onAsyncFinishTube(result, statusCode);
        }
        else {*/
            asyncResponse.onAsyncFinish(result, statusCode);
        //}

    }

    @Override
    public void onUploadProgress(long bytesWritten, long contentLength, boolean done) {

    }

    @Override
    public void onDownloadProgress(long bytesRead, long contentLength, boolean done) {
        // TODO show progress bar
    }
}
