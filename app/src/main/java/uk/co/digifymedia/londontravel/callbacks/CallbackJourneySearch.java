package uk.co.digifymedia.londontravel.callbacks;

import android.util.Log;

import java.util.List;
import java.util.Map;

import uk.co.digifymedia.londontravel.interfaces.AsyncResponseJourneySearch;
import uk.co.digifymedia.swagger.client.ApiCallback;
import uk.co.digifymedia.swagger.client.ApiException;
import uk.co.digifymedia.swagger.client.model.TflApiPresentationEntitiesSearchResponse;

public class CallbackJourneySearch implements ApiCallback<TflApiPresentationEntitiesSearchResponse>{
    private AsyncResponseJourneySearch asyncResponse;

    public CallbackJourneySearch(AsyncResponseJourneySearch asyncResponse) {
        this.asyncResponse = asyncResponse;
    }

    @Override
    public void onFailure(ApiException e, int statusCode, Map<String, List<String>> responseHeaders) {
        // TODO handle failures properly
        Log.d("response", e.toString());
    }

    @Override
    public void onSuccess(TflApiPresentationEntitiesSearchResponse result, int statusCode, Map<String, List<String>> responseHeaders) {
        // TODO handle success properly
        //Log.d("response", result.toString());
        asyncResponse.onAsyncFinish(result);
    }

    @Override
    public void onUploadProgress(long bytesWritten, long contentLength, boolean done) {

    }

    @Override
    public void onDownloadProgress(long bytesRead, long contentLength, boolean done) {
        // TODO show progress bar
    }
}
