package uk.co.digifymedia.londontravel.helpers;

import android.util.Log;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class CustomOkHttpClient extends OkHttpClient {

    public CustomOkHttpClient() {

        this.setConnectTimeout(20000, TimeUnit.MILLISECONDS);
        this.setReadTimeout(20000, TimeUnit.MILLISECONDS);
        /*this.interceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                Response response = null;
                boolean responseOK = false;
                int tryCount = 0;

                while (!responseOK && tryCount < 6) {
                    try {
                        response = chain.proceed(request);
                        responseOK = response.isSuccessful();
                    }catch (Exception e){
                        Log.d("intercept", "Request is not successful - " + tryCount);
                    }finally{
                        tryCount++;
                    }
                }

                // otherwise just pass the original response on
                return response;
            }
        });*/
    }
}
