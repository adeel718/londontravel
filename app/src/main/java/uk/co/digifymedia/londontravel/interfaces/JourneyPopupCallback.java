package uk.co.digifymedia.londontravel.interfaces;

public interface JourneyPopupCallback {
    void execute();
}
