package uk.co.digifymedia.londontravel.listeners;

import android.location.Location;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nhaarman.listviewanimations.appearance.simple.ScaleInAnimationAdapter;
import com.squareup.okhttp.Call;

import java.util.ArrayList;
import java.util.List;

import uk.co.digifymedia.R;
import uk.co.digifymedia.londontravel.adaptors.JourneySearchAdaptor;
import uk.co.digifymedia.londontravel.adaptors.SectionsPagerAdapter;
import uk.co.digifymedia.londontravel.callbacks.CallbackJourneySearch;
import uk.co.digifymedia.londontravel.config.Constants;
import uk.co.digifymedia.londontravel.helpers.Internet;
import uk.co.digifymedia.londontravel.helpers.LocationHelper;
import uk.co.digifymedia.londontravel.helpers.ViewPagerJourney;
import uk.co.digifymedia.londontravel.interfaces.AsyncResponseJourneySearch;
import uk.co.digifymedia.londontravel.interfaces.JourneyPopupCallback;
import uk.co.digifymedia.londontravel.interfaces.LocationCallback;
import uk.co.digifymedia.londontravel.service.StopPointService;
import uk.co.digifymedia.swagger.client.ApiException;
import uk.co.digifymedia.swagger.client.model.TflApiPresentationEntitiesMatchedStop;
import uk.co.digifymedia.swagger.client.model.TflApiPresentationEntitiesSearchResponse;

public class JourneySearchTextViewListener implements AsyncResponseJourneySearch, LocationCallback, SearchView.OnQueryTextListener {
    SearchView editText;
    private ListView listViewStations;
    AppCompatActivity activity;
    Fragment fragment;
    ArrayList<String> modes = new ArrayList<>();
    CallbackJourneySearch callbackJourneySearch;
    Internet internetHelper;
    TabLayout tabs;
    ViewPager viewPager;
    ProgressBar progressBar;
    FrameLayout linearLayoutFragmentContainer;

    private String lastQuerystring = "";
    private Call listCalls;
    JourneySearchAdaptor adapter;

    TextView textView;

    private double lat;
    private  double lon;


    public JourneySearchTextViewListener(SearchView editTextSearch, ListView listViewStations, TextView viewById, AppCompatActivity activity, Fragment fragment) {
        this.activity = activity;
        this.fragment = fragment;
        this.editText = editTextSearch;
        this.listViewStations = listViewStations;
        this.tabs = activity.findViewById(R.id.tabs);
        this.textView = viewById;

        callbackJourneySearch = new CallbackJourneySearch(this);
        linearLayoutFragmentContainer = activity.findViewById(R.id.frameLayoutContent);
        internetHelper = new Internet(activity);
        viewPager = activity.findViewById(R.id.container);
        progressBar = activity.findViewById(R.id.progressbar);


        LocationHelper locationHelper = new LocationHelper();
        locationHelper.callback = this;
        locationHelper.getLocation(activity.getApplicationContext(), activity, fragment);

        setModes();
    }

    private void hideTabsAndDisplayFragment() {
        tabs.setVisibility(View.GONE);
        linearLayoutFragmentContainer.setVisibility(View.VISIBLE);
        viewPager.setVisibility(View.GONE);

    }

    private void displayTabsAndHideFragment() {
        tabs.setVisibility(View.VISIBLE);
        linearLayoutFragmentContainer.setVisibility(View.GONE);
        viewPager.setVisibility(View.VISIBLE);
    }


    private void setListViewStations(final JourneySearchAdaptor adapter) {
        activity.runOnUiThread(new Runnable() {

            @Override
            public void run() {


                ScaleInAnimationAdapter animationAdapter = new ScaleInAnimationAdapter(adapter);
                animationAdapter.setAbsListView(listViewStations);
                listViewStations.setAdapter(animationAdapter);

            }
        });
    }

    private void setModes() {
        // TODO each mode should be seperate element but API does not allow
        modes.add(Constants.TRANSPORT_MODE.BUS.getValue() + "," +
                Constants.TRANSPORT_MODE.TUBE.getValue() + "," +
                Constants.TRANSPORT_MODE.NATIONAL_RAIL.getValue() + "," +
                Constants.TRANSPORT_MODE.DLR.getValue() + "," +
                Constants.TRANSPORT_MODE.OVERGROUND.getValue() + "," +
                Constants.TRANSPORT_MODE.TFLRAIL.getValue() + "," +
                Constants.TRANSPORT_MODE.RIVER_BUS.getValue() + "," +
                Constants.TRANSPORT_MODE.TRAM.getValue() + "," +
                Constants.TRANSPORT_MODE.CABLE_CAR.getValue() + "," +
                Constants.TRANSPORT_MODE.COACH.getValue()
        );
    }


    @Override
    public void onAsyncFinish(TflApiPresentationEntitiesSearchResponse result) {

        final List<TflApiPresentationEntitiesMatchedStop> list = result.getMatches();
        Log.d("Journey", "response:" + result.getQuery());
        lastQuerystring = result.getQuery();

        // check needed to see if not navigated away from screen
        if(linearLayoutFragmentContainer.getVisibility() == View.VISIBLE) {

            final JourneyPopupCallback myCallback = new JourneyPopupCallback() {
                @Override
                public void execute() {
                    init(list, this);
                }
            };

            init(list, myCallback);

            activity.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    progressBar.setVisibility(View.INVISIBLE);
                    hideTabsAndDisplayFragment();
                }
            });
        }
        else {
            Log.d("Journey", "onAsyncFinish fragment has gone");
        }

    }

    private void init(List<TflApiPresentationEntitiesMatchedStop> list, JourneyPopupCallback myCallback) {
        adapter = new JourneySearchAdaptor(
                activity,
                fragment,
                R.layout.list_item_journey_search_1,
                list,
                myCallback,
                lat,
                lon);

        adapter.notifyDataSetChanged();
        setListViewStations(adapter);

    }

    @Override
    public void execute(Location location) {
        lat = location.getLatitude();
        lon = location.getLongitude();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {

        // this will set to the 'Search' fragment
        hideTabsAndDisplayFragment();

        listViewStations.invalidate();
        linearLayoutFragmentContainer.invalidate();

            //here, after we introduced something in the EditText we get the string from it
            String answerString = newText;

            if (answerString.toString().length() == 0) {
                if(listCalls != null) {
                    listCalls.cancel();
                }
                displayTabsAndHideFragment();
                progressBar.setVisibility(View.INVISIBLE);
                //setListViewStations(null);

                // this will set to the 'Recents' fragment
                viewPager.setCurrentItem(0);

                // this will refresh the fragment
                // only set icons after notifyDataSetChanged
                TabLayout tabLayout = (TabLayout) activity.findViewById(R.id.tabs);

                if(viewPager.getAdapter() != null) {
                    viewPager.getAdapter().notifyDataSetChanged();
                }

                ViewPagerJourney.setupTabIcons((SectionsPagerAdapter) viewPager.getAdapter(), tabLayout);
            }
            else {
                if (internetHelper.check()) {

                    if ((!lastQuerystring.equals(answerString))) {
                        StopPointService stopPointService = new StopPointService();
                        hideTabsAndDisplayFragment();
                        try {
                            progressBar.setVisibility(View.VISIBLE);
                            Log.d("Journey", "Calling API with search string: " + answerString);

                            if (listCalls != null) {
                                listCalls.cancel();
                            }

                            Call call = stopPointService.stopPointSearch(modes, answerString, callbackJourneySearch);
                            listCalls = call;
                        } catch (ApiException e) {
                            progressBar.setVisibility(View.INVISIBLE);
                            displayTabsAndHideFragment();
                            Log.e("Journey exception", e.getMessage());
                            e.printStackTrace();
                        }
                    }
                }
            }

        return false;
    }
}
