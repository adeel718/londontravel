package uk.co.digifymedia.londontravel.activities;

import android.content.ContextWrapper;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import com.pixplicity.easyprefs.library.Prefs;

import uk.co.digifymedia.R;
import uk.co.digifymedia.londontravel.adaptors.SectionsPagerAdapter;
import uk.co.digifymedia.londontravel.classes.CustomViewPager;
import uk.co.digifymedia.londontravel.fragments.JourneyPlannerRecentsFragment;
import uk.co.digifymedia.londontravel.fragments.JourneyPlannerSavedFragment;
import uk.co.digifymedia.londontravel.helpers.ViewPagerJourney;

public class JourneyPlannerEndActivity extends ApplicationActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    TabLayout tabLayout;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private CustomViewPager mViewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_journey_planner_end);

        // Initialize the Prefs class
        new Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName("uk.co.digifymedia.londontravel")
                .setUseDefaultSharedPreference(true)
                .build();

        /*Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);*/

        // get the Intent that started this Activity
        Intent intent = getIntent();

        // get the Bundle that stores the data of this Activity
        Bundle bundle = intent.getExtras();


        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), this);

        // Set up the ViewPager with the sections adapter.
        mViewPager = findViewById(R.id.container);

        mSectionsPagerAdapter.addFragment(new JourneyPlannerRecentsFragment(), "Recent");
        mSectionsPagerAdapter.addFragment(new JourneyPlannerSavedFragment(), "Saved");

        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setPagingEnabled(true);

        tabLayout = (TabLayout) findViewById(R.id.tabs);

        // Set up tab listeners
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
        tabLayout.setupWithViewPager(mViewPager);

        // Tab icons
        ViewPagerJourney.setupTabIcons(mSectionsPagerAdapter, tabLayout);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                View view = tab.getCustomView();
                if(view != null) {
                    view.setAlpha((float) 1.0);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                View view = tab.getCustomView();
                if(view != null) {
                    view.setAlpha((float) 0.5);
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        tabLayout.getTabAt(1).getCustomView().setAlpha((float) 0.5);

        // Set up navigation listeners
        ImageView imageViewNavBack = findViewById(R.id.navigationIconBack);
        imageViewNavBack.setOnClickListener(navigationBackListener());

        Prefs.remove("arrival-time");
        Prefs.remove("arrival-date");
        Prefs.remove("depart-time");
        Prefs.remove("depart-date");

    }



    @NonNull
    private View.OnClickListener navigationBackListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        };
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tabbed, menu);
        return true;
    }*/

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/
}
