package uk.co.digifymedia.londontravel.interfaces;

import uk.co.digifymedia.swagger.client.model.TflApiPresentationEntitiesJourneyPlannerItineraryResult;
import uk.co.digifymedia.swagger.client.model.TflApiPresentationEntitiesSearchResponse;

public interface AsyncResponseJourneySearch {
    void onAsyncFinish(TflApiPresentationEntitiesSearchResponse result);

}