package uk.co.digifymedia.myoystercontactless.helpers;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hookedonplay.decoviewlib.DecoView;
import com.hookedonplay.decoviewlib.charts.SeriesItem;

import java.io.IOException;
import java.util.List;

import okhttp3.HttpUrl;
import uk.co.digifymedia.myoystercontactless.GetJourneyHistory;
import uk.co.digifymedia.myoystercontactless.GetSummaryCards;
import uk.co.digifymedia.myoystercontactless.InternetConnection;
import uk.co.digifymedia.myoystercontactless.Logging;
import uk.co.digifymedia.R;
import uk.co.digifymedia.myoystercontactless.model.CardDetailed;
import uk.co.digifymedia.myoystercontactless.model.JourneyHistory;
import uk.co.digifymedia.myoystercontactless.model.Login;
import uk.co.digifymedia.myoystercontactless.model.Ticket;

import static uk.co.digifymedia.myoystercontactless.Urls.URL_OYSTER_CARDS;

/**
 * Created by adeel on 29/12/2016.
 */

public class CardsDetailedDetailedHelper {
    Activity activity;
    FragmentManager fm;
    View view;

    public CardsDetailedDetailedHelper(Activity activity, FragmentManager fm, View view) {
        this.activity = activity;
        this.fm = fm;
        this.view = view;
    }

    public void populateCardsDetailed(CardDetailed cardDetailed, List<String> notificationList,
                                      final String mParamCardNo) {

        List<Ticket> ticketList = Ticket.find(Ticket.class, "CARD_NUMBER = ?", mParamCardNo);

        DecoView arcView = (DecoView)activity.findViewById(R.id.dynamicArcView);

        // Create background track
        arcView.addSeries(new SeriesItem.Builder(Color.argb(255, 218, 218, 218))
                .setRange(99, 100, 100)
                .setInitialVisibility(true)
                .setLineWidth(45f)
                .build());

        //Create data series track
        /*final SeriesItem seriesItem1 = new SeriesItem.Builder(Color.argb(255, 63, 81, 181))
                .setRange(0, 100, 0)
                .setLineWidth(45f)
                .build();*/

        //int series1Index = arcView.addSeries(seriesItem1);

        /*arcView.addEvent(new DecoEvent.Builder(DecoEvent.EventType.EVENT_SHOW, true)
                .setDelay(0)
                .setDuration(1)

                .build());*/

        //arcView.addEvent(new DecoEvent.Builder(25).setIndex(series1Index).setDelay(4000).build());
        //arcView.addEvent(new DecoEvent.Builder(100).setIndex(series1Index).setDelay(500).build());
        //arcView.addEvent(new DecoEvent.Builder(10).setIndex(series1Index).setDelay(12000).build());

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );

        TextView textViewCardNumber = (TextView)activity.findViewById(R.id.textViewCardNumber);
        TextView textViewAutoTopUp = (TextView)activity.findViewById(R.id.textViewAutoTopUp);
        TextView textViewBalance = (TextView)activity.findViewById(R.id.textViewBalance);

        LinearLayout linearLayoutIncompleteJourneys = (LinearLayout)activity.findViewById(R.id.linearLayoutIncompleteJourneys);

        textViewBalance.setText(cardDetailed.getBalance());
        textViewCardNumber.setText(mParamCardNo);

        populateTickets(ticketList);

        populateAutoTopup(cardDetailed, textViewAutoTopUp);

        populateNotificationList(notificationList, linearLayoutIncompleteJourneys, lp);

        final GetSummaryCards getSummaryCards = new GetSummaryCards(fm, activity, view, null);
        final InternetConnection connection = new InternetConnection();
        final Login login = new Login().first(Login.class);

        onClickRefreshButton(getSummaryCards, connection, login);

        onClickJourneyHistory(mParamCardNo, connection);
    }

    private void populateTickets(List<Ticket> ticketList) {
        String prefixStartDate = "Start date: ";
        String prefixEndDate = "End date: ";
        String typeBus = "BUS_PASS";
        String typeTravelCard = "TRAVEL_CARD";
        Drawable imgBus = activity.getResources().getDrawable(R.drawable.icon_bus);
        Drawable imgTrain = activity.getResources().getDrawable(R.drawable.icon_tube);

        RelativeLayout rLayoutTicket1 = (RelativeLayout) activity.findViewById(R.id.relativeLayoutTicket1);
        RelativeLayout rLayoutTicket2 = (RelativeLayout) activity.findViewById(R.id.relativeLayoutTicket2);
        RelativeLayout rLayoutTicket3 = (RelativeLayout) activity.findViewById(R.id.relativeLayoutTicket3);

        ImageView imgViewTicket1 = (ImageView) activity.findViewById(R.id.imageViewTicket1);
        TextView textViewTicket1 = (TextView)activity.findViewById(R.id.textViewTicket1);
        TextView textViewTicket1SDate = (TextView)activity.findViewById(R.id.textViewTicket1StartDate);
        TextView textViewTicket1EDate = (TextView)activity.findViewById(R.id.textViewTicket1EndDate);
        ImageView imgViewTicket2 = (ImageView) activity.findViewById(R.id.imageViewTicket2);
        TextView textViewTicket2 = (TextView)activity.findViewById(R.id.textViewTicket2);
        TextView textViewTicket2SDate = (TextView)activity.findViewById(R.id.textViewTicket2StartDate);
        TextView textViewTicket2EDate = (TextView)activity.findViewById(R.id.textViewTicket2EndDate);
        ImageView imgViewTicket3 = (ImageView) activity.findViewById(R.id.imageViewTicket3);
        TextView textViewTicket3 = (TextView)activity.findViewById(R.id.textViewTicket3);
        TextView textViewTicket3SDate = (TextView)activity.findViewById(R.id.textViewTicket3StartDate);
        TextView textViewTicket3EDate = (TextView)activity.findViewById(R.id.textViewTicket3EndDate);

        // No tickets found processing
        if(ticketList.size() == 0){
            rLayoutTicket1.setVisibility(View.VISIBLE);
            textViewTicket1.setText("There are no tickets linked to this card");
            textViewTicket1SDate.setVisibility(View.GONE);
            textViewTicket1EDate.setVisibility(View.GONE);

        }

        // Loop through each ticket
        int i=0;
        for(Ticket ticket : ticketList) {
            // Setup first ticket
            if(i==0){
                rLayoutTicket1.setVisibility(View.VISIBLE);
                textViewTicket1.setText(ticket.getTitle());
                textViewTicket1SDate.setText(prefixStartDate + ticket.getStartDate());
                textViewTicket1EDate.setText(prefixEndDate +ticket.getEndDate());
                if(ticket.getType().equals(typeBus)){
                    imgViewTicket1.setImageDrawable(imgBus);
                }
                else if(ticket.getType().equals(typeTravelCard)){
                    imgViewTicket1.setImageDrawable(imgTrain);
                }
            }
            // Setup second ticket
            else if(i==1){
                rLayoutTicket2.setVisibility(View.VISIBLE);
                textViewTicket2.setText(ticket.getTitle());
                textViewTicket2SDate.setText(prefixStartDate + ticket.getStartDate());
                textViewTicket2EDate.setText(prefixEndDate +ticket.getEndDate());
                if(ticket.getType().equals(typeBus)){
                    imgViewTicket2.setImageDrawable(imgBus);
                }
                else if(ticket.getType().equals(typeTravelCard)){
                    imgViewTicket2.setImageDrawable(imgTrain);
                }
            }
            // Setup third ticket
            else if(i==2){
                rLayoutTicket3.setVisibility(View.VISIBLE);
                textViewTicket3.setText(ticket.getTitle());
                textViewTicket3SDate.setText(prefixStartDate + ticket.getStartDate());
                textViewTicket3EDate.setText(prefixEndDate + ticket.getEndDate());
                if(ticket.getType().equals(typeBus)){
                    imgViewTicket3.setImageDrawable(imgBus);
                }
                else if(ticket.getType().equals(typeTravelCard)){
                    imgViewTicket3.setImageDrawable(imgTrain);
                }
            }
            i++;
            // And.. presume no one has more than 3 tickets
        }
    }

    private void onClickJourneyHistory(final String mParamCardNo, final InternetConnection connection) {
        Button journeyHistBtn = (Button) activity.findViewById(R.id.buttonDetailedJourneyHistory);
        final GetJourneyHistory getJourneyHistory = new GetJourneyHistory();
        getJourneyHistory.setFragmentManager(fm);
        getJourneyHistory.setActivity(activity);
        List<JourneyHistory> journeyHistory = JourneyHistory.find(JourneyHistory.class, "CARD_NUMBER = ?", mParamCardNo);

        if(journeyHistory.size() <= 0){
            getJourneyHistory.disableJourneyHistoryButton(journeyHistBtn);
        }
        else {
            getJourneyHistory.enableJourneyHistoryButton(journeyHistBtn);
        }


        Button btnDetailedJourneyHistory = (Button)activity.findViewById(R.id.buttonDetailedJourneyHistory);
        btnDetailedJourneyHistory.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getJourneyHistory.switchToJourneyHistoryFragment(mParamCardNo);
            }
        });
    }



    private void onClickRefreshButton(final GetSummaryCards getSummaryCards, final InternetConnection connection, final Login login) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(URL_OYSTER_CARDS).newBuilder();
        final String url = urlBuilder.build().toString();
        final GetJourneyHistory getJourneyHistory = new GetJourneyHistory();
        getJourneyHistory.setFragmentManager(fm);
        getJourneyHistory.setActivity(activity);

        Button btnRefresh = (Button)activity.findViewById(R.id.buttonDetailedRefresh);
        btnRefresh.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    ProgressBar progressBar = (ProgressBar)activity.findViewById(R.id.progressBarLoading);
                    progressBar.setVisibility(View.VISIBLE);

                    /*// Disable the journey history button until all data received
                    SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(activity.getBaseContext());
                    boolean turnedOn = Boolean.parseBoolean(sp.getString("pref_sync_app_jrefresh","true"));
                    Button journeyHistBtn = (Button) activity.findViewById(R.id.buttonDetailedJourneyHistory);

                    if(journeyHistBtn != null && turnedOn){
                        getJourneyHistory.disableJourneyHistoryButton(journeyHistBtn);
                    }*/

                    getSummaryCards.doGetRequestUpdateCards(url, login, true);
                } catch (IOException e) {
                    e.printStackTrace();
                    Logging.logError(this.getClass().getSimpleName() + "/3", e);
                    connection.showErrorMessage(activity.getApplicationContext(), "An error occurred, please try again later (051)");
                } catch (Exception e) {
                    e.printStackTrace();
                    Logging.logError(this.getClass().getSimpleName() + "/4", e);
                    connection.showErrorMessage(activity.getApplicationContext(), "An error occurred, please try again later (052)");
                }

            }
        });
    }

    private void populateNotificationList(List<String> notificationList, LinearLayout linearLayoutIncompleteJourneys, LinearLayout.LayoutParams lp) {
        if(notificationList==null){
            TextView tv = new TextView(activity);
            tv.setText("You have 0 incomplete journey(s) eligible for refund.");
            tv.setLayoutParams(lp);
            tv.setTextColor(activity.getResources().getColor(R.color.colorPrimaryDark));
            linearLayoutIncompleteJourneys.addView(tv);
        }
        else {
            for (String item : notificationList) {
                TextView tv = new TextView(activity);
                tv.setText(item.toString());
                tv.setLayoutParams(lp);
                tv.setTextColor(activity.getResources().getColor(R.color.colorPrimaryDark));
                linearLayoutIncompleteJourneys.addView(tv);
            }
        }
    }

    private static void populateAutoTopup(CardDetailed cardDetailed, TextView textViewAutoTopUp) {
        if(cardDetailed.getAutoTopUp() == null){

        }
        if(cardDetailed.getAutoTopUp() != null && !cardDetailed.getAutoTopUp().trim().equals("")){
            textViewAutoTopUp.setText("Auto top-up is enabled");
        }
        else {
            textViewAutoTopUp.setText("Auto top-up not enabled");
        }
    }
}
