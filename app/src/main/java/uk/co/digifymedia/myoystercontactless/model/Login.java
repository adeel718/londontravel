package uk.co.digifymedia.myoystercontactless.model;

import com.orm.SugarRecord;

/**
 * Created by adeel on 04/12/2016.
 */

public class Login extends SugarRecord {

    String uniqueLoginId;

    String email;

    String password;

    String name;

    public Login(){
    }

    public Login(String email, String password, String name){
        this.email = email;
        this.password = password;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUniqueLoginId() {
        return uniqueLoginId;
    }

    public void setUniqueLoginId(String uniqueLoginId) {
        this.uniqueLoginId = uniqueLoginId;
    }
}
