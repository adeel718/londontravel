package uk.co.digifymedia.myoystercontactless.widget;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import uk.co.digifymedia.myoystercontactless.helpers.LogCustom;

/**
 * Created by adeel on 21/01/2017.
 */

public class AppWidgetAlarm {

    private Context mContext;
    String TAG_WIDGALARM = "WIDGET/alarm";

    public AppWidgetAlarm(Context context) {
        LogCustom.d(TAG_WIDGALARM,"instantiate AppWidgetAlarm");
        mContext = context;
    }

    public void startAlarm() {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(mContext);
        String interval = sp.getString("pref_sync_widget_time","180");

        LogCustom.d(TAG_WIDGALARM,"startAlarm enter interval is " + interval);

        AlarmManager am = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
        long intervalMillis = Integer.valueOf(interval) * 60 * 1000;

        PendingIntent pi = getAlarmIntent(mContext);
        am.cancel(pi);
        am.setInexactRepeating(AlarmManager.RTC, System.currentTimeMillis(), intervalMillis, pi);
    }

    private static PendingIntent getAlarmIntent(Context context) {
        Intent intent = new Intent().setClass(context, WidgetProviderOysterSimple.class);
        intent.setAction(WidgetProviderOysterSimple.WIDGET_BUTTON_REFRESH);
        intent.putExtra("card-no", "");
        intent.putExtra("updateAll", true);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        return pi;
    }

    public void stopAlarm() {
        LogCustom.d(TAG_WIDGALARM,"stopAlarm enter");
        AlarmManager am = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
        am.cancel(getAlarmIntent(mContext));
    }
}
