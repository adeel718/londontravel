package uk.co.digifymedia.myoystercontactless.helpers;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import uk.co.digifymedia.myoystercontactless.FragmentCardsDetailed;
import uk.co.digifymedia.R;
import uk.co.digifymedia.myoystercontactless.model.CardDetailed;
import uk.co.digifymedia.myoystercontactless.model.contactless.ActiveCard;

/**
 * Created by adeel on 25/12/2016.
 */

public class CardsSummaryHelper {
    android.support.v4.app.FragmentManager fm;
    Activity activity;

    public CardsSummaryHelper(android.support.v4.app.FragmentManager fm, Activity activity) {
        this.activity = activity;
        this.fm = fm;
    }

    public static void sortCards(final List<CardDetailed> cardDetailedList) {
        Collections.sort(cardDetailedList, new Comparator<CardDetailed>() {
            @Override
            public int compare(CardDetailed fruit2, CardDetailed fruit1)
            {

                return  fruit1.getCardNumber().compareTo(fruit2.getCardNumber());
            }
        });
    }

    public void addCardToView(final LinearLayout mainLayout, final CardDetailed card) {

        final android.support.v4.app.FragmentManager fragmentManager = fm;
        final LayoutInflater inflater =
                (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View summaryCardLayout = inflater.inflate(R.layout.inflate_summary_card, mainLayout, false);

        TextView textViewCardNumber = (TextView) summaryCardLayout.findViewById(R.id.textViewCardNumber);
        TextView textViewCardBalance = (TextView) summaryCardLayout.findViewById(R.id.textViewBalance);
        TextView textViewCardType = (TextView) summaryCardLayout.findViewById(R.id.textViewCardType);

        textViewCardNumber.setText(card.getCardNumber());
        textViewCardBalance.setText(card.getBalance());
        textViewCardType.setText("OYSTER");

        summaryCardLayout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Fragment fragment = new FragmentCardsDetailed().newInstance(card.getCardNumber(), "oyster");

                fragmentManager.beginTransaction()
                        .replace(R.id.content_main_oyster, fragment)
                        .addToBackStack("Main")
                        .commit();
            }
        });

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //stuff that updates ui
                mainLayout.addView(summaryCardLayout);
            }
        });

    }

    public void addCardToView(final LinearLayout mainLayout, final ActiveCard card) {

        final android.support.v4.app.FragmentManager fragmentManager = fm;
        final LayoutInflater inflater =
                (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View summaryCardLayout = inflater.inflate(R.layout.inflate_summary_card, mainLayout, false);

        ImageView imageCardType = (ImageView) summaryCardLayout.findViewById(R.id.imageCardType);
        imageCardType.setBackgroundResource(R.drawable.ic_type_contactless);
        TextView textViewCardNumber = (TextView) summaryCardLayout.findViewById(R.id.textViewCardNumber);
        TextView textViewCardBalance = (TextView) summaryCardLayout.findViewById(R.id.textViewBalance);
        TextView textViewCardType = (TextView) summaryCardLayout.findViewById(R.id.textViewCardType);
        textViewCardNumber.setText(card.getFormattedCardXxxxName());
        textViewCardBalance.setText("");
        textViewCardType.setText("CONTACTLESS");

        // TODO functionality to implement at future release
        summaryCardLayout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(activity, "Note: You can't view payment history or journey history for Contactless cards yet. This will be implemented in a future release",
                        Toast.LENGTH_LONG).show();
            }
        });

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //stuff that updates ui
                mainLayout.addView(summaryCardLayout);
            }
        });

    }
}
