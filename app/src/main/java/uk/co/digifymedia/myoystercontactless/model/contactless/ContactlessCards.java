
package uk.co.digifymedia.myoystercontactless.model.contactless;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.orm.SugarRecord;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "error",
        "activeCards",
        "inactiveCards",
        "journeyServiceAvailable",
        "maxCardsToDisplay",
        "sdrEnabled"
})
public class ContactlessCards extends SugarRecord{
    @JsonProperty("error")
    private String error;
    @JsonProperty("activeCards")
    private List<ActiveCard> activeCards = null;
    @JsonProperty("inactiveCards")
    private List<Object> inactiveCards = null;
    @JsonProperty("journeyServiceAvailable")
    private Boolean journeyServiceAvailable;
    @JsonProperty("maxCardsToDisplay")
    private Integer maxCardsToDisplay;
    @JsonProperty("sdrEnabled")
    private Boolean sdrEnabled;

    @JsonProperty("activeCards")
    public List<ActiveCard> getActiveCards() {
        return activeCards;
    }

    @JsonProperty("activeCards")
    public void setActiveCards(List<ActiveCard> activeCards) {
        this.activeCards = activeCards;
    }

    @JsonProperty("inactiveCards")
    public List<Object> getInactiveCards() {
        return inactiveCards;
    }

    @JsonProperty("inactiveCards")
    public void setInactiveCards(List<Object> inactiveCards) {
        this.inactiveCards = inactiveCards;
    }

    @JsonProperty("journeyServiceAvailable")
    public Boolean getJourneyServiceAvailable() {
        return journeyServiceAvailable;
    }

    @JsonProperty("journeyServiceAvailable")
    public void setJourneyServiceAvailable(Boolean journeyServiceAvailable) {
        this.journeyServiceAvailable = journeyServiceAvailable;
    }

    @JsonProperty("maxCardsToDisplay")
    public Integer getMaxCardsToDisplay() {
        return maxCardsToDisplay;
    }

    @JsonProperty("maxCardsToDisplay")
    public void setMaxCardsToDisplay(Integer maxCardsToDisplay) {
        this.maxCardsToDisplay = maxCardsToDisplay;
    }

    @JsonProperty("sdrEnabled")
    public Boolean getSdrEnabled() {
        return sdrEnabled;
    }

    @JsonProperty("sdrEnabled")
    public void setSdrEnabled(Boolean sdrEnabled) {
        this.sdrEnabled = sdrEnabled;
    }

    @JsonProperty("error")
    public String getError() {
        return error;
    }

    @JsonProperty("error")
    public void setError(String error) {
        this.error = error;
    }

}
