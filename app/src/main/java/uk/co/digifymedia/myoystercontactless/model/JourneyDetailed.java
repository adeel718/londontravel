package uk.co.digifymedia.myoystercontactless.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.orm.SugarRecord;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "journey-id",
        "journey-is-summary",
        "journey-time",
        "journey-desc",
        "journey-charge",
        "journey-bal"
})
public class JourneyDetailed extends SugarRecord {

    private Long journeyLinkHistoryId;
    @JsonProperty("journey-time")
    private String journeyTime;

    @JsonProperty("journey-id")
    private String journeyId;

    @JsonProperty("journey-is-summary")
    private String journeyIsSummary;

    @JsonProperty("journey-desc")
    private String journeyDesc;
    @JsonProperty("journey-charge")
    private String journeyCharge;
    @JsonProperty("journey-bal")
    private String journeyBal;

    public Long getJourneyLinkHistoryId() {
        return journeyLinkHistoryId;
    }

    public void setJourneyLinkHistoryId(Long journeyLinkHistoryId) {
        this.journeyLinkHistoryId = journeyLinkHistoryId;
    }


    @JsonProperty("journey-id")
    public String getJourneyId() {
        return journeyId;
    }

    @JsonProperty("journey-id")
    public void setJourneyId(String journeyId) {
        this.journeyId = journeyId;
    }

    @JsonProperty("journey-is-summary")
    public String getJourneyIsSummary() {
        return journeyIsSummary;
    }

    @JsonProperty("journey-is-summary")
    public void setJourneyIsSummary(String journeyIsSummary) {
        this.journeyIsSummary = journeyIsSummary;
    }

    @JsonProperty("journey-time")
    public String getJourneyTime() {
        return journeyTime;
    }

    @JsonProperty("journey-time")
    public void setJourneyTime(String journeyTime) {
        this.journeyTime = journeyTime;
    }

    @JsonProperty("journey-desc")
    public String getJourneyDesc() {
        return journeyDesc;
    }

    @JsonProperty("journey-desc")
    public void setJourneyDesc(String journeyDesc) {
        this.journeyDesc = journeyDesc;
    }

    @JsonProperty("journey-charge")
    public String getJourneyCharge() {
        return journeyCharge;
    }

    @JsonProperty("journey-charge")
    public void setJourneyCharge(String journeyCharge) {
        this.journeyCharge = journeyCharge;
    }

    @JsonProperty("journey-bal")
    public String getJourneyBal() {
        return journeyBal;
    }

    @JsonProperty("journey-bal")
    public void setJourneyBal(String journeyBal) {
        this.journeyBal = journeyBal;
    }

}

