package uk.co.digifymedia.myoystercontactless;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import uk.co.digifymedia.R;

/**
 * Created by adeel on 03/12/2016.
 */

// This class will deal with internet connection
public class InternetConnection {
    static Logging l = new Logging();

    public static void showNoConnectionError(final Context context, final Activity activity){
        if (context != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {

                @Override
                public void run() {

                    l.logEvent("showNoConnectionError", "no network connection");

                    Toast.makeText(context, "There is an issue with your network, please try again later",
                            Toast.LENGTH_LONG).show();

                    if(activity != null) {
                        FrameLayout fl = (FrameLayout) activity.findViewById(R.id.frameLayoutLoading);
                        if (fl != null) {
                            fl.setVisibility(View.GONE);
                        }
                    }
                }
            });
        }
    }

    public static void showErrorMessageFromServer(final Context context, final String msg){
        if (context != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {

                @Override
                public void run() {

                    l.logEvent("showErrorMessageFromServer", msg);

                    Toast.makeText(context, msg,
                            Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    public static void showErrorMessage(final Context context, final String msg){
        if (context != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {

                @Override
                public void run() {

                    l.logEvent("showResponseErrorMessage", msg);

                    Toast.makeText(context, msg,
                            Toast.LENGTH_LONG).show();
                }
            });
        }
    }

}
