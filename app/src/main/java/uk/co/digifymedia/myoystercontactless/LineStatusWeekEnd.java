package uk.co.digifymedia.myoystercontactless;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import uk.co.digifymedia.R;
import uk.co.digifymedia.myoystercontactless.helpers.LineStatusHelper;
import uk.co.digifymedia.myoystercontactless.model.line_status_2.Wrapper;


public class LineStatusWeekEnd extends Fragment {

    private static final String ARG_PARAM_WEEKEND = "weekend";
    GetLineStatusInfo getLineStatusInfo;
    Boolean isWeekend;
    // create boolean for fetching data
    private boolean isViewShown = false;

    public LineStatusWeekEnd() {
        // Required empty public constructor
    }

    public static LineStatusWeekEnd newInstance(Boolean isWeekend) {
        LineStatusWeekEnd fragment = new LineStatusWeekEnd();
        Bundle args = new Bundle();
        args.putBoolean(ARG_PARAM_WEEKEND, isWeekend);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            isWeekend = getArguments().getBoolean(ARG_PARAM_WEEKEND);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_line_status_week_end, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        /*if (!isViewShown) {
            onLoadProcessing();
        }*/
    }

    private void onLoadProcessing(){
        TextView title = (TextView) getActivity().findViewById(R.id.textViewTitle);
        title.setText("Line Status Weekend");
        TextView textViewUpdateTime = (TextView) getView().findViewById(R.id.textViewLineStatusUpdateTime);
        textViewUpdateTime.setText("n/a");
        getLineStatusInfo = new GetLineStatusInfo(getChildFragmentManager(), getActivity(), getView());

        List<Wrapper> wrapperList = Wrapper.getWrapperList(isWeekend);

        LineStatusHelper lineStatusHelper = new LineStatusHelper();
        lineStatusHelper.populateLineStatusList(wrapperList, getActivity(), getView(), isWeekend);

        makeAndHandleRequest();

        SwipeRefreshLayout swipe_view = (SwipeRefreshLayout) getActivity().findViewById(R.id.swipeRefreshLineStatus);
        swipe_view.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
        {
            @Override
            public void onRefresh()
            {
                makeAndHandleRequest();
            }
        });
    }

    public void makeAndHandleRequest() {
        try{
            getLineStatusInfo.doGetRequest(getContext(), isWeekend);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (getView() != null) {
            isViewShown = true;
            // fetchdata() contains logic to show data when page is selected mostly asynctask to fill the data
            onLoadProcessing();
        } else {
            isViewShown = false;
        }
    }

}
