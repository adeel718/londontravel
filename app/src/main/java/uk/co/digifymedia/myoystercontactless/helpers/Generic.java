package uk.co.digifymedia.myoystercontactless.helpers;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;

import uk.co.digifymedia.londontravel.fragments.JourneyPlannerFragment;
import uk.co.digifymedia.myoystercontactless.FragmentCardsSummary;
import uk.co.digifymedia.myoystercontactless.FragmentLineStatus;
import uk.co.digifymedia.myoystercontactless.FragmentLogin;
import uk.co.digifymedia.myoystercontactless.Logout;
import uk.co.digifymedia.R;
import uk.co.digifymedia.myoystercontactless.SettingsFragment;
import uk.co.digifymedia.myoystercontactless.model.SummaryCards;

/**
 * Created by adeel on 27/12/2016.
 */

public class Generic {
    FragmentManager fm;
    Activity activity;

    public Generic(FragmentManager fm, Activity activity) {
        this.fm = fm;
        this.activity = activity;
    }

    public void checkIfLoginAccountActive(SummaryCards obj){
        if(obj.getError().toLowerCase().contains("password")) {
            FragmentManager f = fm;
            f.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            Logout.signoutAndClearAllDB();

            Fragment fragment = new FragmentLogin().newInstanceWithMessage("Your account password has changed or is inactive.");
            FragmentTransaction ft = fm.beginTransaction();

            fm.beginTransaction()
                    .replace(R.id.content_main_oyster, fragment)
                    .addToBackStack("Main")
                    .commit();
        }
    }

    public void setNavigationMenuUnauthorised(final NavigationView navigationView){
        navigationView.getMenu().clear(); //clear old inflated items.
        navigationView.inflateMenu(R.menu.activity_main_unauthorised); //inflate new items.
        navigationView.getMenu().getItem(0).setChecked(true);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(final MenuItem menuItem) {
                return onNavigationItemSelectedListener(menuItem, navigationView, false);
            }
        });
    }

    public void setNavigationMenuAuthorised(final NavigationView navigationView){
        navigationView.getMenu().clear(); //clear old inflated items.
        navigationView.inflateMenu(R.menu.activity_main_drawer_oyster); //inflate new items.

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(final MenuItem menuItem) {
                return onNavigationItemSelectedListener(menuItem, navigationView, true);
            }
        });
    }

    public boolean onNavigationItemSelectedListener(MenuItem item, final NavigationView navigationView, Boolean loggedIn) {
        int id = item.getItemId();
        final android.support.v4.app.FragmentManager fragmentManager = fm; // For AppCompat use getSupportFragmentManager
        Fragment fragment;
        // Handle navigation view item clicks here.

        if(loggedIn) {
            // update the main content by replacing fragments
            fragment = new FragmentCardsSummary().newInstance("oyster");
            if(id == R.id.nav_line_status) {
                fragment = new FragmentLineStatus();
            } else if (id == R.id.nav_cards_oyster) {
                fragment = new FragmentCardsSummary().newInstance("oyster");
            } else if (id == R.id.nav_cards_contactless) {
                fragment = new FragmentCardsSummary().newInstance("contactless");
            } else if (id == R.id.nav_signout) {
                AlertDialog alert = new AlertDialog.Builder(activity).create();
                alert.setTitle("Are you sure");
                alert.setMessage("Signout?");
                alert.setButton(Dialog.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //do stuff
                        Logout.signoutAndClearAllDB();
                        setNavigationMenuUnauthorised(navigationView);
                        fragmentManager.beginTransaction()
                                .replace(R.id.content_main_oyster, new FragmentLogin())
                                .addToBackStack("Main")
                                .commit();
                    }
                });
                alert.setCancelable(true);
                alert.show();

            }
            else if(id == R.id.nav_journey) {
                fragment = new JourneyPlannerFragment();
            }
            else {
                fragment = new SettingsFragment();
            }
        }
        else
        {
            if(id == R.id.nav_journey) {
                fragment = new JourneyPlannerFragment();
            }
            else  {
                fragment = new FragmentLogin();
            }
        }
        fragmentManager.beginTransaction()
                .replace(R.id.content_main_oyster, fragment)
                .addToBackStack("Main")
                .commit();
        DrawerLayout drawer = (DrawerLayout) activity.findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
