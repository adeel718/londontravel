package uk.co.digifymedia.myoystercontactless.helpers;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import uk.co.digifymedia.R;
import uk.co.digifymedia.myoystercontactless.widget.ConfigurationActivity;

import static uk.co.digifymedia.myoystercontactless.widget.WidgetProviderOysterSimple.WIDGET_BUTTON_REFRESH;

/**
 * Created by adeel on 15/01/2017.
 */

public class WidgetHelper {
    public static void setIntentAndWidgetOnClickListeners(int appWidgetId, String selectedCardNo, RemoteViews remoteViews, Context context) {
        Intent intent;
        intent = new Intent(WIDGET_BUTTON_REFRESH);
        //intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, incomingAppWidgetIds);
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);

        intent.putExtra("card-no", selectedCardNo);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, appWidgetId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.actionButtonRefresh, pendingIntent);

        Intent intentConfigure = new Intent(context, ConfigurationActivity.class);
        intentConfigure.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);    //set widget id
        PendingIntent pendingIntentConfigure = PendingIntent.getActivity(context, appWidgetId, intentConfigure, 0);
        remoteViews.setOnClickPendingIntent(R.id.actionButtonSettings, pendingIntentConfigure);
    }
}
