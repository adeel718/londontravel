package uk.co.digifymedia.myoystercontactless;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import uk.co.digifymedia.R;
import uk.co.digifymedia.myoystercontactless.helpers.LineStatusHelper;
import uk.co.digifymedia.myoystercontactless.helpers.LogCustom;
import uk.co.digifymedia.myoystercontactless.model.line_status_2.LineStatus;
import uk.co.digifymedia.myoystercontactless.model.line_status_2.Wrapper;


import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static uk.co.digifymedia.myoystercontactless.Urls.URL_LINE_STATUS;
import static uk.co.digifymedia.myoystercontactless.Urls.URL_LINE_STATUS_WE;
import static uk.co.digifymedia.myoystercontactless.helpers.DBDeleteQueries.deleteInfoWeekDay;
import static uk.co.digifymedia.myoystercontactless.helpers.DBDeleteQueries.deleteInfoWeekEnd;

/**
 * Created by XPS15-9550 on 14/05/2017.
 */

public class GetLineStatusInfo {
    OkHttpClient client = null;

    FragmentManager fragmentManager;

    Activity activity;

    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }

    View view;

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public FragmentManager getFragmentManager() {
        return fragmentManager;
    }

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    public GetLineStatusInfo(FragmentManager fragmentManager, Activity activity, View view){
        this.fragmentManager = fragmentManager;
        this.activity = activity;
        this.view = view;
        client = new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)

                .build();
    }

    public void doGetRequest(final Context context, final Boolean weekend) throws Exception {
        progressBarVisibility(VISIBLE);

        final InternetConnection connection = new InternetConnection();
        Request request;
        if(weekend){
            request = new Request.Builder()
                    .url(URL_LINE_STATUS_WE)
                    .build();
        }
        else {
            request = new Request.Builder()
                    .url(URL_LINE_STATUS)
                    .build();
        }
        Log.d("line status url is", request.url().toString());

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
                progressBarVisibility(GONE);
                connection.showNoConnectionError(context, activity);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String responseBody = response.body().string();
                final String resp = responseBody;
                LogCustom.d("line status : resp ", resp);

                final LineStatusHelper lineStatusHelper = new LineStatusHelper();

                ObjectMapper mapper = new ObjectMapper();

                try {
                    //Wrapper wrapper = gson.fromJson(resp, Wrapper.class);
                    List<Wrapper> obj = Arrays.asList(
                            mapper.readValue(resp, Wrapper[].class)
                    );

                    if(weekend){
                        deleteInfoWeekEnd();
                    }
                    else {
                        deleteInfoWeekDay();
                    }

                    lineStatusHelper.clearView(getActivity(), getView());

                    for (Wrapper wrapper : obj) {
                        LogCustom.d("line status : line wrapper save", wrapper.getActualId());
                        final List<LineStatus> arrayOfLineStatus = wrapper.getLineStatuses();
                        wrapper.setIdLine(wrapper.getActualId());
                        wrapper.setWeekend(weekend);
                        DateFormat dateFormat = new SimpleDateFormat("dd MMM HH:mm");
                        Date date = new Date();
                        wrapper.setLastUpdate(dateFormat.format(date));
                        wrapper.save();

                        for (LineStatus lineStatus : arrayOfLineStatus) {
                            LogCustom.d("line status : line status save", lineStatus.getLineId());
                            lineStatus.setLinkedLineId(wrapper.getActualId());
                            lineStatus.setWeekend(weekend);
                            lineStatus.setLineName(wrapper.getName());
                            lineStatus.save();
                        }
                    }

                    lineStatusHelper.populateLineStatusList(obj, activity, view, weekend);

                    progressBarVisibility(GONE);
                }
                catch(Exception e){
                    e.printStackTrace();
                    progressBarVisibility(GONE);
                    connection.showNoConnectionError(context, activity);
                }
            }
        });

    }

    private void progressBarVisibility(final int visibility) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                SwipeRefreshLayout swipe_view = (SwipeRefreshLayout) getActivity().findViewById(R.id.swipeRefreshLineStatus);
                if(swipe_view != null) {
                    if (visibility == GONE) {
                        if (swipe_view.isRefreshing()) {
                            swipe_view.setRefreshing(false);
                        }
                    } else if (visibility == VISIBLE) {
                        swipe_view.setRefreshing(true);
                    }
                }
            }
        });
    }

}
