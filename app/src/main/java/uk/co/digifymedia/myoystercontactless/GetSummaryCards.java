package uk.co.digifymedia.myoystercontactless;


import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RemoteViews;
import android.widget.TextView;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.lang3.time.DateUtils;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import uk.co.digifymedia.R;
import uk.co.digifymedia.myoystercontactless.helpers.CardsDetailedDetailedHelper;
import uk.co.digifymedia.myoystercontactless.helpers.CardsSummaryHelper;
import uk.co.digifymedia.myoystercontactless.helpers.DBDeleteQueries;
import uk.co.digifymedia.myoystercontactless.helpers.Generic;
import uk.co.digifymedia.myoystercontactless.helpers.JourneyHistoryHelper;
import uk.co.digifymedia.myoystercontactless.helpers.LogCustom;
import uk.co.digifymedia.myoystercontactless.model.CardDetailed;
import uk.co.digifymedia.myoystercontactless.model.Login;
import uk.co.digifymedia.myoystercontactless.model.SummaryCards;
import uk.co.digifymedia.myoystercontactless.model.WidgetStorage;
import uk.co.digifymedia.myoystercontactless.model.contactless.ActiveCard;
import uk.co.digifymedia.myoystercontactless.model.contactless.ContactlessCards;
import uk.co.digifymedia.myoystercontactless.widget.WidgetProviderOysterSimple;

import static uk.co.digifymedia.myoystercontactless.Urls.URL_LOGIN;
import static uk.co.digifymedia.myoystercontactless.helpers.CardsSummaryHelper.sortCards;
import static uk.co.digifymedia.myoystercontactless.helpers.DBSaveQueries.saveCardTicketsToDB;

/**
 * Created by adeel on 03/12/2016.
 */

public class GetSummaryCards {
    static String TAG_GETSM_GENERIC = "GSC/generic";
    static String TAG_GETSM_DOGETREQUEST = "GSC/login-get-oyster";
    static String TAG_GETSM_DOGETREQUESTUPDATE = "GSC/card-upd-oyster";
    static String TAG_GETSM_DOGETREQUESTUPDATECONTACTLESS = "GSC/card-upd-contactlss";
    static String TAG_GETSM_DOGETREQUESTUPDATEWIDG = "GSC/widget-update";

    static String switchFirstLogin = "FIRST";
    String switchUpdateOyster = "UPDATE-OYSTER";
    String switchUpdateContactless = "UPDATE-CONTACTLESS";
    String switchUpdateBoth = "UPDATE-BOTH";
    String switchUpdateWidget = "UPDATE-WIDGET";

    OkHttpClient client = new OkHttpClient.Builder()
            .connectTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .build();

    FragmentManager fragmentManager;
    Activity activity;
    View view;
    Context context;
    FrameLayout frameLoading;

    // Required classes
    CardsDetailedDetailedHelper cardsDetailedDetailedHelper;
    CardsSummaryHelper cardsSummaryHelper;
    Generic generic;

    // Constructor
    public GetSummaryCards(FragmentManager fragmentManager, Activity activity, View view, Context contextOver) {
        this.fragmentManager = fragmentManager;
        this.activity = activity;
        this.view = view;

        // Override with context if one was passed in
        if(contextOver != null){
            context = contextOver;
        }
        // Widget won't have activity
        if(activity != null) {
            frameLoading = (FrameLayout) activity.findViewById(R.id.frameLayoutLoading);
            context = activity.getBaseContext();
        }

        cardsDetailedDetailedHelper = new CardsDetailedDetailedHelper(activity, fragmentManager, view);
        cardsSummaryHelper = new CardsSummaryHelper(fragmentManager, activity);
        generic = new Generic(fragmentManager, activity);
    }


    public void doGetRequest(String url, final Login login, final String oysterOrContactless) throws Exception {
        Request request = createRequestLoginCardDetails(url, login);

        showLoadingIconStatusBar(View.VISIBLE, false, -1);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                onFailureHandling(switchFirstLogin, e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                onResponseHandling(switchUpdateBoth, response, login, oysterOrContactless);
            }
        });

    }


    public void doGetRequestUpdateCards(String url, final Login login, final Boolean forceRefresh) throws Exception {
        // either refresh times was x Mins ago or forceRefresh is true
        if((checkIfRefreshShouldHappen()) || (forceRefresh)) {
            LogCustom.d(TAG_GETSM_DOGETREQUESTUPDATE, "checkIfRefreshShouldHappen is true or forceRefresh is true");
            showLoadingIconStatusBar(View.VISIBLE, false, -1);
            Request request = createRequestUpdateCardDetails(url);

            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    onFailureHandling(switchUpdateOyster, e);
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    onResponseHandling(switchUpdateOyster, response, login, null);
                    final InternetConnection connection = new InternetConnection();
                    // Also refresh journey history in bg if on
                    SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
                    boolean turnedOn = Boolean.parseBoolean(sp.getString("pref_sync_app_jrefresh","true"));
                    try {
                        if (turnedOn && view != null) {
                            Snackbar snackbar = Snackbar
                                    .make(view, "Your journey history is being updated", Snackbar.LENGTH_LONG);

                            snackbar.show();
                            final JourneyHistoryHelper jh = new JourneyHistoryHelper();
                            jh.fetchAllJourneyHistoryBg(activity, fragmentManager);
                        }
                    }
                    catch (Exception e){
                        Log.d(TAG_GETSM_DOGETREQUESTUPDATE, e.getMessage());
                    }
                }
            });
        }
        else {
            // not reached time to refresh yet, don't do anything
            LogCustom.d(TAG_GETSM_DOGETREQUESTUPDATE, "no refresh required");
            showLoadingIconStatusBar(View.GONE, false, -1);
        }
    }

    public void doGetRequestUpdateCardsContactless(String url, final Login login, final Boolean forceRefresh) throws Exception {
        // either refresh times was x Mins ago or forceRefresh is true
        LogCustom.d(TAG_GETSM_DOGETREQUESTUPDATECONTACTLESS, "checkIfRefreshShouldHappen is true or forceRefresh is true");
        if((checkIfRefreshShouldHappen()) || (forceRefresh)) {
            showLoadingIconStatusBar(View.VISIBLE, false, -1);
            Request request = createRequestLoginCardDetails(url, login);

            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    onFailureHandling(switchUpdateContactless, e);
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    onResponseHandling(switchUpdateContactless, response, login, null);
                }

            });
        }
        else {
            // not reached time to refresh yet, don't do anything
            LogCustom.d(TAG_GETSM_DOGETREQUESTUPDATECONTACTLESS, "no refresh required");
            showLoadingIconStatusBar(View.GONE, false, -1);
        }
    }

    public void doGetRequestUpdateCardsWidget(final String url, final Login login,
                                              final RemoteViews remoteViews, final String selCardNo,
                                              final AppWidgetManager appWidgetManager, final int appWidgetId,
                                              final Boolean sessionTimedOut) throws Exception {

        Request request;
        if(sessionTimedOut){
            // session has time out, login again and get cards
            HttpUrl.Builder urlBuilder = HttpUrl.parse(URL_LOGIN).newBuilder();
            String loginUrl = urlBuilder.build().toString();
            LogCustom.d(TAG_GETSM_DOGETREQUESTUPDATEWIDG, "session was timed out set url to " + URL_LOGIN);
            request = createRequestLoginCardDetails(loginUrl, login);
        } else {
            LogCustom.d(TAG_GETSM_DOGETREQUESTUPDATEWIDG, "session still exists url is " + url);
            request = createRequestUpdateCardDetails(url);
        }

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                onFailureHandling(switchUpdateWidget, e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String responseBody = response.body().string();
                LogCustom.d(TAG_GETSM_DOGETREQUESTUPDATEWIDG, "resp raw" + responseBody);

                remoteViews.setImageViewResource(R.id.actionButtonRefresh, R.drawable.widget_ic_menu_refresh);
                if (!response.isSuccessful()) {
                    LogCustom.d(TAG_GETSM_DOGETREQUESTUPDATEWIDG, "server error" + response.code());
                    InternetConnection.showErrorMessage(context, "Server error code " + response.code());
                    remoteViews.setImageViewResource(R.id.actionButtonRefresh, R.drawable.widget_ic_menu_refresh);
                } else {
                    ObjectMapper mapper = new ObjectMapper();
                    SummaryCards obj = mapper.readValue(responseBody, SummaryCards.class);

                    if (obj.getError() != null) {
                        LogCustom.d(TAG_GETSM_DOGETREQUESTUPDATEWIDG, "error message from server" + obj.getError());
                        showErrorMessageFromServer(obj);
                        remoteViews.setImageViewResource(R.id.actionButtonRefresh, R.drawable.widget_ic_menu_refresh);

                        // check if session persists
                        checkSessionPersistsProcess(obj, url, login, remoteViews, selCardNo, appWidgetManager, appWidgetId);
                    } else {
                        DBDeleteQueries.deleteLoginDetailsFromDB(login);
                        DBDeleteQueries.deleteOysterCardSummaryAndDetailsFromDB();
                        DBDeleteQueries.deleteCardTicketsFromDB();

                        storeLoginAndSummary(obj, login);

                        // save card detailed info
                        for (final CardDetailed card : obj.getDetail().getCards()) {
                            LogCustom.d(TAG_GETSM_DOGETREQUESTUPDATEWIDG, "saving card detailed: " + card.getCardNumber());
                            card.setBalance(card.getBalance().replace("&#163;", "£"));
                            card.setNextRefreshTime(getCurrentTime(true));
                            card.setLastRefreshTime(getCurrentTime(false));
                            card.save();

                            saveCardTicketsToDB(card.getCardNumber(), card.getTickets());

                            // set widget text-views
                            if(selCardNo.equals(card.getCardNumber())) {
                                LogCustom.d(TAG_GETSM_DOGETREQUESTUPDATEWIDG, "set widget info card info");
                                remoteViews.setTextViewText(R.id.textViewCardNumber, card.getCardNumber());
                                remoteViews.setTextViewText(R.id.textViewBalance, card.getBalance());
                            }

                            // set widget text-views
                            if(selCardNo.equals(card.getCardNumber())) {
                                LogCustom.d(TAG_GETSM_DOGETREQUESTUPDATEWIDG, "set widget info refresh time");
                                remoteViews.setTextViewText(R.id.textViewLastUpdate, card.getLastRefreshTime());
                            }
                        }

                        remoteViews.setImageViewResource(R.id.actionButtonRefresh, R.drawable.widget_ic_menu_refresh);
                        appWidgetManager.updateAppWidget(appWidgetId, remoteViews);

                        // now we update all widgets info
                        updateAllWidgets(R.layout.widget_oyster_simple, WidgetProviderOysterSimple.class);

                    }
                }

            }

        });

    }

    private void checkSessionPersistsProcess(SummaryCards obj, String url, Login login, RemoteViews remoteViews, String selCardNo, AppWidgetManager appWidgetManager, int appWidgetId) {
        if(obj.getError().equals("Session timed out")){
            LogCustom.d(TAG_GETSM_DOGETREQUESTUPDATEWIDG, "session has timed out");
            try {
                LogCustom.d(TAG_GETSM_DOGETREQUESTUPDATEWIDG, "session timed out set var sessionTimedOut to true");
                doGetRequestUpdateCardsWidget(url, login, remoteViews, selCardNo, appWidgetManager, appWidgetId, true);
            }
            catch (Exception e){
                LogCustom.d(TAG_GETSM_DOGETREQUESTUPDATEWIDG, "exception when call doGetRequestUpdateCardsWidget", e);
                Logging.logError(this.getClass().getSimpleName() + "/3", e);
                showLoadingIconStatusBar(View.GONE, false, -1);
                e.printStackTrace();
            }
        }
    }

    private void showLoadingIconStatusBar(final int visibility, final Boolean showLoading, final int visibilityLoadingLayout) {
        // null check, might be from widget
        if(activity != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // only set visibility on loading overlay if true
                    if(showLoading) {
                        LogCustom.d(TAG_GETSM_GENERIC, "set loading overlay to " + visibilityLoadingLayout);
                        frameLoading.setVisibility(visibilityLoadingLayout);
                    }

                    LogCustom.d(TAG_GETSM_GENERIC, "set progressBarLoading icon to " + visibility);
                    final ProgressBar progressBar = (ProgressBar) activity.findViewById(R.id.progressBarLoading);
                    progressBar.setVisibility(visibility);
                }
            });
        }
    }

    private String getCurrentTime(Boolean nextRefreshIncrement){
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date));

        if(nextRefreshIncrement) {
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this.context);
            int minutesToAdd = Integer.parseInt(sp.getString("pref_sync_app_time","180"));

            LogCustom.d(TAG_GETSM_GENERIC, "nextRefresh time true add mins " + minutesToAdd);
            return dateFormat.format(DateUtils.addMinutes(date, minutesToAdd));
        } else{
            LogCustom.d(TAG_GETSM_GENERIC, "nextRefresh time false " + date);
            return dateFormat.format(date);
        }
    }

    private Boolean checkIfRefreshShouldHappen(){
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();

        String current = dateFormat.format(date) ;
        Boolean retValue = true;

        CardDetailed card = null;
        try{
            card = CardDetailed.first(CardDetailed.class);
        }
        catch(Exception e){
            LogCustom.d(TAG_GETSM_GENERIC, "exception when getting card " + e);
            Logging.logError(this.getClass().getSimpleName() + "/1", e);
            e.printStackTrace();
        }
        if(card!=null){
            try {
                Date lastCardRefDate = (Date)dateFormat.parse(card.getNextRefreshTime());
                Date cur = (Date)dateFormat.parse(current);

                LogCustom.d(TAG_GETSM_GENERIC, "refresh to happen on: " + lastCardRefDate);
                LogCustom.d(TAG_GETSM_GENERIC, "current time is: " + cur);

                if(cur.after(lastCardRefDate)){
                    retValue = true;
                    LogCustom.d(TAG_GETSM_GENERIC, "refresh set to true");
                }
                else
                {
                    retValue = false;
                    LogCustom.d(TAG_GETSM_GENERIC, "refresh set to false");
                }
            } catch (ParseException e) {
                LogCustom.d(TAG_GETSM_GENERIC, "exception " + e);
                Logging.logError(this.getClass().getSimpleName() + "/2", e);
                e.printStackTrace();
            }
        }

        return retValue;
    }

    private void storeLoginAndSummary(SummaryCards obj, Login login) {
        // store login credentials
        if((obj.getName()) != null && !(obj.getName().equals(""))) {
            login.setName(obj.getName());
        }
        login.setId((long) 1);
        if((obj.getLoginId() != null) && !(obj.getLoginId().equals(""))) {
            login.setUniqueLoginId(obj.getLoginId());
        }
        login.setPassword(login.getPassword()); // already encrypted
        login.save();
        HashMap<String, String> hm = new HashMap<String, String>();
        hm.put("email", login.getEmail());
        hm.put("password", login.getPassword());
        hm.put("uniqueId", login.getUniqueLoginId());
        LogCustom.d(TAG_GETSM_GENERIC, hm);

        // store the summary info
        obj.delete();
        obj.setCardType("oyster");
        obj.save();

    }

    private void storeContactlessCards(ContactlessCards obj) {
        // store contactless cards
        obj.delete();
        obj.save();
        LogCustom.d(TAG_GETSM_GENERIC, "save contactless card " + obj.toString());
    }

    public void updateAllWidgets(final int layoutResourceId,
                                 final Class<? extends AppWidgetProvider> appWidgetClass)
    {
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), layoutResourceId);

        AppWidgetManager manager = AppWidgetManager.getInstance(context);
        final int[] appWidgetIds = manager.getAppWidgetIds(new ComponentName(context, appWidgetClass));

        // update all widget details
        for (int i = 0; i < appWidgetIds.length; ++i)
        {
            int appWidgetId = appWidgetIds[i];
            LogCustom.d(TAG_GETSM_GENERIC, "updating widget id" + appWidgetId);
            Bundle w = manager.getAppWidgetOptions(appWidgetId);
            List<WidgetStorage> ws = WidgetStorage.find(WidgetStorage.class, "APP_WIDGET_ID = ?", String.valueOf(appWidgetId));

            if(ws != null && ws.size() > 0) {
                List<CardDetailed> cardList = CardDetailed.find(CardDetailed.class, "CARD_NUMBER = ?", ws.get(0).getCardNumber());
                if(cardList != null && cardList.size() > 0) {
                    CardDetailed card = cardList.get(0);

                    LogCustom.d(TAG_GETSM_GENERIC, "updating widget card no " + card.getCardNumber());

                    // set widget text-views
                    remoteViews.setTextViewText(R.id.textViewCardNumber, card.getCardNumber());
                    remoteViews.setTextViewText(R.id.textViewBalance, card.getBalance());
                    remoteViews.setTextViewText(R.id.textViewLastUpdate, card.getLastRefreshTime());
                }
            }

            manager.updateAppWidget(appWidgetIds[i], remoteViews);
        }
    }

    private void updateHeaderViewAccountDetails(final Login login) {
        LogCustom.d(TAG_GETSM_GENERIC, "update nav header details");
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //stuff that updates ui
                View headerView = activity.findViewById(R.id.linearLayoutNavHeader);

                TextView textViewNavAccountEmail = (TextView) headerView.findViewById(R.id.textViewNavAccountEmail);
                TextView textViewNavAccountName = (TextView) headerView.findViewById(R.id.textViewNavAccountName);
                textViewNavAccountEmail.setText(login.getEmail());
                textViewNavAccountName.setText(login.getName());
            }
        });
    }

    private void showErrorMessageFromServer(SummaryCards obj) {
        InternetConnection.showErrorMessageFromServer(context, obj.getError());
    }

    private Request createRequestLoginCardDetails(String url, Login login) {
        String id = "";
        if(login.getUniqueLoginId() != null && login.getUniqueLoginId().length()>0){
            id = login.getUniqueLoginId();
        }
        RequestBody formBody = new FormBody.Builder()
                .add("username", login.getEmail())
                .add("password", login.getPassword())
                .add("id", id)
                .build();

        Request req =  new Request.Builder()
                .url(url)
                .post(formBody)
                .build();

        HashMap<String, String> hm = new HashMap<String, String>();
        hm.put("email", login.getEmail());
        hm.put("password", login.getPassword());
        hm.put("uniqueId", id);
        LogCustom.d("created login request ", hm);

        return req;
    }

    private Request createRequestUpdateCardDetails(String url) {

        Login login = Login.first(Login.class);

        if(login.getUniqueLoginId() == null){
            LogCustom.d(TAG_GETSM_GENERIC, "could not find id " + login);
            url = URL_LOGIN;
            return createRequestLoginCardDetails(url, login);
        }
        else {
            RequestBody formBody = new FormBody.Builder()
                    .add("id", login.getUniqueLoginId())
                    .build();
            LogCustom.d(TAG_GETSM_GENERIC, "created update card request " + formBody.toString());
            return new Request.Builder()
                    .url(url)
                    .post(formBody)
                    .build();
        }
    }

    private void switchFragment(Fragment fragment) {

        FragmentTransaction ft = fragmentManager.beginTransaction();
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

        fragmentManager.beginTransaction()
                .replace(R.id.content_main_oyster, fragment)
                .commitAllowingStateLoss();
    }


    // REFACTORING
    private void onFailureHandling(String callMode, Exception e) {

        switch(callMode) {
            case "FIRST" :
                // Statements
                LogCustom.d(TAG_GETSM_DOGETREQUEST, "call failed", e);
                e.printStackTrace();
                showLoadingIconStatusBar(View.GONE, true, View.GONE);
                InternetConnection.showNoConnectionError(context, activity);
                break; // optional

            case "UPDATE-OYSTER" :
                LogCustom.d(TAG_GETSM_DOGETREQUESTUPDATE, "call failed", e);
                e.printStackTrace();
                InternetConnection.showNoConnectionError(context, activity);
                showLoadingIconStatusBar(View.GONE, false, -1);
                break; // optional

            case "UPDATE-CONTACTLESS" :
                LogCustom.d(TAG_GETSM_DOGETREQUESTUPDATECONTACTLESS, "call failed", e);
                e.printStackTrace();
                InternetConnection.showNoConnectionError(context, activity);
                showLoadingIconStatusBar(View.GONE, false, -1);
                break; // optional

            case "UPDATE-WIDGET" :
                LogCustom.d(TAG_GETSM_DOGETREQUESTUPDATEWIDG, "call failed", e);
                e.printStackTrace();
                InternetConnection.showNoConnectionError(context, activity);
                break; // optional
        }
    }

    private void onResponseHandling(String callMode, Response response, Login login, String oysterOrContactless) throws IOException {
        String responseBody;
        ObjectMapper mapper = new ObjectMapper();

        try {
            switch (callMode) {
                case "UPDATE-BOTH":
                    responseBody = response.body().string();
                    LogCustom.d(TAG_GETSM_DOGETREQUEST, "resp raw" + responseBody);

                    showLoadingIconStatusBar(View.GONE, true, View.GONE);

                    if (!response.isSuccessful()) {
                        LogCustom.d(TAG_GETSM_DOGETREQUEST, "server error" + response.code());
                        InternetConnection.showErrorMessage(context, "Server error code " + response.code());
                    }

                    Fragment fragment = new FragmentCardsSummary().newInstance("oyster");

                    // If requested oyster cards
                    if (oysterOrContactless.equals("oyster")) {
                        SummaryCards obj = mapper.readValue(responseBody, SummaryCards.class);
                        if (obj.getError() != null) {
                            LogCustom.d(TAG_GETSM_DOGETREQUEST, "error message from server 0" + obj.getError());
                            showErrorMessageFromServer(obj);
                        } else {
                            // wrap deletion in try catch if table not exist
                            try {
                                LogCustom.d(TAG_GETSM_DOGETREQUEST, "is oyster, deleting");
                                DBDeleteQueries.deleteLoginDetailsFromDB(login);
                                DBDeleteQueries.deleteOysterCardSummaryAndDetailsFromDB();
                                DBDeleteQueries.deleteCardTicketsFromDB();
                            } catch (Exception e) {
                                Logging.logError(this.getClass().getSimpleName() + "/4", e);
                                LogCustom.d(TAG_GETSM_DOGETREQUEST, "exception when deleting");
                                e.printStackTrace();
                            }

                            storeLoginAndSummary(obj, login);

                            // save card detailed info
                            for (CardDetailed card : obj.getDetail().getCards()) {
                                LogCustom.d(TAG_GETSM_DOGETREQUEST, "saving card detailed: " + card.getCardNumber());

                                card.setBalance(card.getBalance().replace("&#163;", "£"));
                                card.setNextRefreshTime(getCurrentTime(true));
                                card.setLastRefreshTime(getCurrentTime(false));
                                card.save();

                                saveCardTicketsToDB(card.getCardNumber(), card.getTickets());
                            }

                            // there were no cards
                            if (obj.getDetail().getCards() == null || obj.getDetail().getCards().size() == 0) {
                                LogCustom.d(TAG_GETSM_DOGETREQUEST, "no cards show message");
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        //stuff that updates ui
                                        TextView textViewNoCardsToDisplay = (TextView) activity.findViewById(R.id.textViewNoCardsToDisplay);
                                        textViewNoCardsToDisplay.setVisibility(View.VISIBLE);
                                    }
                                });
                            }

                            updateHeaderViewAccountDetails(login);

                            switchFragment(fragment);
                        }
                    }
                    // else if request to get contactless cards
                    else {
                        showLoadingIconStatusBar(View.GONE, false, -1);
                        ContactlessCards obj = mapper.readValue(responseBody, ContactlessCards.class);
                        if (obj.getError() != null) {
                            LogCustom.d(TAG_GETSM_DOGETREQUEST, "error message from server 1" + obj.getError());
                            //showErrorMessageFromServer(obj, context);
                        } else {
                            // wrap deletion in try catch if table not exist
                            try {
                                LogCustom.d(TAG_GETSM_DOGETREQUEST, "is contactless, deleting");
                                DBDeleteQueries.deleteContactlessCardSummaryAndDetailsFromDB();
                            } catch (Exception e) {
                                Logging.logError(this.getClass().getSimpleName() + "/5", e);
                                LogCustom.d(TAG_GETSM_DOGETREQUEST, "exception when deleting");
                                e.printStackTrace();
                            }

                            //storeLoginAndSummary(obj, login);

                            final LinearLayout mainLayout = (LinearLayout) activity.findViewById(R.id.linearLayoutSummaryCardsContainer);
                            final FrameLayout frameLayout = (FrameLayout) activity.findViewById(R.id.frameLayoutSummaryCardsContainer);

                            // might be on different fragment by now so check if still on summary
                            if (mainLayout != null) {
                                LogCustom.d(TAG_GETSM_DOGETREQUEST, "remove existing cards");
                                // remove any existing cards displayed first
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        //stuff that updates ui
                                        if (((LinearLayout) mainLayout).getChildCount() > 0) {
                                            ((LinearLayout) mainLayout).removeAllViews();
                                        }
                                    }
                                });
                            }

                            // save card summary
                            for (final ActiveCard card : obj.getActiveCards()) {
                                LogCustom.d(TAG_GETSM_DOGETREQUEST, "saving active contactless card" + card);
                                card.save();

                                // might be on different fragment by now so check if still on summary
                                if (mainLayout != null) {
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            LogCustom.d(TAG_GETSM_DOGETREQUEST, "add active contactless card to view");
                                            //stuff that updates ui
                                            cardsSummaryHelper.addCardToView(mainLayout, card);
                                        }
                                    });

                                }
                            }

                            // there were no cards
                            if (obj.getActiveCards() == null || obj.getActiveCards().size() == 0) {
                                LogCustom.d(TAG_GETSM_DOGETREQUEST, "no cards for contactless show message");
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        //stuff that updates ui
                                        TextView textViewNoCardsToDisplay = (TextView) frameLayout.findViewById(R.id.textViewNoCardsToDisplay);
                                        textViewNoCardsToDisplay.setVisibility(View.VISIBLE);
                                    }
                                });
                            }

                            updateHeaderViewAccountDetails(login);

                        }
                    }
                    break;

                case "UPDATE-OYSTER":
                    responseBody = response.body().string();
                    LogCustom.d(TAG_GETSM_DOGETREQUESTUPDATE, "resp raw" + responseBody);

                    showLoadingIconStatusBar(View.GONE, false, -1);
                    if (!response.isSuccessful()) {
                        LogCustom.d(TAG_GETSM_DOGETREQUESTUPDATE, "server error" + response.code());
                        InternetConnection.showErrorMessage(context, "Server error code " + response.code());
                    } else {
                        SummaryCards obj = mapper.readValue(responseBody, SummaryCards.class);

                        if (obj.getError() != null) {
                            LogCustom.d(TAG_GETSM_DOGETREQUESTUPDATE, "error message from server" + obj.getError());

                            // check if account still active
                            generic.checkIfLoginAccountActive(obj);

                            // check if session persists
                            if (obj.getError().equals("Session timed out")) {
                                LogCustom.d(TAG_GETSM_DOGETREQUESTUPDATE, "session was timed out" + obj.getError());
                                HttpUrl.Builder urlBuilder = HttpUrl.parse(URL_LOGIN).newBuilder();
                                String url = urlBuilder.build().toString();
                                try {
                                    LogCustom.d(TAG_GETSM_DOGETREQUESTUPDATE, "session was timed out, call doGetRequest");
                                    doGetRequest(url, login, "oyster");
                                } catch (Exception e) {
                                    Logging.logError(this.getClass().getSimpleName() + "/6", e);
                                    LogCustom.d(TAG_GETSM_DOGETREQUESTUPDATE, "failed call doGetRequest", e);
                                    showLoadingIconStatusBar(View.GONE, false, -1);
                                    e.printStackTrace();
                                }
                            } else {
                                InternetConnection.showErrorMessageFromServer(context, obj.getError());
                            }
                        } else {
                            DBDeleteQueries.deleteLoginDetailsFromDB(login);
                            DBDeleteQueries.deleteOysterCardSummaryAndDetailsFromDB();
                            DBDeleteQueries.deleteCardTicketsFromDB();

                            storeLoginAndSummary(obj, login);

                            final LinearLayout mainLayout = (LinearLayout) activity.findViewById(R.id.linearLayoutSummaryCardsContainer);
                            final LinearLayout mainLayoutDetailed = (LinearLayout) activity.findViewById(R.id.layoutContainer);
                            final FrameLayout frameLayout = (FrameLayout) activity.findViewById(R.id.frameLayoutSummaryCardsContainer);

                            // might be on different fragment by now so check if still on summary
                            if (mainLayout != null) {
                                // remove any existing cards displayed first
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        //stuff that updates ui
                                        if (((LinearLayout) mainLayout).getChildCount() > 0) {
                                            ((LinearLayout) mainLayout).removeAllViews();
                                        }
                                    }
                                });
                            }

                            List<CardDetailed> cardDetailedList = obj.getDetail().getCards();
                            sortCards(cardDetailedList);

                            // save card detailed info
                            for (final CardDetailed card : cardDetailedList) {
                                LogCustom.d(TAG_GETSM_DOGETREQUESTUPDATE, "saving card detailed: " + card.getCardNumber());
                                card.setBalance(card.getBalance().replace("&#163;", "£"));
                                card.setNextRefreshTime(getCurrentTime(true));
                                card.setLastRefreshTime(getCurrentTime(false));
                                card.save();

                                saveCardTicketsToDB(card.getCardNumber(), card.getTickets());

                                // might be on different fragment by now so check if still on summary
                                if (mainLayout != null) {
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            //stuff that updates ui
                                            cardsSummaryHelper.addCardToView(mainLayout, card);
                                        }
                                    });

                                }

                                // might be on different fragment by now so check if still on detail
                                if (mainLayoutDetailed != null) {
                                    LogCustom.d(TAG_GETSM_DOGETREQUESTUPDATE, "check and still on detail screen");
                                    final TextView textViewCardNo = (TextView) mainLayoutDetailed.findViewById(R.id.textViewCardNumber);
                                    if (textViewCardNo.equals(card.getCardNumber())) {
                                        LogCustom.d(TAG_GETSM_DOGETREQUESTUPDATE, "update card on detail screen" + card);
                                        activity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                // if on detailed screen refresh this data
                                                cardsDetailedDetailedHelper.populateCardsDetailed(
                                                        card, card.getNotifications(), textViewCardNo.getText().toString());
                                            }
                                        });
                                    }
                                }
                            }

                            // there were no cards
                            if (obj.getDetail().getCards() == null || obj.getDetail().getCards().size() == 0) {
                                // might be on different fragment by now so check if still on summary
                                if (frameLayout != null) {
                                    LogCustom.d(TAG_GETSM_DOGETREQUESTUPDATE, "no cards show message");
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            //stuff that updates ui
                                            TextView textViewNoCardsToDisplay = (TextView) frameLayout.findViewById(R.id.textViewNoCardsToDisplay);
                                            textViewNoCardsToDisplay.setVisibility(View.VISIBLE);
                                        }
                                    });

                                }
                            }

                        }
                    }
                    break;

                case "UPDATE-CONTACTLESS":
                    responseBody = response.body().string();
                    LogCustom.d(TAG_GETSM_DOGETREQUESTUPDATECONTACTLESS, "resp raw" + responseBody);

                    showLoadingIconStatusBar(View.GONE, false, -1);
                    if (!response.isSuccessful()) {
                        LogCustom.d(TAG_GETSM_DOGETREQUESTUPDATECONTACTLESS, "server error" + response.code());
                        InternetConnection.showErrorMessage(context, "Server error code " + response.code());
                    } else {
                        ContactlessCards obj = mapper.readValue(responseBody, ContactlessCards.class);

                        if (obj.getError() != null) {

                            LogCustom.d(TAG_GETSM_DOGETREQUESTUPDATECONTACTLESS, "error message from server" + obj.getError());
                            InternetConnection.showErrorMessageFromServer(context, obj.getError());

                            // check if account still active
                            //Generic.checkIfLoginAccountActive(fragmentManager, obj);
                        } else {
                            //DBDeleteQueries.deleteLoginDetailsFromDB(login);
                            DBDeleteQueries.deleteContactlessCardSummaryAndDetailsFromDB();

                            storeContactlessCards(obj);

                            final FrameLayout frameLayout = (FrameLayout) activity.findViewById(R.id.frameLayoutSummaryCardsContainer);
                            final LinearLayout mainLayout = (LinearLayout) activity.findViewById(R.id.linearLayoutSummaryCardsContainer);
                            final LinearLayout mainLayoutDetailed = (LinearLayout) activity.findViewById(R.id.layoutContainer);


                            // might be on different fragment by now so check if still on summary
                            if (mainLayout != null) {
                                LogCustom.d(TAG_GETSM_DOGETREQUESTUPDATECONTACTLESS, "check and still on summary screen 1");
                                // remove any existing cards displayed first
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        //stuff that updates ui
                                        if (((LinearLayout) mainLayout).getChildCount() > 0) {
                                            ((LinearLayout) mainLayout).removeAllViews();
                                        }
                                    }
                                });
                            }

                            // save card summary
                            for (final ActiveCard card : obj.getActiveCards()) {
                                LogCustom.d(TAG_GETSM_DOGETREQUESTUPDATECONTACTLESS, "saving card contactless" + card.getLastFourDigits());
                                card.setNextRefreshTime(getCurrentTime(true));
                                card.setLastRefreshTime(getCurrentTime(false));
                                card.save();

                                // might be on different fragment by now so check if still on summary
                                if (mainLayout != null) {
                                    LogCustom.d(TAG_GETSM_DOGETREQUESTUPDATECONTACTLESS, "check and still on summary screen 2");
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            //stuff that updates ui
                                            cardsSummaryHelper.addCardToView(mainLayout, card);
                                        }
                                    });

                                }
                            }

                            // there were no cards
                            if (obj.getActiveCards() == null || obj.getActiveCards().size() == 0) {
                                // might be on different fragment by now so check if still on summary
                                if (frameLayout != null) {
                                    LogCustom.d(TAG_GETSM_DOGETREQUESTUPDATECONTACTLESS, "no cards show message");
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            //stuff that updates ui
                                            TextView textViewNoCardsToDisplay = (TextView) frameLayout.findViewById(R.id.textViewNoCardsToDisplay);
                                            textViewNoCardsToDisplay.setVisibility(View.VISIBLE);
                                        }
                                    });

                                }
                            }

                        }
                    }

                    showLoadingIconStatusBar(View.GONE, false, -1);
                    break;
            }
        }
        catch (Exception e) {
            InternetConnection.showErrorMessage(context, "An error occurred, please try again later (059)");
            Logging.logError(TAG_GETSM_GENERIC, e);
        }
    }

}
