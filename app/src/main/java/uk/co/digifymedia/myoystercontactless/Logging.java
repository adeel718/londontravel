package uk.co.digifymedia.myoystercontactless;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by adeel on 03/12/2016.
 */

public class Logging {
    public void logEvent(String tag , String logString){
    }

    public void logEventWithData(String tag, HashMap<String, String> data){

        StringBuilder stringBuilder = new StringBuilder();

        for (Map.Entry<String, String> entry : data.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            stringBuilder.append(key + ": " + value);
        }

    }

    public static void logError(String title, Exception e){
        StringWriter errors = new StringWriter();
        e.printStackTrace(new PrintWriter(errors));

    }
}
