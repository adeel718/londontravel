package uk.co.digifymedia.myoystercontactless.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.orm.SugarRecord;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "error",
        "card-number",
        "date",
        "daily-total",
        "journey-detailed"
})
public class JourneyHistory extends SugarRecord {
    @JsonProperty("error")
    private String error;

    @JsonProperty("card-number")
    private String cardNumber;
    @JsonProperty("date")
    private String date;
    @JsonProperty("daily-total")
    private String dailyTotal;
    @JsonProperty("journey-detailed")
    private List<JourneyDetailed> journeyDetailed = null;

    @JsonProperty("card-number")
    public String getCardNumber() {
        return cardNumber;
    }

    @JsonProperty("card-number")
    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    @JsonProperty("date")
    public String getDate() {
        return date;
    }

    @JsonProperty("date")
    public void setDate(String date) {
        this.date = date;
    }

    @JsonProperty("daily-total")
    public String getDailyTotal() {
        return dailyTotal;
    }

    @JsonProperty("daily-total")
    public void setDailyTotal(String dailyTotal) {
        this.dailyTotal = dailyTotal;
    }

    @JsonProperty("journey-detailed")
    public List<JourneyDetailed> getJourneyDetailed() {
        return journeyDetailed;
    }

    @JsonProperty("journey-detailed")
    public void setJourneyDetailed(List<JourneyDetailed> journeyDetailed) {
        this.journeyDetailed = journeyDetailed;
    }

    @JsonProperty("error")
    public String getError() {
        return error;
    }

    @JsonProperty("error")
    public void setError(String error) {
        this.error = error;
    }

}
