
package uk.co.digifymedia.myoystercontactless.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.orm.SugarRecord;

import java.util.List;

import uk.co.digifymedia.myoystercontactless.model.contactless.ActiveCard;


@JsonInclude(JsonInclude.Include.NON_NULL)

@JsonPropertyOrder({
        "id",
        "error",
        "name",
        "cards",
        "detail",
        "notifications_text"
})

public class SummaryCards extends SugarRecord{
    @JsonProperty("loginId")
    private String loginId;
    @JsonProperty("error")
    private String error;
    @JsonProperty("name")
    private String name;
    @JsonProperty("detail")
    private Detail detail;
    @JsonProperty("notifications_text")
    private List<Object> notificationsText = null;

    @JsonProperty("loginId")
    public String getLoginId() {
        return loginId;
    }

    @JsonProperty("loginId")
    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("detail")
    public Detail getDetail() {
        return detail;
    }

    @JsonProperty("detail")
    public void setDetail(Detail detail) {
        this.detail = detail;
    }

    @JsonProperty("notifications_text")
    public List<Object> getNotificationsText() {
        return notificationsText;
    }

    @JsonProperty("notifications_text")
    public void setNotificationsText(List<Object> notificationsText) {
        this.notificationsText = notificationsText;
    }

    @JsonProperty("error")
    public String getError() {
        return error;
    }

    @JsonProperty("error")
    public void setError(String error) {
        this.error = error;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String cardType;

    // Get list of all card contactless summary
    public List<ActiveCard> getContactlessCardsListSummaryFromDB() {
        return ActiveCard.find(ActiveCard.class, "");
    }

    // Get list of all card details
    public List<CardDetailed> getCardsListDetailedFromDB(String oysterOrContactlesss) {
        if(oysterOrContactlesss.equals("oyster")){
            return CardDetailed.find(CardDetailed.class, "CARD_NUMBER NOT LIKE ?", "%x%");
        }else {
            return CardDetailed.find(CardDetailed.class, "CARD_NUMBER LIKE ?", "%x%");
        }
    }

    // Get specific detail for one card
    public List<CardDetailed> getCardDetailsFromDB(String cardNo) {
        return CardDetailed.findWithQuery(CardDetailed.class, "Select * from CARD_DETAILED where CARD_NUMBER = ?", cardNo);
    }




}
