package uk.co.digifymedia.myoystercontactless;


import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import okhttp3.HttpUrl;
import uk.co.digifymedia.R;
import uk.co.digifymedia.myoystercontactless.helpers.Generic;
import uk.co.digifymedia.myoystercontactless.model.Login;

import static uk.co.digifymedia.myoystercontactless.AES.encryptPassword;
import static uk.co.digifymedia.myoystercontactless.Urls.URL_LOGIN;


/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class FragmentLogin extends Fragment {
    private static final String ARG_PARAM_MSG = "message";
    private String mParamMessage;

    public FragmentLogin() {
        // Required empty public constructor
    }


    public static FragmentLogin newInstanceWithMessage(String param1) {
        FragmentLogin fragment = new FragmentLogin();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM_MSG, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParamMessage = getArguments().getString(ARG_PARAM_MSG);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_login, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        TextView title = (TextView) getActivity().findViewById(R.id.textViewTitle);
        title.setText("Login");

        final Generic generic = new Generic(getFragmentManager(), getActivity());

        if(mParamMessage!=null){
            AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
            builder1.setTitle("Your account");
            builder1.setMessage(mParamMessage);
            builder1.setCancelable(true);
            builder1.setNeutralButton(android.R.string.ok,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();
        }

        Login login = new Login();
        try {
            login = Login.findById(Login.class, 1);
        }
        catch(Exception e){
            Logging.logError(this.getClass().getSimpleName() + "/1", e);
            e.printStackTrace();
        }


        EditText editTextEmail = (EditText) getActivity().findViewById(R.id.editTextEmail);
        EditText editTextPassword = (EditText) getActivity().findViewById(R.id.editTextPassword);

        if(login!=null) {
            if (login.getEmail() != null && login.getPassword() != null) {
                editTextEmail.setText(login.getEmail());
                editTextPassword.setText(login.getPassword());
            }
        }

        Button btnSignIn = (Button)getActivity().findViewById(R.id.buttonSignIn);
        Button btnSignUp = (Button)getActivity().findViewById(R.id.buttonSignUp);
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
                InternetConnection connection = new InternetConnection();
                KeyBoardUtil keyBoardUtil = new KeyBoardUtil();
                GetSummaryCards m = new GetSummaryCards(getFragmentManager(), getActivity(), getView(), null);

                Boolean validationFailed = false;

                EditText editTextEmail = (EditText) getActivity().findViewById(R.id.editTextEmail);
                EditText editTextPassword = (EditText) getActivity().findViewById(R.id.editTextPassword);

                if(editTextEmail.getText() == null || editTextEmail.getText().length() <= 0){
                    // validate email
                    Toast.makeText(getActivity(), "Email is required",
                            Toast.LENGTH_LONG).show();
                    validationFailed = true;
                }
                if(editTextPassword.getText() == null || editTextPassword.getText().length() <= 0){
                    // validate password
                    Toast.makeText(getActivity(), "Password is required",
                            Toast.LENGTH_LONG).show();
                    validationFailed = true;
                }

                NavigationView navigationView = (NavigationView) getActivity().findViewById(R.id.nav_view);

                if(!validationFailed) {
                    keyBoardUtil.hideKeyboard(getActivity());
                    HttpUrl.Builder urlBuilder = HttpUrl.parse(URL_LOGIN).newBuilder();
                    String url = urlBuilder.build().toString();

                    Login login = new Login();
                    login.setEmail(editTextEmail.getText().toString());
                    login.setPassword(editTextPassword.getText().toString());

                    try {
                        FrameLayout fl = (FrameLayout) getActivity().findViewById(R.id.frameLayoutLoading);
                        fl.setVisibility(View.VISIBLE);
                        login.setPassword(encryptPassword(editTextPassword.getText().toString()));
                        m.doGetRequest(url, login, "oyster");
                        generic.setNavigationMenuAuthorised(navigationView);
                        navigationView.getMenu().getItem(0).setChecked(true);
                    } catch (IOException e) {
                        e.printStackTrace();
                        Logging.logError(this.getClass().getSimpleName() + "/2", e);
                        connection.showNoConnectionError(getContext(), getActivity());
                    } catch (Exception e) {
                        e.printStackTrace();
                        Logging.logError(this.getClass().getSimpleName() + "/3", e);
                        connection.showNoConnectionError(getContext(), getActivity());
                    }
                }
            }
        });

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
                Intent openURL = new Intent(android.content.Intent.ACTION_VIEW);
                openURL.setData(Uri.parse("https://oyster.tfl.gov.uk/oyster/link/0004.do"));
                startActivity(openURL);
            }
        });
    }

}
