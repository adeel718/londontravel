
package uk.co.digifymedia.myoystercontactless.model.line_status_2;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "$type",
    "category",
    "categoryDescription",
    "description",
    "affectedRoutes",
    "affectedStops",
    "isWholeLine",
    "closureText",
    "additionalInfo",
    "created",
    "isBlocking"
})
public class Disruption {

    /*@JsonProperty("$type")
    private String $type;*/
    @JsonProperty("category")
    private String category;
    @JsonProperty("categoryDescription")
    private String categoryDescription;
    @JsonProperty("description")
    private String description;
    @JsonProperty("affectedRoutes")
    private List<Object> affectedRoutes = null;
    @JsonProperty("affectedStops")
    private List<Object> affectedStops = null;
    @JsonProperty("isWholeLine")
    private Boolean isWholeLine;
    @JsonProperty("closureText")
    private String closureText;
    @JsonProperty("additionalInfo")
    private String additionalInfo;
    @JsonProperty("created")
    private String created;
    @JsonProperty("isBlocking")
    private Boolean isBlocking;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /*@JsonProperty("$type")
    public String get$type() {
        return $type;
    }

    @JsonProperty("$type")
    public void set$type(String $type) {
        this.$type = $type;
    }*/

    @JsonProperty("category")
    public String getCategory() {
        return category;
    }

    @JsonProperty("category")
    public void setCategory(String category) {
        this.category = category;
    }

    @JsonProperty("categoryDescription")
    public String getCategoryDescription() {
        return categoryDescription;
    }

    @JsonProperty("categoryDescription")
    public void setCategoryDescription(String categoryDescription) {
        this.categoryDescription = categoryDescription;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("affectedRoutes")
    public List<Object> getAffectedRoutes() {
        return affectedRoutes;
    }

    @JsonProperty("affectedRoutes")
    public void setAffectedRoutes(List<Object> affectedRoutes) {
        this.affectedRoutes = affectedRoutes;
    }

    @JsonProperty("affectedStops")
    public List<Object> getAffectedStops() {
        return affectedStops;
    }

    @JsonProperty("affectedStops")
    public void setAffectedStops(List<Object> affectedStops) {
        this.affectedStops = affectedStops;
    }

    @JsonProperty("isWholeLine")
    public Boolean getIsWholeLine() {
        return isWholeLine;
    }

    @JsonProperty("isWholeLine")
    public void setIsWholeLine(Boolean isWholeLine) {
        this.isWholeLine = isWholeLine;
    }

    @JsonProperty("closureText")
    public String getClosureText() {
        return closureText;
    }

    @JsonProperty("closureText")
    public void setClosureText(String closureText) {
        this.closureText = closureText;
    }

    @JsonProperty("additionalInfo")
    public String getAdditionalInfo() {
        return additionalInfo;
    }

    @JsonProperty("additionalInfo")
    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    @JsonProperty("created")
    public String getCreated() {
        return created;
    }

    @JsonProperty("created")
    public void setCreated(String created) {
        this.created = created;
    }

    @JsonProperty("isBlocking")
    public Boolean getIsBlocking() {
        return isBlocking;
    }

    @JsonProperty("isBlocking")
    public void setIsBlocking(Boolean isBlocking) {
        this.isBlocking = isBlocking;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
