package uk.co.digifymedia.myoystercontactless.helpers;

import java.util.List;

import uk.co.digifymedia.myoystercontactless.model.JourneyDetailed;
import uk.co.digifymedia.myoystercontactless.model.JourneyHistory;
import uk.co.digifymedia.myoystercontactless.model.Ticket;

/**
 * Created by adeel on 27/12/2016.
 */

public class DBSaveQueries {
    public static void saveCardTicketsToDB(String assocCardNo, List<Ticket> tickets){
        for (Ticket ticket : tickets) {
            LogCustom.d("card tickets", "saving ticket for card: " + assocCardNo);
            ticket.setCardNumber(assocCardNo);
            ticket.save();
        }
    }

    public static void saveJourneyHistoryDetailsToDB(List<JourneyHistory> obj){
        // store journey history
        for (JourneyHistory journeyHistory : obj){
            LogCustom.d("journey history", "saving");
            journeyHistory.setDailyTotal(journeyHistory.getDailyTotal().replace("&#163;", "£"));
            journeyHistory.save();

            // store journey detailed assoc
            List<JourneyDetailed> jdList = journeyHistory.getJourneyDetailed();
            for (JourneyDetailed jd : jdList){
                LogCustom.d("journey detail", "saving");
                jd.setJourneyLinkHistoryId(journeyHistory.getId());
                jd.setJourneyDesc(jd.getJourneyDesc().replace("&amp;", "&"));
                jd.setJourneyBal(jd.getJourneyBal().replace("&#163;", "£"));
                jd.setJourneyCharge(jd.getJourneyCharge().replace("&#163;", "£"));
                jd.save();
            }
        }

    }
}
