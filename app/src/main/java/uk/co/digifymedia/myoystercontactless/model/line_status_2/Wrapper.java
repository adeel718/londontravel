
package uk.co.digifymedia.myoystercontactless.model.line_status_2;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.orm.SugarRecord;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "$type",
    "id",
    "name",
    "modeName",
    "disruptions",
    "created",
    "modified",
    "lineStatuses",
    "routeSections",
    "serviceTypes",
    "crowding"
})
public class Wrapper extends SugarRecord{

    /*@JsonProperty("$type")
        private String $type;*/
    private Boolean weekend;
    @JsonProperty("id")
    private String id;
    private String idLine;

    private String lastUpdate;
    @JsonProperty("name")
    private String name;
    @JsonProperty("modeName")
    private String modeName;
    @JsonProperty("disruptions")
    private List<Object> disruptions = null;
    @JsonProperty("created")
    private String created;
    @JsonProperty("modified")
    private String modified;
    @JsonProperty("lineStatuses")
    private List<LineStatus> lineStatuses = null;
    @JsonProperty("routeSections")
    private List<Object> routeSections = null;
    @JsonProperty("serviceTypes")
    private List<ServiceType> serviceTypes = null;
    @JsonProperty("crowding")
    private Crowding crowding;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /*@JsonProperty("$type")
    public String get$type() {
        return $type;
    }

    @JsonProperty("$type")
    public void set$type(String $type) {
        this.$type = $type;
    }*/

    public Boolean getWeekend() {
        return weekend;
    }

    public void setWeekend(Boolean weekend) {
        this.weekend = weekend;
    }

    public String getIdLine() {
        return idLine;
    }

    public void setIdLine(String idLine) {
        this.idLine = idLine;
    }

    @JsonProperty("id")
    public String getActualId() {
        return id;
    }

    @JsonProperty("id")
    public void setActualId(String id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("modeName")
    public String getModeName() {
        return modeName;
    }

    @JsonProperty("modeName")
    public void setModeName(String modeName) {
        this.modeName = modeName;
    }

    @JsonProperty("disruptions")
    public List<Object> getDisruptions() {
        return disruptions;
    }

    @JsonProperty("disruptions")
    public void setDisruptions(List<Object> disruptions) {
        this.disruptions = disruptions;
    }

    @JsonProperty("created")
    public String getCreated() {
        return created;
    }

    @JsonProperty("created")
    public void setCreated(String created) {
        this.created = created;
    }

    @JsonProperty("modified")
    public String getModified() {
        return modified;
    }

    @JsonProperty("modified")
    public void setModified(String modified) {
        this.modified = modified;
    }

    @JsonProperty("lineStatuses")
    public List<LineStatus> getLineStatuses() {
        return lineStatuses;
    }

    @JsonProperty("lineStatuses")
    public void setLineStatuses(List<LineStatus> lineStatuses) {
        this.lineStatuses = lineStatuses;
    }

    @JsonProperty("routeSections")
    public List<Object> getRouteSections() {
        return routeSections;
    }

    @JsonProperty("routeSections")
    public void setRouteSections(List<Object> routeSections) {
        this.routeSections = routeSections;
    }

    @JsonProperty("serviceTypes")
    public List<ServiceType> getServiceTypes() {
        return serviceTypes;
    }

    @JsonProperty("serviceTypes")
    public void setServiceTypes(List<ServiceType> serviceTypes) {
        this.serviceTypes = serviceTypes;
    }

    @JsonProperty("crowding")
    public Crowding getCrowding() {
        return crowding;
    }

    @JsonProperty("crowding")
    public void setCrowding(Crowding crowding) {
        this.crowding = crowding;
    }

    public static List<LineStatus> getLineStatusList(Boolean weekend) {
        String weekendVal = "0";
        if (weekend) {
            weekendVal = "1";
        }

        List<LineStatus> lineStatusList = LineStatus.findWithQuery(
                LineStatus.class, "Select * from LINE_STATUS where WEEKEND = ? ORDER BY LINKED_LINE_ID ASC", weekendVal);

        return lineStatusList;
    }

    public static List<LineStatus> getLineStatusListFiltered(Boolean weekend, String id_line) {
        String weekendVal = "0";
        if (weekend) {
            weekendVal = "1";
        }

        List<LineStatus> lineStatusList = LineStatus.findWithQuery(
                LineStatus.class, "Select * from LINE_STATUS where WEEKEND = ? and LINKED_LINE_ID = ? ORDER BY LINKED_LINE_ID ASC", weekendVal, id_line);

        return lineStatusList;
    }

    public static List<Wrapper> getWrapperList(Boolean weekend) {
        String weekendVal = "0";
        if (weekend) {
            weekendVal = "1";
        }

        List<Wrapper> lineWrapperList = Wrapper.findWithQuery(
                Wrapper.class, "Select * from WRAPPER where WEEKEND = ?", weekendVal);

        return lineWrapperList;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
