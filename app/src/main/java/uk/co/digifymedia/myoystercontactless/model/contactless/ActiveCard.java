
package uk.co.digifymedia.myoystercontactless.model.contactless;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.orm.SugarRecord;

import java.util.HashMap;
import java.util.Map;

@JsonPropertyOrder({
        "IsCardStatusInError",
        "aboutToExpire",
        "allCardIssues",
        "cardCss",
        "cardDisplayId",
        "cardId",
        "cardStatus",
        "cardStatusCss",
        "cardStatusIcon",
        "formattedCardXxxxName",
        "formattedExpiryDate",
        "incompleteJourneyCount",
        "isActive",
        "lastFourDigits",
        "nickName",
        "paymentCardTypeDescription",
        "shortNotificationTitle",
        "submittedRefunds",
        "travelTokenId"
})
public class ActiveCard extends SugarRecord{

    @JsonProperty("IsCardStatusInError")
    private Boolean isCardStatusInError;
    @JsonProperty("aboutToExpire")
    private Boolean aboutToExpire;
    @JsonProperty("allCardIssues")
    private Object allCardIssues;
    @JsonProperty("cardCss")
    private String cardCss;
    @JsonProperty("cardDisplayId")
    private String cardDisplayId;
    @JsonProperty("cardId")
    private String cardId;
    @JsonProperty("cardStatus")
    private String cardStatus;
    @JsonProperty("cardStatusCss")
    private String cardStatusCss;
    @JsonProperty("cardStatusIcon")
    private String cardStatusIcon;
    @JsonProperty("formattedCardXxxxName")
    private String formattedCardXxxxName;
    @JsonProperty("formattedExpiryDate")
    private String formattedExpiryDate;
    @JsonProperty("incompleteJourneyCount")
    private Integer incompleteJourneyCount;
    @JsonProperty("isActive")
    private Boolean isActive;
    @JsonProperty("lastFourDigits")
    private String lastFourDigits;
    @JsonProperty("nickName")
    private Object nickName;
    @JsonProperty("paymentCardTypeDescription")
    private String paymentCardTypeDescription;
    @JsonProperty("shortNotificationTitle")
    private Object shortNotificationTitle;
    @JsonProperty("submittedRefunds")
    private Object submittedRefunds;
    @JsonProperty("travelTokenId")
    private Integer travelTokenId;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private String lastRefreshTime;
    private String nextRefreshTime;

    @JsonProperty("IsCardStatusInError")
    public Boolean getIsCardStatusInError() {
        return isCardStatusInError;
    }

    @JsonProperty("IsCardStatusInError")
    public void setIsCardStatusInError(Boolean isCardStatusInError) {
        this.isCardStatusInError = isCardStatusInError;
    }

    @JsonProperty("aboutToExpire")
    public Boolean getAboutToExpire() {
        return aboutToExpire;
    }

    @JsonProperty("aboutToExpire")
    public void setAboutToExpire(Boolean aboutToExpire) {
        this.aboutToExpire = aboutToExpire;
    }

    @JsonProperty("allCardIssues")
    public Object getAllCardIssues() {
        return allCardIssues;
    }

    @JsonProperty("allCardIssues")
    public void setAllCardIssues(Object allCardIssues) {
        this.allCardIssues = allCardIssues;
    }

    @JsonProperty("cardCss")
    public String getCardCss() {
        return cardCss;
    }

    @JsonProperty("cardCss")
    public void setCardCss(String cardCss) {
        this.cardCss = cardCss;
    }

    @JsonProperty("cardDisplayId")
    public String getCardDisplayId() {
        return cardDisplayId;
    }

    @JsonProperty("cardDisplayId")
    public void setCardDisplayId(String cardDisplayId) {
        this.cardDisplayId = cardDisplayId;
    }

    @JsonProperty("cardId")
    public String getCardId() {
        return cardId;
    }

    @JsonProperty("cardId")
    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    @JsonProperty("cardStatus")
    public String getCardStatus() {
        return cardStatus;
    }

    @JsonProperty("cardStatus")
    public void setCardStatus(String cardStatus) {
        this.cardStatus = cardStatus;
    }

    @JsonProperty("cardStatusCss")
    public String getCardStatusCss() {
        return cardStatusCss;
    }

    @JsonProperty("cardStatusCss")
    public void setCardStatusCss(String cardStatusCss) {
        this.cardStatusCss = cardStatusCss;
    }

    @JsonProperty("cardStatusIcon")
    public String getCardStatusIcon() {
        return cardStatusIcon;
    }

    @JsonProperty("cardStatusIcon")
    public void setCardStatusIcon(String cardStatusIcon) {
        this.cardStatusIcon = cardStatusIcon;
    }

    @JsonProperty("formattedCardXxxxName")
    public String getFormattedCardXxxxName() {
        return formattedCardXxxxName;
    }

    @JsonProperty("formattedCardXxxxName")
    public void setFormattedCardXxxxName(String formattedCardXxxxName) {
        this.formattedCardXxxxName = formattedCardXxxxName;
    }

    @JsonProperty("formattedExpiryDate")
    public String getFormattedExpiryDate() {
        return formattedExpiryDate;
    }

    @JsonProperty("formattedExpiryDate")
    public void setFormattedExpiryDate(String formattedExpiryDate) {
        this.formattedExpiryDate = formattedExpiryDate;
    }

    @JsonProperty("incompleteJourneyCount")
    public Integer getIncompleteJourneyCount() {
        return incompleteJourneyCount;
    }

    @JsonProperty("incompleteJourneyCount")
    public void setIncompleteJourneyCount(Integer incompleteJourneyCount) {
        this.incompleteJourneyCount = incompleteJourneyCount;
    }

    @JsonProperty("isActive")
    public Boolean getIsActive() {
        return isActive;
    }

    @JsonProperty("isActive")
    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    @JsonProperty("lastFourDigits")
    public String getLastFourDigits() {
        return lastFourDigits;
    }

    @JsonProperty("lastFourDigits")
    public void setLastFourDigits(String lastFourDigits) {
        this.lastFourDigits = lastFourDigits;
    }

    @JsonProperty("nickName")
    public Object getNickName() {
        return nickName;
    }

    @JsonProperty("nickName")
    public void setNickName(Object nickName) {
        this.nickName = nickName;
    }

    @JsonProperty("paymentCardTypeDescription")
    public String getPaymentCardTypeDescription() {
        return paymentCardTypeDescription;
    }

    @JsonProperty("paymentCardTypeDescription")
    public void setPaymentCardTypeDescription(String paymentCardTypeDescription) {
        this.paymentCardTypeDescription = paymentCardTypeDescription;
    }

    @JsonProperty("shortNotificationTitle")
    public Object getShortNotificationTitle() {
        return shortNotificationTitle;
    }

    @JsonProperty("shortNotificationTitle")
    public void setShortNotificationTitle(Object shortNotificationTitle) {
        this.shortNotificationTitle = shortNotificationTitle;
    }

    @JsonProperty("submittedRefunds")
    public Object getSubmittedRefunds() {
        return submittedRefunds;
    }

    @JsonProperty("submittedRefunds")
    public void setSubmittedRefunds(Object submittedRefunds) {
        this.submittedRefunds = submittedRefunds;
    }

    @JsonProperty("travelTokenId")
    public Integer getTravelTokenId() {
        return travelTokenId;
    }

    @JsonProperty("travelTokenId")
    public void setTravelTokenId(Integer travelTokenId) {
        this.travelTokenId = travelTokenId;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public String getLastRefreshTime() {
        return lastRefreshTime;
    }

    public void setLastRefreshTime(String lastRefreshTime) {
        this.lastRefreshTime = lastRefreshTime;
    }

    public String getNextRefreshTime() {
        return nextRefreshTime;
    }

    public void setNextRefreshTime(String nextRefreshTime) {
        this.nextRefreshTime = nextRefreshTime;
    }

}
