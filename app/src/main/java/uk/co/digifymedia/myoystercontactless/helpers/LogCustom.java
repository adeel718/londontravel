package uk.co.digifymedia.myoystercontactless.helpers;

import java.util.HashMap;

import uk.co.digifymedia.myoystercontactless.Logging;

/**
 * Created by adeel.a.ahmed on 18/01/2017.
 */

public final class LogCustom {
    static Logging l = new Logging();
    // Override the Log commands to send logs to splunk
    public static void d(String tag, String msg) {
        l.logEvent(tag, msg);
    }

    public static void d(String tag, String msg, Throwable tr) {
        l.logEvent(tag, msg);
    }

    public static void d(String tag, HashMap<String, String> hm) {
        l.logEventWithData(tag, hm);
    }
}
