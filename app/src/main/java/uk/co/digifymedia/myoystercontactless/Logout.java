package uk.co.digifymedia.myoystercontactless;

import uk.co.digifymedia.myoystercontactless.helpers.DBDeleteQueries;
import uk.co.digifymedia.myoystercontactless.model.Login;

/**
 * Created by adeel on 27/12/2016.
 */

public class Logout {
    public static void signoutAndClearAllDB(){
        DBDeleteQueries.deleteJourneyHistoryDetailsFromDB();
        DBDeleteQueries.deleteOysterCardSummaryAndDetailsFromDB();
        DBDeleteQueries.deleteContactlessCardSummaryAndDetailsFromDB();
        DBDeleteQueries.deleteWidgetDetailsFromDB();
        DBDeleteQueries.deleteLoginDetailsFromDB(new Login());

    }
}
