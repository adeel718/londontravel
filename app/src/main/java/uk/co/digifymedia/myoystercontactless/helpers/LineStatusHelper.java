package uk.co.digifymedia.myoystercontactless.helpers;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import uk.co.digifymedia.myoystercontactless.Logging;
import uk.co.digifymedia.R;
import uk.co.digifymedia.myoystercontactless.model.line_status_2.LineStatus;
import uk.co.digifymedia.myoystercontactless.model.line_status_2.Wrapper;

import static android.view.View.GONE;


/**
 * Created by XPS15-9550 on 25/05/2017.
 */

public class LineStatusHelper {
    public void clearView(final Activity activity, final View view){
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //stuff that updates ui
                LinearLayout viewTab = (LinearLayout) view.findViewById(R.id.lineearLayoutLineStatusContainer);

                if (viewTab != null) {
                    viewTab.removeAllViews();
                }
            }
        });
    }

    public void populateLineStatusList(final List<Wrapper> arrayOfWrapper, final Activity activity, final View view, final Boolean weekend){
        try {
            clearView(activity, view);
            activity.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    //stuff that updates ui
                    LinearLayout viewTab = (LinearLayout) view.findViewById(R.id.lineearLayoutLineStatusContainer);
                    TextView textViewUpdateTime = (TextView) view.findViewById(R.id.textViewLineStatusUpdateTime);

                    View view = null;
                    View viewInner = null;

                    for (Wrapper listWrapper : arrayOfWrapper) {
                        Log.d("populate line status 1", listWrapper.getName());

                        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        view = inflater.inflate(R.layout.row_line_status_item, null);

                        TextView textViewName = (TextView) view.findViewById(R.id.textViewLineStatusName);
                        TextView textViewDesc = (TextView) view.findViewById(R.id.textViewLineStatusDesc);
                        LinearLayout linearLayoutAdDesc = (LinearLayout) view.findViewById(R.id.linearLayoutAdditionalLineInfo);

                        setLineColor(listWrapper.getIdLine(), view, activity);

                        if (textViewName != null && textViewUpdateTime != null) {
                            textViewName.setText(listWrapper.getName());

                            textViewUpdateTime.setText(listWrapper.getLastUpdate());
                        }

                        for (LineStatus lineStatus : Wrapper.getLineStatusListFiltered(weekend, listWrapper.getIdLine())) {
                            Log.d("populate line status 2", lineStatus.getLinkedLineId());

                            textViewDesc.setText(lineStatus.getStatusSeverityDescription());

                            // This is a Good service so don't create additional desc layout
                            if ((lineStatus.getReason() != null) &&
                                    (!lineStatus.getReason().equals(""))) {

                                viewInner = inflater.inflate(R.layout.row_line_status_desc_item, null);
                                TextView textViewAdditonalDesc = (TextView) viewInner.findViewById(R.id.textViewLineStatusAdditionalDesc);

                                textViewAdditonalDesc.setText(lineStatus.getReason());
                                linearLayoutAdDesc.addView(viewInner);
                            }
                        }

                        view.setOnClickListener(linearLayoutAdDescOnClickListener);
                        if (viewTab != null) {
                            viewTab.addView(view);
                        }
                    }
                }
            });
        }
        catch(Exception e){
            Logging.logError("populateLineStatusList exception", e);
        }
    }

    // Create an anonymous implementation of OnClickListener
    private View.OnClickListener linearLayoutAdDescOnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            LinearLayout linearLayoutAdDesc = (LinearLayout) v.findViewById(R.id.linearLayoutAdditionalLineInfo);
            if(linearLayoutAdDesc.getVisibility() == GONE){
                linearLayoutAdDesc.setVisibility(View.VISIBLE);
            }
            else {
                linearLayoutAdDesc.setVisibility(View.GONE);
            }
        }
    };

    private void setLineColor(String idLine, View view, Activity activity) {
        ImageView imageViewLineColor = (ImageView) view.findViewById(R.id.imageViewLineStatus);

        int colorLine = 0;

        switch (idLine) {
            case "bakerloo":
                colorLine = R.color.und_bakerloo;
                break;
            case "central":
                colorLine = R.color.und_central;
                break;
            case "circle":
                colorLine = R.color.und_circle;
                break;
            case "district":
                colorLine = R.color.und_district;
                break;
            case "dlr":
                colorLine = R.color.dlr;
                break;
            case "hammersmith-city":
                colorLine = R.color.und_hammersmithcity;
                break;
            case "jubilee":
                colorLine = R.color.und_jubilee;
                break;
            case "london-overground":
                colorLine = R.color.overground;
                break;
            case "metropolitan":
                colorLine = R.color.und_metropolitan;
                break;
            case "northern":
                colorLine = R.color.und_northern;
                break;
            case "piccadilly":
                colorLine = R.color.und_piccadilly;
                break;
            case "victoria":
                colorLine = R.color.und_victoria;
                break;
            case "waterloo-city":
                colorLine = R.color.und_waterloocity;
                break;
        }

        GradientDrawable shape =  new GradientDrawable();
        shape.setCornerRadius( 16 );
        shape.setColor(ContextCompat.getColor(activity, colorLine));
        imageViewLineColor.setBackground(shape);
    }
}
