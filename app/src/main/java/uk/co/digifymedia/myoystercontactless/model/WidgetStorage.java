package uk.co.digifymedia.myoystercontactless.model;

import com.orm.SugarRecord;

/**
 * Created by adeel on 14/01/2017.
 */

public class WidgetStorage extends SugarRecord{
    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public int getAppWidgetId() {
        return appWidgetId;
    }

    public void setAppWidgetId(int appWidgetId) {
        this.appWidgetId = appWidgetId;
    }

    public String cardNumber;
    public int appWidgetId;
}
