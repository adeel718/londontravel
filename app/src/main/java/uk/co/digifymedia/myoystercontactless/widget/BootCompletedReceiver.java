package uk.co.digifymedia.myoystercontactless.widget;

import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import java.util.List;

import uk.co.digifymedia.R;
import uk.co.digifymedia.myoystercontactless.helpers.LogCustom;
import uk.co.digifymedia.myoystercontactless.helpers.WidgetHelper;
import uk.co.digifymedia.myoystercontactless.model.CardDetailed;
import uk.co.digifymedia.myoystercontactless.model.WidgetStorage;

/**
 * Created by adeel on 14/01/2017.
 */

public class BootCompletedReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        LogCustom.d("boot_broadcast_poc", "starting service...");

        LogCustom.d("widget", "WIDGET Enabled");

        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_oyster_simple);

        AppWidgetManager manager = AppWidgetManager.getInstance(context);
        final int[] appWidgetIds = manager.getAppWidgetIds(new ComponentName(context, WidgetProviderOysterSimple.class));

        // update all widget details
        for (int i = 0; i < appWidgetIds.length; ++i)
        {
            LogCustom.d("oyster widget id 1 ", String.valueOf(appWidgetIds[i]));
            // TODO manage null pointer
            List<WidgetStorage> ws = WidgetStorage.find(WidgetStorage.class, "APP_WIDGET_ID = ?", String.valueOf(appWidgetIds[i]));

            if(ws != null && ws.size() > 0){
                // TODO handle null pointer exceptions
                CardDetailed card = CardDetailed.find(CardDetailed.class, "CARD_NUMBER = ?", ws.get(0).getCardNumber()).get(0);

                // set widget text-views
                remoteViews.setTextViewText(R.id.textViewCardNumber, card.getCardNumber());
                remoteViews.setTextViewText(R.id.textViewBalance, card.getBalance());
                remoteViews.setTextViewText(R.id.textViewLastUpdate, card.getLastRefreshTime());

                // set on click listeners, refresh etc
                WidgetHelper.setIntentAndWidgetOnClickListeners(appWidgetIds[i], card.getCardNumber(), remoteViews, context);

                manager.updateAppWidget(appWidgetIds[i], remoteViews);
            }


        }
    }

}