package uk.co.digifymedia.myoystercontactless;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.IOException;
import java.util.List;

import okhttp3.HttpUrl;
import uk.co.digifymedia.R;
import uk.co.digifymedia.myoystercontactless.helpers.CardsSummaryHelper;
import uk.co.digifymedia.myoystercontactless.model.CardDetailed;
import uk.co.digifymedia.myoystercontactless.model.Login;
import uk.co.digifymedia.myoystercontactless.model.SummaryCards;
import uk.co.digifymedia.myoystercontactless.model.contactless.ActiveCard;

import static uk.co.digifymedia.myoystercontactless.Urls.URL_CONTACTLESS_CARDS;
import static uk.co.digifymedia.myoystercontactless.Urls.URL_OYSTER_CARDS;
import static uk.co.digifymedia.myoystercontactless.helpers.CardsSummaryHelper.sortCards;
import static uk.co.digifymedia.R.menu.card_summary_menu;



/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentCardsSummary#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentCardsSummary extends Fragment {
    private static final String ARG_PARAM_CARD_TYPE = "card_type";
    private String mParamCardType;

    public FragmentCardsSummary() {
        // Required empty public constructor
    }

    public static FragmentCardsSummary newInstance(String paramCardType) {
        FragmentCardsSummary fragment = new FragmentCardsSummary();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM_CARD_TYPE, paramCardType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParamCardType = getArguments().getString(ARG_PARAM_CARD_TYPE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setHasOptionsMenu(true);
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_cards_summary, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        TextView title = (TextView) getActivity().findViewById(R.id.textViewTitle);
        if(mParamCardType.contains("oyster")){
            title.setText("Oyster Cards");
        }
        else {
            title.setText("Contactless Cards");
        }

        LinearLayout mainLayout = (LinearLayout) getActivity().findViewById(R.id.linearLayoutSummaryCardsContainer);

        CardsSummaryHelper cardsSummaryHelper = new CardsSummaryHelper(getFragmentManager(), getActivity());
        SummaryCards sc = new SummaryCards();

        refreshCardsProcess(false, mParamCardType);

        try {
            sc = SummaryCards.first(SummaryCards.class);
        } catch (Exception e) {
            Logging.logError(this.getClass().getSimpleName() + "/1", e);
            e.printStackTrace();
        }

        // add each card to the summary screen
        if(mParamCardType.equals("oyster")) {
            if(sc != null) {
                List<CardDetailed> cardDetailed = sc.getCardsListDetailedFromDB("oyster");
                sortCards(cardDetailed);
                for (int i = 0; i < cardDetailed.size(); i++) {
                    CardDetailed card = cardDetailed.get(i);
                    cardsSummaryHelper.addCardToView(mainLayout, card);
                }
            }
        } else {
            List<ActiveCard> cardActive = sc.getContactlessCardsListSummaryFromDB();
            for (int i = 0; i < cardActive.size(); i++) {
                ActiveCard card = cardActive.get(i);
                cardsSummaryHelper.addCardToView(mainLayout, card);
            }
        }
    }

    private void refreshCardsProcess(Boolean forceRefresh, String oysterOrContactless) {
        final GetSummaryCards getSummaryCards = new GetSummaryCards(getFragmentManager(), getActivity(), getView(), null);
        final InternetConnection connection = new InternetConnection();
        final Login login = new Login().first(Login.class);
        HttpUrl.Builder urlBuilder;

        if(oysterOrContactless.equals("oyster")) {
            urlBuilder = HttpUrl.parse(URL_OYSTER_CARDS).newBuilder();
        } else {
            urlBuilder = HttpUrl.parse(URL_CONTACTLESS_CARDS).newBuilder();
        }
        final String url = urlBuilder.build().toString();
        ProgressBar progressBar = (ProgressBar)getActivity().findViewById(R.id.progressBarLoading);
        FrameLayout frameLayoutLoading = (FrameLayout)getActivity().findViewById(R.id.frameLayoutLoading);

        try {
            if(oysterOrContactless.equals("oyster")) {
                progressBar.setVisibility(View.VISIBLE);
                getSummaryCards.doGetRequestUpdateCards(url, login, forceRefresh);

            } else {
                ActiveCard activeCard = null;
                try{
                    activeCard = ActiveCard.first(ActiveCard.class);
                }
                catch (Exception e){
                    e.printStackTrace();
                    Logging.logError(this.getClass().getSimpleName() + "/1", e);
                }

                if(activeCard==null){
                    frameLayoutLoading.setVisibility(View.VISIBLE);
                    getSummaryCards.doGetRequest(url, login, "contactless");
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                    getSummaryCards.doGetRequestUpdateCardsContactless(url, login, forceRefresh);
                }

            }
        } catch (IOException e) {
            progressBar.setVisibility(View.GONE);
            e.printStackTrace();
            Logging.logError(this.getClass().getSimpleName() + "/2", e);
            connection.showErrorMessage(getContext(), "An error occurred, please try again later (053)");
        } catch (Exception e) {
            progressBar.setVisibility(View.GONE);
            e.printStackTrace();
            Logging.logError(this.getClass().getSimpleName() + "/3", e);
            connection.showErrorMessage(getContext(), "An error occurred, please try again later (054)");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(card_summary_menu, menu);
        super.onCreateOptionsMenu(menu,inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                refreshCardsProcess(true, mParamCardType);
                return true;

        }
        return true;
    }
}
