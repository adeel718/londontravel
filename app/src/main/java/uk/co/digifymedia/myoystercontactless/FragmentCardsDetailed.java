package uk.co.digifymedia.myoystercontactless;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import uk.co.digifymedia.R;
import uk.co.digifymedia.myoystercontactless.helpers.CardsDetailedDetailedHelper;
import uk.co.digifymedia.myoystercontactless.model.CardDetailed;
import uk.co.digifymedia.myoystercontactless.model.SummaryCards;
import uk.co.digifymedia.myoystercontactless.model.Ticket;
import uk.co.digifymedia.myoystercontactless.model.contactless.ActiveCard;

public class FragmentCardsDetailed extends Fragment {
    private static final String ARG_PARAM_CARD_NO = "card_no";
    private static final String ARG_PARAM_CARD_TYPE = "card_type";

    private String mParamCardNo;
    private String mParamCardType;
    private CardDetailed cardDetailed;
    private ActiveCard activeCard;
    private List<String> notificationList;

    public FragmentCardsDetailed() {
        // Required empty public constructor
    }

    public static FragmentCardsDetailed newInstance(String param1,String param2) {
        FragmentCardsDetailed fragment = new FragmentCardsDetailed();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM_CARD_NO, param1);
        args.putString(ARG_PARAM_CARD_TYPE, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParamCardNo = getArguments().getString(ARG_PARAM_CARD_NO);
            mParamCardType = getArguments().getString(ARG_PARAM_CARD_TYPE);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        TextView title = (TextView) getActivity().findViewById(R.id.textViewTitle);
        title.setText(mParamCardNo);

        SummaryCards summaryCards = new SummaryCards();
        Ticket tickets = new Ticket();
        ActiveCard activeCard = new ActiveCard();

        if(mParamCardType.equals("oyster")) {
            summaryCards = summaryCards.first(SummaryCards.class);

            List<CardDetailed> list = summaryCards.getCardDetailsFromDB(mParamCardNo);

            if(list != null && list.size()>0)
            {
                cardDetailed = list.get(0);
                notificationList = cardDetailed.getNotifications();
            }
            else {
                Toast.makeText(getContext(), "Refresh is required first",
                        Toast.LENGTH_LONG).show();
            }
        }
        else {
            activeCard = ActiveCard.find(ActiveCard.class, "CARD_ID = ?", mParamCardNo).get(0);
        }
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_cards_detailed, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        CardsDetailedDetailedHelper cardsDetailedDetailedHelper = new CardsDetailedDetailedHelper(getActivity(), getFragmentManager(), getView());

        if(mParamCardType.equals("oyster")) {
            cardsDetailedDetailedHelper.populateCardsDetailed(cardDetailed, notificationList, mParamCardNo);
        }
        else {
            //CardsDetailedDetailedHelper.populateCardsDetailed(getActivity(), getFragmentManager(), cardDetailed, notificationList, mParamCardNo);
        }
    }



}
