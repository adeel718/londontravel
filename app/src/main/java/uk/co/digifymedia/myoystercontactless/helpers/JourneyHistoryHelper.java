package uk.co.digifymedia.myoystercontactless.helpers;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.apache.commons.collections.IteratorUtils;

import java.io.IOException;
import java.util.List;

import okhttp3.HttpUrl;
import uk.co.digifymedia.myoystercontactless.GetJourneyHistory;
import uk.co.digifymedia.myoystercontactless.InternetConnection;
import uk.co.digifymedia.myoystercontactless.Logging;
import uk.co.digifymedia.R;
import uk.co.digifymedia.myoystercontactless.model.CardDetailed;
import uk.co.digifymedia.myoystercontactless.model.JourneyDetailed;
import uk.co.digifymedia.myoystercontactless.model.JourneyHistory;

import static uk.co.digifymedia.myoystercontactless.Urls.URL_OYSTER_JOURNEYS;

/**
 * Created by adeel on 29/12/2016.
 */

public class JourneyHistoryHelper {
    public void fetchAllJourneyHistoryBg(final Activity activity, final FragmentManager fm){

        final InternetConnection connection = new InternetConnection();
        CardDetailed cardDetailed = new CardDetailed();
        List<CardDetailed> list = IteratorUtils.toList(cardDetailed.findAll(CardDetailed.class));
        for (CardDetailed card : list) {
            fetchJourneyHistory(card.getCardNumber(), null, connection, true, activity, fm);
        }
    }

    public void fetchJourneyHistory(final String mParamCardNo, FrameLayout frameLayout, final InternetConnection connection,
                                    final Boolean processInBackground, final Activity activity, final FragmentManager fm){
        final GetJourneyHistory getJourneyHistory = new GetJourneyHistory();

        HttpUrl.Builder urlBuilder2 = HttpUrl.parse(URL_OYSTER_JOURNEYS).newBuilder();
        final String url2 = urlBuilder2.build().toString();

        try {
            if(!processInBackground) {
                frameLayout.setVisibility(View.VISIBLE);
            }
            getJourneyHistory.setActivity(activity);
            getJourneyHistory.setFragmentManager(fm);
            getJourneyHistory.doGetRequest(url2, activity.getApplicationContext(), mParamCardNo, processInBackground);
        } catch (IOException e) {
            if(!processInBackground) {
                frameLayout.setVisibility(View.GONE);
                connection.showErrorMessage(activity.getApplicationContext(), "An error occurred, please try again later (053)");
            }
            e.printStackTrace();
            Logging.logError(this.getClass().getSimpleName() + "/1", e);

        } catch (Exception e) {
            if(!processInBackground) {
                frameLayout.setVisibility(View.GONE);
                connection.showErrorMessage(activity.getApplicationContext(), "An error occurred, please try again later (054)");
            }
            e.printStackTrace();
            Logging.logError(this.getClass().getSimpleName() + "/2", e);
        }
    }
    public static void populateJourneyHistoryFetchDBData(LayoutInflater layoutInflater, Bundle savedInstanceState, LinearLayout linearLayoutContainerJourneyHist, String mParamCardNo) {
        List<JourneyHistory> listJourneyHist = JourneyHistory.find(JourneyHistory.class,"CARD_NUMBER = ?", mParamCardNo);
        for (JourneyHistory journeyHistory : listJourneyHist){
            View childRowJourneyHistory = layoutInflater.inflate(R.layout.row_journey_hist, null);
            TextView textViewJourneyDailyTotal = (TextView)childRowJourneyHistory.findViewById(R.id.textViewJourneyDailyTotal) ;
            TextView textViewJourneyDate = (TextView)childRowJourneyHistory.findViewById(R.id.textViewJourneyDate) ;
            textViewJourneyDailyTotal.setText(journeyHistory.getDailyTotal());
            textViewJourneyDate.setText(journeyHistory.getDate());

            // populate journey detail for this
            LinearLayout linearLayoutJExtra = (LinearLayout) childRowJourneyHistory.findViewById(R.id.linearLayoutJourneyExtra);

            List<JourneyDetailed> listJourneyDetailed = JourneyDetailed.find(JourneyDetailed.class, "JOURNEY_LINK_HISTORY_ID = ?", journeyHistory.getId().toString());
            for (JourneyDetailed journeyDetail : listJourneyDetailed){
                // If overall journey summary
                if((journeyDetail.getJourneyIsSummary().toLowerCase().equals("true"))
                        || journeyDetail.getJourneyId() == null) {

                    View childRowJourneyDetail = layoutInflater.inflate(R.layout.row_journey_hist_detail, null);
                    final LinearLayout linearLayoutJExtraDrillDwn = (LinearLayout) childRowJourneyDetail.findViewById(R.id.linearLayoutJourneyExtraDrillDown);
                    RelativeLayout rlJourneySummaryContainer = (RelativeLayout) childRowJourneyDetail.findViewById(R.id.relativeLayoutJourneySummaryContainer);
                    TextView textViewJourneyDesc = (TextView) childRowJourneyDetail.findViewById(R.id.textViewJourneyDesc);
                    TextView textViewJourneyAmountCharge = (TextView) childRowJourneyDetail.findViewById(R.id.textViewAmountCharge);
                    TextView textViewJourneyAmountBal = (TextView) childRowJourneyDetail.findViewById(R.id.textViewAmountBalance);
                    TextView textViewJourneyTime = (TextView) childRowJourneyDetail.findViewById(R.id.textViewJourneyTime);
                    final ImageView imgViewChevron = (ImageView) childRowJourneyDetail.findViewById(R.id.imageViewChevron);
                    imgViewChevron.setImageResource(R.drawable.chevron_rotated);
                    final TextView textViewShowDetail = (TextView) childRowJourneyDetail.findViewById(R.id.textViewShowDetail);

                    textViewJourneyDesc.setText(journeyDetail.getJourneyDesc());
                    textViewJourneyAmountCharge.setText(journeyDetail.getJourneyCharge());
                    textViewJourneyAmountBal.setText(journeyDetail.getJourneyBal());
                    textViewJourneyTime.setText(journeyDetail.getJourneyTime());

                    String journeyId="";
                    if(journeyDetail.getJourneyId() != null) {
                        journeyId = journeyDetail.getJourneyId().toString();
                    }
                    // drilldown for this journey
                    List<JourneyDetailed> listJourneyDetailedDrillDwn =
                            JourneyDetailed.find(JourneyDetailed.class, "JOURNEY_LINK_HISTORY_ID = ? and (JOURNEY_ID = ?) and JOURNEY_IS_SUMMARY = ?",
                                    journeyHistory.getId().toString(), journeyId, "false");

                    Boolean drillDownItemAtLeastMoreThan1 = false;
                    for (JourneyDetailed journeyDetailDrillDwn : listJourneyDetailedDrillDwn){
                        drillDownItemAtLeastMoreThan1 = true;
                        View childRowJourneyDetailDrillDwn = layoutInflater.inflate(R.layout.row_journey_hist_detail_drilldown, null);
                        TextView textViewJourneyDrillDownDesc = (TextView) childRowJourneyDetailDrillDwn.findViewById(R.id.textViewJourneyDrillDownDesc);
                        TextView textViewJourneyDrillDownAmountCharge = (TextView) childRowJourneyDetailDrillDwn.findViewById(R.id.textViewDrillDownAmountCharge);
                        TextView textViewJourneyDrillDownAmountBal = (TextView) childRowJourneyDetailDrillDwn.findViewById(R.id.textViewDrillDownAmountBalance);
                        TextView textViewJourneyDrillDownTime = (TextView) childRowJourneyDetailDrillDwn.findViewById(R.id.textViewDrillDownJourneyTime);

                        textViewJourneyDrillDownDesc.setText(journeyDetailDrillDwn.getJourneyDesc());
                        textViewJourneyDrillDownAmountCharge.setText(journeyDetailDrillDwn.getJourneyCharge());

                        if(journeyDetailDrillDwn.getJourneyCharge().contains("+")){
                            textViewJourneyDrillDownAmountCharge.setTextColor(Color.BLUE);
                        }else {
                            textViewJourneyDrillDownAmountCharge.setTextColor(Color.RED);
                        }

                        textViewJourneyDrillDownAmountBal.setText(journeyDetailDrillDwn.getJourneyBal());
                        textViewJourneyDrillDownTime.setText(journeyDetailDrillDwn.getJourneyTime());

                        linearLayoutJExtraDrillDwn.addView(childRowJourneyDetailDrillDwn);
                    }

                    if(!drillDownItemAtLeastMoreThan1) {
                        imgViewChevron.setVisibility(View.INVISIBLE);
                        textViewShowDetail.setVisibility(View.INVISIBLE);
                    }
                    else {
                        rlJourneySummaryContainer.setOnClickListener(new View.OnClickListener() {

                            public void onClick(View v) {
                                int ddVisibilty = linearLayoutJExtraDrillDwn.getVisibility();
                                if (ddVisibilty == View.GONE) {
                                    linearLayoutJExtraDrillDwn.setVisibility(View.VISIBLE);
                                    imgViewChevron.setImageResource(R.drawable.chevron_rotated);
                                    textViewShowDetail.setText("Hide detail");
                                } else {
                                    linearLayoutJExtraDrillDwn.setVisibility(View.GONE);
                                    imgViewChevron.setImageResource(R.drawable.chevron);
                                    textViewShowDetail.setText("Show detail");
                                }
                            }
                        });
                    }

                    linearLayoutJExtra.addView(childRowJourneyDetail);
                }
            }

            linearLayoutContainerJourneyHist.addView(childRowJourneyHistory);
        }
    }
}
