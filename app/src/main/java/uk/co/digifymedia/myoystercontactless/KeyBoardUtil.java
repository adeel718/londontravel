package uk.co.digifymedia.myoystercontactless;

import android.app.Activity;
import android.content.Context;
import android.view.inputmethod.InputMethodManager;

import uk.co.digifymedia.myoystercontactless.helpers.LogCustom;

public class KeyBoardUtil
{
    public static void hideKeyboard(Activity activity)
    {
        try
        {
            InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
        catch (Exception e)
        {
            // Ignore exceptions if any
            LogCustom.d("KeyBoardUtil", e.toString(), e);
        }
    }
}