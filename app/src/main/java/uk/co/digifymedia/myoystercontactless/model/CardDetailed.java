
package uk.co.digifymedia.myoystercontactless.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.orm.SugarRecord;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "autoTopUp",
    "balance",
    "ppvBalance",
    "cardNumber",
    "tickets",
    "notifications",
    "jcp",
    "cardPasswordType",
    "cardPassword",
    "isProtected",
    "isStopped",
    "numberOfActiveCards"
})
public class CardDetailed extends SugarRecord{

    private String nextRefreshTime;

    public String getLastRefreshTime() {
        return lastRefreshTime;
    }

    public void setLastRefreshTime(String lastRefreshTime) {
        this.lastRefreshTime = lastRefreshTime;
    }

    public String getNextRefreshTime() {
        return nextRefreshTime;
    }

    public void setNextRefreshTime(String nextRefreshTime) {
        this.nextRefreshTime = nextRefreshTime;
    }

    private String lastRefreshTime;
    @JsonProperty("autoTopUp")
    private String autoTopUp;
    @JsonProperty("balance")
    private String balance;
    @JsonProperty("ppvBalance")
    private Integer ppvBalance;
    @JsonProperty("cardNumber")
    private String cardNumber;
    @JsonProperty("tickets")
    private List<Ticket> tickets = null;
    @JsonProperty("notifications")
    private List<String> notifications = null;
    @JsonProperty("jcp")
    private Jcp jcp;
    @JsonProperty("cardPasswordType")
    private String cardPasswordType;
    @JsonProperty("cardPassword")
    private String cardPassword;
    @JsonProperty("isProtected")
    private Boolean isProtected;
    @JsonProperty("isStopped")
    private Object isStopped;
    @JsonProperty("numberOfActiveCards")
    private Integer numberOfActiveCards;

    @JsonProperty("autoTopUp")
    public String getAutoTopUp() {
        return autoTopUp;
    }

    @JsonProperty("autoTopUp")
    public void setAutoTopUp(String autoTopUp) {
        this.autoTopUp = autoTopUp;
    }

    @JsonProperty("balance")
    public String getBalance() {
        return balance;
    }

    @JsonProperty("balance")
    public void setBalance(String balance) {
        this.balance = balance;
    }

    @JsonProperty("ppvBalance")
    public Integer getPpvBalance() {
        return ppvBalance;
    }

    @JsonProperty("ppvBalance")
    public void setPpvBalance(Integer ppvBalance) {
        this.ppvBalance = ppvBalance;
    }

    @JsonProperty("cardNumber")
    public String getCardNumber() {
        return cardNumber;
    }

    @JsonProperty("cardNumber")
    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    @JsonProperty("tickets")
    public List<Ticket> getTickets() {
        return tickets;
    }

    @JsonProperty("tickets")
    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }

    @JsonProperty("notifications")
    public List<String> getNotifications() {
        return notifications;
    }

    @JsonProperty("notifications")
    public void setNotifications(List<String> notifications) {
        this.notifications = notifications;
    }

    @JsonProperty("jcp")
    public Jcp getJcp() {
        return jcp;
    }

    @JsonProperty("jcp")
    public void setJcp(Jcp jcp) {
        this.jcp = jcp;
    }

    @JsonProperty("cardPasswordType")
    public String getCardPasswordType() {
        return cardPasswordType;
    }

    @JsonProperty("cardPasswordType")
    public void setCardPasswordType(String cardPasswordType) {
        this.cardPasswordType = cardPasswordType;
    }

    @JsonProperty("cardPassword")
    public String getCardPassword() {
        return cardPassword;
    }

    @JsonProperty("cardPassword")
    public void setCardPassword(String cardPassword) {
        this.cardPassword = cardPassword;
    }

    @JsonProperty("isProtected")
    public Boolean getIsProtected() {
        return isProtected;
    }

    @JsonProperty("isProtected")
    public void setIsProtected(Boolean isProtected) {
        this.isProtected = isProtected;
    }

    @JsonProperty("isStopped")
    public Object getIsStopped() {
        return isStopped;
    }

    @JsonProperty("isStopped")
    public void setIsStopped(Object isStopped) {
        this.isStopped = isStopped;
    }

    @JsonProperty("numberOfActiveCards")
    public Integer getNumberOfActiveCards() {
        return numberOfActiveCards;
    }

    @JsonProperty("numberOfActiveCards")
    public void setNumberOfActiveCards(Integer numberOfActiveCards) {
        this.numberOfActiveCards = numberOfActiveCards;
    }

}
