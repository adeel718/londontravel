package uk.co.digifymedia.myoystercontactless;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import android.util.Base64;

/**
 Aes encryption
 */
public class AES {
    private static String PASSWORD_BASED_KEY = "9B7D2C34A3-6BF890C7#0641E6CECF6@";

    public static String encryptPassword(String password){
        String val = AES.encrypt(password, PASSWORD_BASED_KEY);
        //System.out.println(AES.decrypt(AES.encrypt(data, key), key));
        System.out.println(val);
        return val;
    }

    public static String encrypt(String input, String key){
        byte[] crypted = null;
        try{
            SecretKeySpec skey = new SecretKeySpec(key.getBytes(), "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, skey);
            crypted = cipher.doFinal(input.getBytes());
        }catch(Exception e){
            System.out.println(e.toString());
            Logging.logError("AES.java - 1", e);
        }
        return new String(Base64.encodeToString(crypted, Base64.DEFAULT));
    }

    public static String decrypt(String input, String key){
        byte[] output = null;
        try{
            SecretKeySpec skey = new SecretKeySpec(key.getBytes(), "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, skey);
            output = cipher.doFinal(Base64.decode(input, Base64.DEFAULT));
        }catch(Exception e){
            System.out.println(e.toString());
            Logging.logError("AES.java - 2", e);
        }
        return new String(output);
    }
}