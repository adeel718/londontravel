package uk.co.digifymedia.myoystercontactless.widget;

import android.app.IntentService;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.widget.RemoteViews;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;

import okhttp3.HttpUrl;
import uk.co.digifymedia.myoystercontactless.GetSummaryCards;
import uk.co.digifymedia.myoystercontactless.InternetConnection;
import uk.co.digifymedia.myoystercontactless.Logging;
import uk.co.digifymedia.R;
import uk.co.digifymedia.myoystercontactless.helpers.LogCustom;
import uk.co.digifymedia.myoystercontactless.helpers.WidgetHelper;
import uk.co.digifymedia.myoystercontactless.model.CardDetailed;
import uk.co.digifymedia.myoystercontactless.model.Login;
import uk.co.digifymedia.myoystercontactless.model.SummaryCards;
import uk.co.digifymedia.myoystercontactless.model.WidgetStorage;

import static android.appwidget.AppWidgetManager.EXTRA_APPWIDGET_ID;
import static android.appwidget.AppWidgetManager.EXTRA_APPWIDGET_IDS;
import static android.appwidget.AppWidgetManager.INVALID_APPWIDGET_ID;
import static uk.co.digifymedia.myoystercontactless.Urls.URL_OYSTER_CARDS;

public class WidgetProviderOysterSimple extends AppWidgetProvider{

    public static String TAG_WIDG_SIMPLE = "WIDGET/generic";
    public static String WIDGET_BUTTON_REFRESH = "WIDGET_BUTTON_REFRESH";

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager,
                         int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);
        LogCustom.d(TAG_WIDG_SIMPLE,"onUpdate enter");

        AppWidgetAlarm alarm = new AppWidgetAlarm(context);
        alarm.startAlarm();
    }

    @Override
    public void onEnabled(Context context) {
        LogCustom.d(TAG_WIDG_SIMPLE,"onEnabled enter");
        super.onEnabled(context);
        AppWidgetAlarm alarm = new AppWidgetAlarm(context);
        alarm.startAlarm();
    }

    @Override
    public void onDisabled(Context context) {
        LogCustom.d(TAG_WIDG_SIMPLE,"onDisabled enter");
        AppWidgetAlarm alarm = new AppWidgetAlarm(context);
        alarm.stopAlarm();
        super.onDisabled(context);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);//add this line
        LogCustom.d(TAG_WIDG_SIMPLE,"onReceive enter with action " + intent.getAction());

        if (WIDGET_BUTTON_REFRESH.equals(intent.getAction())) {
            LogCustom.d(TAG_WIDG_SIMPLE,"onReceive action is refresh");
            int incomingAppWidgetId = intent.getIntExtra(EXTRA_APPWIDGET_ID,
                    INVALID_APPWIDGET_ID);

            String cardNumber = intent.getStringExtra("card-no");
            Boolean updateAll = intent.getBooleanExtra("updateAll", false);

            if(cardNumber == null) {
                LogCustom.d(TAG_WIDG_SIMPLE,"onReceive card number null");
                Toast.makeText(context, "Card number is null", Toast.LENGTH_SHORT).show();
                Toast.makeText(context, String.valueOf(incomingAppWidgetId), Toast.LENGTH_SHORT).show();
            }
            else {
                LogCustom.d(TAG_WIDG_SIMPLE,"onReceive refreshing, updateAll is " + updateAll);
                //Toast.makeText(context, "Please wait...", Toast.LENGTH_SHORT).show();
                UpdateWidgetService updateWidgetService = new UpdateWidgetService();
                AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
                updateWidgetService.setContext(context);

                if(updateAll){
                    updateWidgetService.getLatestDataRefresh(appWidgetManager, incomingAppWidgetId, cardNumber);
                }else {
                    updateWidgetService.updateOysterSimpleAppWidget(appWidgetManager, incomingAppWidgetId, null, cardNumber, intent);
                }
            }
        }
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds){
        LogCustom.d(TAG_WIDG_SIMPLE,"onDeleted enter");
        for (int i = 0; i < appWidgetIds.length; ++i)
        {
            int appWidgetId = appWidgetIds[i];
            WidgetStorage.deleteAll(WidgetStorage.class, "APP_WIDGET_ID = ?", String.valueOf(appWidgetId));
            LogCustom.d(TAG_WIDG_SIMPLE,"onDeleted delete widget " + appWidgetId);
        }
    }


    public static class UpdateWidgetService extends IntentService {
        public void setContext(Context context) {
            this.context = context;
        }

        Context context;

        public UpdateWidgetService() {
            // only for debug purpose
            super("UpdateWidgetService");
        }

        @Override
        protected void onHandleIntent(Intent intent) {
            AppWidgetManager appWidgetManager = AppWidgetManager
                    .getInstance(UpdateWidgetService.this);

            context = getApplicationContext();

            int incomingAppWidgetId = intent.getIntExtra(EXTRA_APPWIDGET_ID,
                    INVALID_APPWIDGET_ID);
            int[] incomingAppWidgetIds = intent.getIntArrayExtra(EXTRA_APPWIDGET_IDS);
            String selectedCardNo = intent.getStringExtra("card-no");

            if (incomingAppWidgetId != INVALID_APPWIDGET_ID) {
                try {
                    LogCustom.d(TAG_WIDG_SIMPLE,"onHandleIntent card number is " + selectedCardNo);
                    updateOysterSimpleAppWidget(appWidgetManager, incomingAppWidgetId, incomingAppWidgetIds,  selectedCardNo,
                            intent);
                } catch (NullPointerException e) {
                    Logging.logError(this.getClass().getSimpleName() + "/3", e);
                }

            }

        }

        public void updateOysterSimpleAppWidget(AppWidgetManager appWidgetManager,
                                                int appWidgetId, int[] incomingAppWidgetIds, String selectedCardNo, Intent intent) {
            LogCustom.d(TAG_WIDG_SIMPLE,"updateOysterSimpleAppWidget widgetId " + String.valueOf(appWidgetId));
            LogCustom.d(TAG_WIDG_SIMPLE,"updateOysterSimpleAppWidget card number " + selectedCardNo);

            RemoteViews remoteViews = getLatestDataRefresh(appWidgetManager, appWidgetId, selectedCardNo);

            WidgetHelper.setIntentAndWidgetOnClickListeners(appWidgetId, selectedCardNo, remoteViews, context);

            WidgetStorage ws = new WidgetStorage();
            WidgetStorage.deleteAll(WidgetStorage.class, "CARD_NUMBER = ?", selectedCardNo);
            ws.setCardNumber(selectedCardNo);
            ws.setAppWidgetId(appWidgetId);
            ws.save();

            appWidgetManager.updateAppWidget(appWidgetId, remoteViews);

        }

        @NonNull
        private RemoteViews getLatestDataRefresh(AppWidgetManager appWidgetManager, int appWidgetId, String selectedCardNo) {
            LogCustom.d(TAG_WIDG_SIMPLE,"getLatestDataRefresh enter widgetId" + appWidgetId);

            // there is no fragment or activity so set all params to null
            GetSummaryCards getSummaryCards = new GetSummaryCards(null, null, null, context);
            final InternetConnection connection = new InternetConnection();
            final Login login = new Login().first(Login.class);

            HttpUrl.Builder urlBuilder = HttpUrl.parse(URL_OYSTER_CARDS).newBuilder();
            final String url = urlBuilder.build().toString();

            RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
                    R.layout.widget_oyster_simple);

            remoteViews.setImageViewResource(R.id.actionButtonRefresh, R.drawable.widget_progress_refresh);

            // Get data from cache first to display instantly
            // Check if there is a card number, if not still proceed to update all widgets
            if(!selectedCardNo.equals("")) {
                SummaryCards summaryCards = new SummaryCards();
                CardDetailed cardDetailed = null;
                List<CardDetailed> cardDetailedList = summaryCards.getCardDetailsFromDB(selectedCardNo);
                if(cardDetailedList.size() > 0) {
                    cardDetailed = cardDetailedList.get(0);

                    remoteViews.setTextViewText(R.id.textViewCardNumber, cardDetailed.getCardNumber());
                    remoteViews.setTextViewText(R.id.textViewBalance, cardDetailed.getBalance());
                    remoteViews.setTextViewText(R.id.textViewLastUpdate, cardDetailed.getLastRefreshTime());
                }
                else {
                    remoteViews.setTextViewText(R.id.textViewCardNumber, "Signed out");
                    remoteViews.setTextViewText(R.id.textViewBalance, "Remove widget");
                    remoteViews.setTextViewText(R.id.textViewLastUpdate, "");

                }

            }

            // now make a API call to get latest
            try {
                LogCustom.d(TAG_WIDG_SIMPLE,"getLatestDataRefresh call url" + url);
                getSummaryCards.doGetRequestUpdateCardsWidget(url, login, remoteViews, selectedCardNo, appWidgetManager, appWidgetId, false);
            } catch (IOException e) {
                e.printStackTrace();
                Logging.logError(this.getClass().getSimpleName() + "/1", e);
                remoteViews.setImageViewResource(R.id.actionButtonRefresh, R.drawable.widget_ic_menu_refresh);
                connection.showErrorMessage(context, "An error occurred, please try again later (053w)");
            } catch (Exception e) {
                e.printStackTrace();
                Logging.logError(this.getClass().getSimpleName() + "/2", e);
                remoteViews.setImageViewResource(R.id.actionButtonRefresh, R.drawable.widget_ic_menu_refresh);
                connection.showErrorMessage(context, "An error occurred, please try again later (054w)");
            }
            return remoteViews;
        }

    }
}