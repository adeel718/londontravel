package uk.co.digifymedia.myoystercontactless;


import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import uk.co.digifymedia.R;
import uk.co.digifymedia.myoystercontactless.helpers.DBDeleteQueries;
import uk.co.digifymedia.myoystercontactless.helpers.DBSaveQueries;
import uk.co.digifymedia.myoystercontactless.helpers.LogCustom;
import uk.co.digifymedia.myoystercontactless.model.JourneyHistory;
import uk.co.digifymedia.myoystercontactless.model.Login;

/**
 * Created by adeel on 03/12/2016.
 */

public class GetJourneyHistory {
    OkHttpClient client = new OkHttpClient.Builder()
            .connectTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)

            .build();

    public FragmentManager getFragmentManager() {
        return fragmentManager;
    }

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    FragmentManager fragmentManager;

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    Activity activity;

    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }

    View view;

    public void switchToJourneyHistoryFragment(final String cardNo){
        final Fragment fragment = new FragmentCardsDetailedJourneyHistory().newInstance(cardNo);
        fragmentManager.beginTransaction()
                .replace(R.id.content_main_oyster, fragment)
                .addToBackStack("Main")
                .commit();
    }

    public void doGetRequest(String url, final Context context, final String cardNo, final Boolean doInBg) throws Exception {
        TextView actualCardDisplayed = (TextView) activity.findViewById(R.id.textViewCardNumber);
        showLoadingIconStatusBar(View.VISIBLE);
        Login login = new Login();
        try {
            login = Login.first(Login.class);
        }
        catch(Exception e){
            Logging.logError(this.getClass().getSimpleName() + "/1", e);
            showLoadingIconStatusBar(View.GONE);
            e.printStackTrace();
        }

        RequestBody formBody = new FormBody.Builder()
                .add("id", login.getUniqueLoginId())
                .add("card", cardNo)
                .add("from", "01/05/2017")
                .add("to", "17/06/2017")
                .build();
        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();

        final FrameLayout fl = (FrameLayout) activity.findViewById(R.id.frameLayoutLoading);

        Response response = client.newCall(request).execute();

        // Note: This call is synchronous because each card journey needs to be processed
        // Asynchronous will not work due to backend cookie session data
        if (!response.isSuccessful()) {
            showLoadingIconStatusBar(View.GONE);
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //stuff that updates ui
                    fl.setVisibility(View.GONE);
                }
            });

            if(!doInBg) {
                InternetConnection.showErrorMessage(context,
                        "No internet connection, loading from cache");

                switchToJourneyHistoryFragment(cardNo);
            }
        }
        else {
            String responseBody = response.body().string();
            LogCustom.d("journey history", "resp raw" + responseBody);

            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //stuff that updates ui
                    fl.setVisibility(View.GONE);
                }
            });

            if (!response.isSuccessful()) {
                InternetConnection.showErrorMessage(context, "Server error code " + response.code());
            }

            final String resp = responseBody;

            ObjectMapper mapper;
            List<JourneyHistory> obj = null;

            mapper = new ObjectMapper();
            if (resp.contains("error")) {
                Log.d("journey history", "error found fail");
            } else {
                obj = Arrays.asList(
                        mapper.readValue(resp, JourneyHistory[].class)
                );
            }

            // User is on summary card detail screen
            Button journeyHistBtn = (Button) activity.findViewById(R.id.buttonDetailedJourneyHistory);

            // Empty response means no history
            if (obj == null || obj.size() == 0) {
                if (!doInBg) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //stuff that updates ui
                            Toast.makeText(context, "No journey history found",
                                    Toast.LENGTH_LONG).show();
                        }
                    });
                } else {
                    LogCustom.d("journey history", "resp null do in bg");
                }
            } else {
                // Check if the card number from response matches one that was requested
                if (!doInBg && (!cardNo.trim().equals(obj.get(0).getCardNumber().trim()))) {
                    LogCustom.d("card number not match ", obj.get(0).getCardNumber().trim());
                    InternetConnection.showErrorMessage(context, "Server error - wrong card number");
                }

                DBDeleteQueries.deleteJourneyHistoryDetailsFromDB(cardNo);
                DBSaveQueries.saveJourneyHistoryDetailsToDB(obj);

                // Need to check if we are on correct fragment
                // And There was some data
                // And That the current fragment card number matches

                if (doInBg && (journeyHistBtn != null) && (obj.size() > 0) &&
                        (actualCardDisplayed.getText().equals(obj.get(0).getCardNumber().trim())) ) {
                    enableJourneyHistoryButton(journeyHistBtn);

                }

                if (!doInBg) {
                    switchToJourneyHistoryFragment(cardNo);
                }
            }
            showLoadingIconStatusBar(View.GONE);
        }
    }

    public void enableJourneyHistoryButton(final Button journeyHistBtn) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                journeyHistBtn.setText("Journey History");
                journeyHistBtn.setAlpha((float) 1.0);
                journeyHistBtn.setEnabled(true);
            }
        });
    }

    public void disableJourneyHistoryButton(final Button journeyHistBtn) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                journeyHistBtn.setText("No Journey History");
                journeyHistBtn.setAlpha((float) 0.6);
                journeyHistBtn.setEnabled(false);
            }
        });
    }

    private void showLoadingIconStatusBar(final int visibility) {
        // null check, might be from widget
        if(activity != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    LogCustom.d("journey history", "set progressBarLoading icon to " + visibility);
                    final ProgressBar progressBar = (ProgressBar) activity.findViewById(R.id.progressBarLoading);
                    progressBar.setVisibility(visibility);
                }
            });
        }
    }
}