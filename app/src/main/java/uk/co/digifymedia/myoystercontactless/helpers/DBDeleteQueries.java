package uk.co.digifymedia.myoystercontactless.helpers;

import java.util.List;

import uk.co.digifymedia.myoystercontactless.model.CardDetailed;
import uk.co.digifymedia.myoystercontactless.model.JourneyDetailed;
import uk.co.digifymedia.myoystercontactless.model.JourneyHistory;
import uk.co.digifymedia.myoystercontactless.model.Login;
import uk.co.digifymedia.myoystercontactless.model.SummaryCards;
import uk.co.digifymedia.myoystercontactless.model.Ticket;
import uk.co.digifymedia.myoystercontactless.model.WidgetStorage;
import uk.co.digifymedia.myoystercontactless.model.contactless.ActiveCard;
import uk.co.digifymedia.myoystercontactless.model.contactless.ContactlessCards;
import uk.co.digifymedia.myoystercontactless.model.line_status_2.LineStatus;
import uk.co.digifymedia.myoystercontactless.model.line_status_2.Wrapper;

/**
 * Created by adeel on 25/12/2016.
 */

public class DBDeleteQueries {
    public static void deleteCardTicketsFromDB(){
        LogCustom.d("card tickets", "deleting");
        try {
            Ticket.deleteAll(Ticket.class);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void deleteOysterCardSummaryAndDetailsFromDB() {
        SummaryCards cardsToDelete = new SummaryCards();
        cardsToDelete = cardsToDelete.first(SummaryCards.class);

        try {
            for (CardDetailed card : cardsToDelete.getCardsListDetailedFromDB("oyster")) {
                LogCustom.d("card details", "deleting");
                card.delete();
            }

            cardsToDelete.delete();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void deleteContactlessCardSummaryAndDetailsFromDB() {
        SummaryCards cardsToDelete = new SummaryCards();
        try {
            cardsToDelete = cardsToDelete.find(SummaryCards.class, "CARD_TYPE = ?", "contactless").get(0);
        } catch (Exception e){
            e.printStackTrace();
        }
        try {
            Boolean cd = ContactlessCards.first(ContactlessCards.class).delete();
        } catch (Exception e){
            e.printStackTrace();
        }

        try {
            if (cardsToDelete.getContactlessCardsListSummaryFromDB() != null) {
                for (ActiveCard card : cardsToDelete.getContactlessCardsListSummaryFromDB()) {
                    LogCustom.d("card", "deleting");
                    card.delete();
                }

                for (CardDetailed card : cardsToDelete.getCardsListDetailedFromDB("contactless")) {
                    LogCustom.d("card details", "deleting");
                    card.delete();
                }
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void deleteLoginDetailsFromDB(Login login) {
        // in all case delete login details first
        try {
            Login loginFind = Login.findById(Login.class, 1);
            if(loginFind!=null) {
                loginFind.delete();
                login.delete();
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void deleteJourneyHistoryDetailsFromDB(){
        // delete all journey history
        List<JourneyHistory> journeyHistoryToDelete = JourneyHistory.find(JourneyHistory.class, "");
        for (JourneyHistory journeyHistory : journeyHistoryToDelete){
            LogCustom.d("journey history", "deleting");
            long journeyHistoryId = journeyHistory.getId();
            journeyHistory.delete();

            // find assoc journey detail & delete
            List<JourneyDetailed> jdToDelete = JourneyDetailed.find(JourneyDetailed.class,
                    "JOURNEY_LINK_HISTORY_ID = ?", String.valueOf(journeyHistoryId));
            for (JourneyDetailed jd : jdToDelete){
                LogCustom.d("journey detail", "deleting");
                jd.delete();
            }
        }
    }

    public static void deleteJourneyHistoryDetailsFromDB(String cardNo){
        // delete journey history for card no
        List<JourneyHistory> journeyHistoryToDelete = JourneyHistory.find(JourneyHistory.class, "CARD_NUMBER = ?", cardNo);
        for (JourneyHistory journeyHistory : journeyHistoryToDelete){
            LogCustom.d("journey history", "deleting");
            long journeyHistoryId = journeyHistory.getId();
            journeyHistory.delete();

            // find assoc journey detail & delete
            List<JourneyDetailed> jdToDelete = JourneyDetailed.find(JourneyDetailed.class,
                    "JOURNEY_LINK_HISTORY_ID = ?", String.valueOf(journeyHistoryId));
            for (JourneyDetailed jd : jdToDelete){
                LogCustom.d("journey detail", "deleting");
                jd.delete();
            }
        }
    }

    public static void deleteWidgetDetailsFromDB() {
        // delete all widgets
        try {
            WidgetStorage.deleteAll(WidgetStorage.class);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void deleteInfoWeekDay(){
        LogCustom.d("line info", "deleting");
        try {
            Wrapper.deleteAll(Wrapper.class, "WEEKEND = ?", "0");
            LineStatus.deleteAll(LineStatus.class, "WEEKEND = ?", "0");
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void deleteInfoWeekEnd(){
        LogCustom.d("line info", "deleting");
        try {
            Wrapper.deleteAll(Wrapper.class, "WEEKEND = ?", "1");
            LineStatus.deleteAll(LineStatus.class, "WEEKEND = ?", "1");
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
}
