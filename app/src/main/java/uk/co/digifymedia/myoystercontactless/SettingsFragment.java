package uk.co.digifymedia.myoystercontactless;

import android.os.Bundle;
import android.widget.TextView;

import com.github.machinarius.preferencefragment.PreferenceFragment;

import uk.co.digifymedia.R;

/**
 * Created by XPS15-9550 on 07/05/2017.
 */

public class SettingsFragment extends PreferenceFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TextView title = (TextView) getActivity().findViewById(R.id.textViewTitle);
        title.setText("Settings");
        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.pref_settings);
    }
}
