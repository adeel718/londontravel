
package uk.co.digifymedia.myoystercontactless.model.line_status_2;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.orm.SugarRecord;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "$type",
    "id",
    "statusSeverity",
    "statusSeverityDescription",
    "created",
    "validityPeriods",
    "lineId",
    "reason",
    "disruption"
})
public class LineStatus extends SugarRecord{

    /*@JsonProperty("$type")
        private String $type;*/
    private Boolean weekend;
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("statusSeverity")
    private Integer statusSeverity;
    @JsonProperty("statusSeverityDescription")
    private String statusSeverityDescription;
    @JsonProperty("created")
    private String created;
    @JsonProperty("validityPeriods")
    private List<ValidityPeriod> validityPeriods = null;
    @JsonProperty("lineId")
    private String lineId;
    private String lineName;
    @JsonProperty("reason")
    private String reason;
    @JsonProperty("disruption")
    private Disruption disruption;

    private String linkedLineId;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /*@JsonProperty("$type")
    public String get$type() {
        return $type;
    }

    @JsonProperty("$type")
    public void set$type(String $type) {
        this.$type = $type;
    }*/

    public String getLineName() {
        return lineName;
    }

    public void setLineName(String lineName) {
        this.lineName = lineName;
    }
    public Boolean getWeekend() {
        return weekend;
    }

    public void setWeekend(Boolean weekend) {
        this.weekend = weekend;
    }

    @JsonProperty("id")
    public Integer getActualId() {
        return id;
    }

    @JsonProperty("id")
    public void setActualId(Integer id) {
        this.id = id;
    }

    @JsonProperty("statusSeverity")
    public Integer getStatusSeverity() {
        return statusSeverity;
    }

    @JsonProperty("statusSeverity")
    public void setStatusSeverity(Integer statusSeverity) {
        this.statusSeverity = statusSeverity;
    }

    @JsonProperty("statusSeverityDescription")
    public String getStatusSeverityDescription() {
        return statusSeverityDescription;
    }

    @JsonProperty("statusSeverityDescription")
    public void setStatusSeverityDescription(String statusSeverityDescription) {
        this.statusSeverityDescription = statusSeverityDescription;
    }

    @JsonProperty("created")
    public String getCreated() {
        return created;
    }

    @JsonProperty("created")
    public void setCreated(String created) {
        this.created = created;
    }

    @JsonProperty("validityPeriods")
    public List<ValidityPeriod> getValidityPeriods() {
        return validityPeriods;
    }

    @JsonProperty("validityPeriods")
    public void setValidityPeriods(List<ValidityPeriod> validityPeriods) {
        this.validityPeriods = validityPeriods;
    }

    @JsonProperty("lineId")
    public String getLineId() {
        return lineId;
    }

    @JsonProperty("lineId")
    public void setLineId(String lineId) {
        this.lineId = lineId;
    }

    @JsonProperty("reason")
    public String getReason() {
        return reason;
    }

    @JsonProperty("reason")
    public void setReason(String reason) {
        this.reason = reason;
    }

    @JsonProperty("disruption")
    public Disruption getDisruption() {
        return disruption;
    }

    @JsonProperty("disruption")
    public void setDisruption(Disruption disruption) {
        this.disruption = disruption;
    }

    public String getLinkedLineId() {
        return linkedLineId;
    }

    public void setLinkedLineId(String linkedLineId) {
        this.linkedLineId = linkedLineId;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }


}
