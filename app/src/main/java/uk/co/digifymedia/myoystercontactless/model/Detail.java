
package uk.co.digifymedia.myoystercontactless.model;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "cards",
    "filtering",
    "pagination"
})
public class Detail {

    @JsonProperty("cards")
    private List<CardDetailed> cards = null;
    @JsonProperty("filtering")
    private Boolean filtering;
    @JsonProperty("pagination")
    private Pagination pagination;

    @JsonProperty("cards")
    public List<CardDetailed> getCards() {
        return cards;
    }

    @JsonProperty("cards")
    public void setCards(List<CardDetailed> cards) {
        this.cards = cards;
    }

    @JsonProperty("filtering")
    public Boolean getFiltering() {
        return filtering;
    }

    @JsonProperty("filtering")
    public void setFiltering(Boolean filtering) {
        this.filtering = filtering;
    }

    @JsonProperty("pagination")
    public Pagination getPagination() {
        return pagination;
    }

    @JsonProperty("pagination")
    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

}
