package uk.co.digifymedia.myoystercontactless;

/**
 * Created by adeel on 14/01/2017.
 */

public class Urls extends UrlHolder{

    public static String URL_LOGIN = "http://digifymedia.co.uk/_oyster/login.php";
    public static String URL_OYSTER_CARDS = "http://digifymedia.co.uk/_oyster/oyster-cards.php";
    public static String URL_OYSTER_JOURNEYS = "http://digifymedia.co.uk/_oyster/oyster-journeys.php";
    public static String URL_CONTACTLESS_CARDS = "http://digifymedia.co.uk/_oyster/contactless-cards.php";
    public static String URL_LINE_STATUS = "http://digifymedia.co.uk/_oyster/line-status.php?weekend=false";
    public static String URL_LINE_STATUS_WE = "http://digifymedia.co.uk/_oyster/line-status.php?weekend=true";



    /*public static String URL_LOGIN = "http://digifymedia.co.uk/_oyster/login-stub.php";
    public static String URL_OYSTER_CARDS = "http://digifymedia.co.uk/_oyster/oyster-cards-stub.php";
    public static String URL_OYSTER_JOURNEYS = "http://digifymedia.co.uk/_oyster/oyster-journeys-stub.php";
    public static String URL_CONTACTLESS_CARDS = "http://digifymedia.co.uk/_oyster/contactless-cards-stub.php";*/



}
