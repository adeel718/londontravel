package uk.co.digifymedia.myoystercontactless.widget;

/**
 * Created by adeel on 30/12/2016.
 */

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProviderInfo;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import uk.co.digifymedia.R;
import uk.co.digifymedia.myoystercontactless.helpers.LogCustom;
import uk.co.digifymedia.myoystercontactless.model.CardDetailed;
import uk.co.digifymedia.myoystercontactless.model.SummaryCards;

import static android.appwidget.AppWidgetManager.EXTRA_APPWIDGET_ID;
import static android.appwidget.AppWidgetManager.INVALID_APPWIDGET_ID;

public class ConfigurationActivity extends Activity {

    String selectedCardNo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_widget_config);
        setResult(RESULT_CANCELED);

        initListViews();

        SummaryCards sc = new SummaryCards();
        final LinearLayout linearLayoutSelCards = (LinearLayout)findViewById(R.id.linearLayoutWidgetConfCardContainer);
        LayoutInflater layoutInflater = getLayoutInflater();

        // loop cards and display with checkbox
        for (final CardDetailed card: sc.getCardsListDetailedFromDB("oyster")){
            View childRow = layoutInflater.inflate(R.layout.row_card_select_checkbox, null);
            TextView textViewCardNumber = (TextView) childRow.findViewById(R.id.textViewCardNumber);
            final CheckBox checkBoxCard = (CheckBox) childRow.findViewById(R.id.checkBoxSelectCard);

            textViewCardNumber.setText(card.getCardNumber());

            // set checkbox onclick
            checkBoxCard.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                        // loop through all checkboxes first and disable
                        CheckBox cb;
                        for (int x = 0; x<linearLayoutSelCards.getChildCount();x++){
                            cb = (CheckBox)linearLayoutSelCards.getChildAt(x).findViewById(R.id.checkBoxSelectCard);
                            if(cb.isChecked()){
                                cb.setChecked(false);
                            }
                        }

                        // set the checkbox and set variable
                        if(isChecked){
                            checkBoxCard.setChecked(true);
                            selectedCardNo = card.getCardNumber();
                        }
                    }
                }
            );

            linearLayoutSelCards.addView(childRow);
        }
    }

    public void initListViews() {

        Button okButton = (Button) findViewById(R.id.okButton);
        okButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                handleOkButton();
            }
        });

    }

    private void handleOkButton() {
        showAppWidget();
    }

    int mAppWidgetId;

    private void showAppWidget() {

        mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if(selectedCardNo == null){
            Toast.makeText(getApplicationContext(), "You need to select a card first",
                Toast.LENGTH_LONG).show();
        }
        else if (extras != null) {
            mAppWidgetId = extras.getInt(EXTRA_APPWIDGET_ID,
                    INVALID_APPWIDGET_ID);

            AppWidgetProviderInfo providerInfo = AppWidgetManager.getInstance(
                    getBaseContext()).getAppWidgetInfo(mAppWidgetId);
            String appWidgetLabel = providerInfo.label;

            Intent startService = new Intent(ConfigurationActivity.this,
                    WidgetProviderOysterSimple.UpdateWidgetService.class);
            startService.putExtra(EXTRA_APPWIDGET_ID, mAppWidgetId);
            startService.putExtra("card-no", selectedCardNo);
            startService.setAction("FROM CONFIGURATION ACTIVITY");
            setResult(RESULT_OK, startService);
            startService(startService);

            finish();
        }
        if ((mAppWidgetId == INVALID_APPWIDGET_ID) && selectedCardNo != null) {
            LogCustom.d("widget id invalid", "widget id invalid");
            finish();
        }

    }

}
