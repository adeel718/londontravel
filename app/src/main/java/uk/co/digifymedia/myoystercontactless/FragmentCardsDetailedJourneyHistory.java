package uk.co.digifymedia.myoystercontactless;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import uk.co.digifymedia.R;
import uk.co.digifymedia.myoystercontactless.helpers.JourneyHistoryHelper;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentCardsDetailedJourneyHistory#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentCardsDetailedJourneyHistory extends Fragment {

    private static final String ARG_PARAM_CARD_NO = "card_no";

    private String mParamCardNo;

    public FragmentCardsDetailedJourneyHistory() {
        // Required empty public constructor
    }

    public static FragmentCardsDetailedJourneyHistory newInstance(String param1) {
        FragmentCardsDetailedJourneyHistory fragment = new FragmentCardsDetailedJourneyHistory();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM_CARD_NO, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParamCardNo = getArguments().getString(ARG_PARAM_CARD_NO);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_cards_detailed_journey_history, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        TextView title = (TextView) getActivity().findViewById(R.id.textViewTitle);
        title.setText(mParamCardNo);

        LinearLayout linearLayoutContainerJourneyHist = (LinearLayout) getActivity().findViewById(R.id.linearLayourContainerJourneyHist);
        LayoutInflater layoutInflater = getLayoutInflater(savedInstanceState);
        JourneyHistoryHelper.populateJourneyHistoryFetchDBData(layoutInflater, savedInstanceState, linearLayoutContainerJourneyHist, mParamCardNo);

    }
}
