package uk.co.digifymedia.swagger.client.model;

import com.orm.SugarRecord;

public class Mode extends SugarRecord {

    public Mode(String mid, String mode) {
        this.mid = mid;
        this.mode = mode;
        // Constructor needed for db
    }

    private String mid;
    private String mode;

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }
}
