/*
 * Transport for London Unified API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package uk.co.digifymedia.swagger.client.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import io.swagger.annotations.ApiModelProperty;

/**
 * TflApiPresentationEntitiesPlacePolygon
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-06-12T23:31:24.944+01:00")
public class TflApiPresentationEntitiesPlacePolygon {
  @SerializedName("geoPoints")
  private List<TflApiCommonGeoPoint> geoPoints = null;

  @SerializedName("commonName")
  private String commonName = null;

  public TflApiPresentationEntitiesPlacePolygon geoPoints(List<TflApiCommonGeoPoint> geoPoints) {
    this.geoPoints = geoPoints;
    return this;
  }

  public TflApiPresentationEntitiesPlacePolygon addGeoPointsItem(TflApiCommonGeoPoint geoPointsItem) {
    if (this.geoPoints == null) {
      this.geoPoints = new ArrayList<TflApiCommonGeoPoint>();
    }
    this.geoPoints.add(geoPointsItem);
    return this;
  }

   /**
   * Get geoPoints
   * @return geoPoints
  **/
  @ApiModelProperty(value = "")
  public List<TflApiCommonGeoPoint> getGeoPoints() {
    return geoPoints;
  }

  public void setGeoPoints(List<TflApiCommonGeoPoint> geoPoints) {
    this.geoPoints = geoPoints;
  }

  public TflApiPresentationEntitiesPlacePolygon commonName(String commonName) {
    this.commonName = commonName;
    return this;
  }

   /**
   * Get commonName
   * @return commonName
  **/
  @ApiModelProperty(value = "")
  public String getCommonName() {
    return commonName;
  }

  public void setCommonName(String commonName) {
    this.commonName = commonName;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TflApiPresentationEntitiesPlacePolygon tflApiPresentationEntitiesPlacePolygon = (TflApiPresentationEntitiesPlacePolygon) o;
    return Objects.equals(this.geoPoints, tflApiPresentationEntitiesPlacePolygon.geoPoints) &&
        Objects.equals(this.commonName, tflApiPresentationEntitiesPlacePolygon.commonName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(geoPoints, commonName);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TflApiPresentationEntitiesPlacePolygon {\n");

    sb.append("    geoPoints: ").append(toIndentedString(geoPoints)).append("\n");
    sb.append("    commonName: ").append(toIndentedString(commonName)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

