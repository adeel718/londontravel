/*
 * Transport for London Unified API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package uk.co.digifymedia.swagger.client.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import io.swagger.annotations.ApiModelProperty;

/**
 * TflApiPresentationEntitiesJourneyPlannerPath
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-06-12T23:31:24.944+01:00")
public class TflApiPresentationEntitiesJourneyPlannerPath {
  @SerializedName("lineString")
  private String lineString = null;

  @SerializedName("stopPoints")
  private List<TflApiPresentationEntitiesIdentifier> stopPoints = null;

  @SerializedName("elevation")
  private List<TflApiCommonJourneyPlannerJpElevation> elevation = null;

  public TflApiPresentationEntitiesJourneyPlannerPath lineString(String lineString) {
    this.lineString = lineString;
    return this;
  }

   /**
   * Get lineString
   * @return lineString
  **/
  @ApiModelProperty(value = "")
  public String getLineString() {
    return lineString;
  }

  public void setLineString(String lineString) {
    this.lineString = lineString;
  }

  public TflApiPresentationEntitiesJourneyPlannerPath stopPoints(List<TflApiPresentationEntitiesIdentifier> stopPoints) {
    this.stopPoints = stopPoints;
    return this;
  }

  public TflApiPresentationEntitiesJourneyPlannerPath addStopPointsItem(TflApiPresentationEntitiesIdentifier stopPointsItem) {
    if (this.stopPoints == null) {
      this.stopPoints = new ArrayList<TflApiPresentationEntitiesIdentifier>();
    }
    this.stopPoints.add(stopPointsItem);
    return this;
  }

   /**
   * Get stopPoints
   * @return stopPoints
  **/
  @ApiModelProperty(value = "")
  public List<TflApiPresentationEntitiesIdentifier> getStopPoints() {
    return stopPoints;
  }

  public void setStopPoints(List<TflApiPresentationEntitiesIdentifier> stopPoints) {
    this.stopPoints = stopPoints;
  }

  public TflApiPresentationEntitiesJourneyPlannerPath elevation(List<TflApiCommonJourneyPlannerJpElevation> elevation) {
    this.elevation = elevation;
    return this;
  }

  public TflApiPresentationEntitiesJourneyPlannerPath addElevationItem(TflApiCommonJourneyPlannerJpElevation elevationItem) {
    if (this.elevation == null) {
      this.elevation = new ArrayList<TflApiCommonJourneyPlannerJpElevation>();
    }
    this.elevation.add(elevationItem);
    return this;
  }

   /**
   * Get elevation
   * @return elevation
  **/
  @ApiModelProperty(value = "")
  public List<TflApiCommonJourneyPlannerJpElevation> getElevation() {
    return elevation;
  }

  public void setElevation(List<TflApiCommonJourneyPlannerJpElevation> elevation) {
    this.elevation = elevation;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TflApiPresentationEntitiesJourneyPlannerPath tflApiPresentationEntitiesJourneyPlannerPath = (TflApiPresentationEntitiesJourneyPlannerPath) o;
    return Objects.equals(this.lineString, tflApiPresentationEntitiesJourneyPlannerPath.lineString) &&
        Objects.equals(this.stopPoints, tflApiPresentationEntitiesJourneyPlannerPath.stopPoints) &&
        Objects.equals(this.elevation, tflApiPresentationEntitiesJourneyPlannerPath.elevation);
  }

  @Override
  public int hashCode() {
    return Objects.hash(lineString, stopPoints, elevation);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TflApiPresentationEntitiesJourneyPlannerPath {\n");

    sb.append("    lineString: ").append(toIndentedString(lineString)).append("\n");
    sb.append("    stopPoints: ").append(toIndentedString(stopPoints)).append("\n");
    sb.append("    elevation: ").append(toIndentedString(elevation)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

