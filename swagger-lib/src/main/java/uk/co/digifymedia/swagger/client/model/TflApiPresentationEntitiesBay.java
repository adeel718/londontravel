/*
 * Transport for London Unified API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package uk.co.digifymedia.swagger.client.model;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

import io.swagger.annotations.ApiModelProperty;

/**
 * TflApiPresentationEntitiesBay
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-06-12T23:31:24.944+01:00")
public class TflApiPresentationEntitiesBay {
  @SerializedName("bayType")
  private String bayType = null;

  @SerializedName("bayCount")
  private Integer bayCount = null;

  @SerializedName("free")
  private Integer free = null;

  @SerializedName("occupied")
  private Integer occupied = null;

  public TflApiPresentationEntitiesBay bayType(String bayType) {
    this.bayType = bayType;
    return this;
  }

   /**
   * Get bayType
   * @return bayType
  **/
  @ApiModelProperty(value = "")
  public String getBayType() {
    return bayType;
  }

  public void setBayType(String bayType) {
    this.bayType = bayType;
  }

  public TflApiPresentationEntitiesBay bayCount(Integer bayCount) {
    this.bayCount = bayCount;
    return this;
  }

   /**
   * Get bayCount
   * @return bayCount
  **/
  @ApiModelProperty(value = "")
  public Integer getBayCount() {
    return bayCount;
  }

  public void setBayCount(Integer bayCount) {
    this.bayCount = bayCount;
  }

  public TflApiPresentationEntitiesBay free(Integer free) {
    this.free = free;
    return this;
  }

   /**
   * Get free
   * @return free
  **/
  @ApiModelProperty(value = "")
  public Integer getFree() {
    return free;
  }

  public void setFree(Integer free) {
    this.free = free;
  }

  public TflApiPresentationEntitiesBay occupied(Integer occupied) {
    this.occupied = occupied;
    return this;
  }

   /**
   * Get occupied
   * @return occupied
  **/
  @ApiModelProperty(value = "")
  public Integer getOccupied() {
    return occupied;
  }

  public void setOccupied(Integer occupied) {
    this.occupied = occupied;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TflApiPresentationEntitiesBay tflApiPresentationEntitiesBay = (TflApiPresentationEntitiesBay) o;
    return Objects.equals(this.bayType, tflApiPresentationEntitiesBay.bayType) &&
        Objects.equals(this.bayCount, tflApiPresentationEntitiesBay.bayCount) &&
        Objects.equals(this.free, tflApiPresentationEntitiesBay.free) &&
        Objects.equals(this.occupied, tflApiPresentationEntitiesBay.occupied);
  }

  @Override
  public int hashCode() {
    return Objects.hash(bayType, bayCount, free, occupied);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TflApiPresentationEntitiesBay {\n");

    sb.append("    bayType: ").append(toIndentedString(bayType)).append("\n");
    sb.append("    bayCount: ").append(toIndentedString(bayCount)).append("\n");
    sb.append("    free: ").append(toIndentedString(free)).append("\n");
    sb.append("    occupied: ").append(toIndentedString(occupied)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

