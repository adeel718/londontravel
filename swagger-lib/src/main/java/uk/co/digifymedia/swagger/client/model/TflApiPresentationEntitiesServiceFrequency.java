/*
 * Transport for London Unified API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package uk.co.digifymedia.swagger.client.model;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

import io.swagger.annotations.ApiModelProperty;

/**
 * TflApiPresentationEntitiesServiceFrequency
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-06-12T23:31:24.944+01:00")
public class TflApiPresentationEntitiesServiceFrequency {
  @SerializedName("lowestFrequency")
  private Double lowestFrequency = null;

  @SerializedName("highestFrequency")
  private Double highestFrequency = null;

  public TflApiPresentationEntitiesServiceFrequency lowestFrequency(Double lowestFrequency) {
    this.lowestFrequency = lowestFrequency;
    return this;
  }

   /**
   * Get lowestFrequency
   * @return lowestFrequency
  **/
  @ApiModelProperty(value = "")
  public Double getLowestFrequency() {
    return lowestFrequency;
  }

  public void setLowestFrequency(Double lowestFrequency) {
    this.lowestFrequency = lowestFrequency;
  }

  public TflApiPresentationEntitiesServiceFrequency highestFrequency(Double highestFrequency) {
    this.highestFrequency = highestFrequency;
    return this;
  }

   /**
   * Get highestFrequency
   * @return highestFrequency
  **/
  @ApiModelProperty(value = "")
  public Double getHighestFrequency() {
    return highestFrequency;
  }

  public void setHighestFrequency(Double highestFrequency) {
    this.highestFrequency = highestFrequency;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TflApiPresentationEntitiesServiceFrequency tflApiPresentationEntitiesServiceFrequency = (TflApiPresentationEntitiesServiceFrequency) o;
    return Objects.equals(this.lowestFrequency, tflApiPresentationEntitiesServiceFrequency.lowestFrequency) &&
        Objects.equals(this.highestFrequency, tflApiPresentationEntitiesServiceFrequency.highestFrequency);
  }

  @Override
  public int hashCode() {
    return Objects.hash(lowestFrequency, highestFrequency);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TflApiPresentationEntitiesServiceFrequency {\n");

    sb.append("    lowestFrequency: ").append(toIndentedString(lowestFrequency)).append("\n");
    sb.append("    highestFrequency: ").append(toIndentedString(highestFrequency)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

