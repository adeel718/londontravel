/*
 * Transport for London Unified API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package uk.co.digifymedia.swagger.client.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import io.swagger.annotations.ApiModelProperty;

/**
 * TflApiPresentationEntitiesCrowding
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-06-12T23:31:24.944+01:00")
public class TflApiPresentationEntitiesCrowding {
  @SerializedName("passengerFlows")
  private List<TflApiPresentationEntitiesPassengerFlow> passengerFlows = null;

  @SerializedName("trainLoadings")
  private List<TflApiPresentationEntitiesTrainLoading> trainLoadings = null;

  public TflApiPresentationEntitiesCrowding passengerFlows(List<TflApiPresentationEntitiesPassengerFlow> passengerFlows) {
    this.passengerFlows = passengerFlows;
    return this;
  }

  public TflApiPresentationEntitiesCrowding addPassengerFlowsItem(TflApiPresentationEntitiesPassengerFlow passengerFlowsItem) {
    if (this.passengerFlows == null) {
      this.passengerFlows = new ArrayList<TflApiPresentationEntitiesPassengerFlow>();
    }
    this.passengerFlows.add(passengerFlowsItem);
    return this;
  }

   /**
   * Busiest times at a station (static information)
   * @return passengerFlows
  **/
  @ApiModelProperty(value = "Busiest times at a station (static information)")
  public List<TflApiPresentationEntitiesPassengerFlow> getPassengerFlows() {
    return passengerFlows;
  }

  public void setPassengerFlows(List<TflApiPresentationEntitiesPassengerFlow> passengerFlows) {
    this.passengerFlows = passengerFlows;
  }

  public TflApiPresentationEntitiesCrowding trainLoadings(List<TflApiPresentationEntitiesTrainLoading> trainLoadings) {
    this.trainLoadings = trainLoadings;
    return this;
  }

  public TflApiPresentationEntitiesCrowding addTrainLoadingsItem(TflApiPresentationEntitiesTrainLoading trainLoadingsItem) {
    if (this.trainLoadings == null) {
      this.trainLoadings = new ArrayList<TflApiPresentationEntitiesTrainLoading>();
    }
    this.trainLoadings.add(trainLoadingsItem);
    return this;
  }

   /**
   * Train Loading on a scale 1-6, 1 being \&quot;Very quiet\&quot; and 6 being \&quot;Exceptionally busy\&quot; (static information)
   * @return trainLoadings
  **/
  @ApiModelProperty(value = "Train Loading on a scale 1-6, 1 being \"Very quiet\" and 6 being \"Exceptionally busy\" (static information)")
  public List<TflApiPresentationEntitiesTrainLoading> getTrainLoadings() {
    return trainLoadings;
  }

  public void setTrainLoadings(List<TflApiPresentationEntitiesTrainLoading> trainLoadings) {
    this.trainLoadings = trainLoadings;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TflApiPresentationEntitiesCrowding tflApiPresentationEntitiesCrowding = (TflApiPresentationEntitiesCrowding) o;
    return Objects.equals(this.passengerFlows, tflApiPresentationEntitiesCrowding.passengerFlows) &&
        Objects.equals(this.trainLoadings, tflApiPresentationEntitiesCrowding.trainLoadings);
  }

  @Override
  public int hashCode() {
    return Objects.hash(passengerFlows, trainLoadings);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TflApiPresentationEntitiesCrowding {\n");

    sb.append("    passengerFlows: ").append(toIndentedString(passengerFlows)).append("\n");
    sb.append("    trainLoadings: ").append(toIndentedString(trainLoadings)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

