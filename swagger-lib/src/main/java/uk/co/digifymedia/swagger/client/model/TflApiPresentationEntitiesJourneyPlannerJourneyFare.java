/*
 * Transport for London Unified API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package uk.co.digifymedia.swagger.client.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import io.swagger.annotations.ApiModelProperty;

/**
 * TflApiPresentationEntitiesJourneyPlannerJourneyFare
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-06-12T23:31:24.944+01:00")
public class TflApiPresentationEntitiesJourneyPlannerJourneyFare {
  @SerializedName("totalCost")
  private Integer totalCost = null;

  @SerializedName("fares")
  private List<TflApiPresentationEntitiesJourneyPlannerFare> fares = null;

  @SerializedName("caveats")
  private List<TflApiPresentationEntitiesJourneyPlannerFareCaveat> caveats = null;

  public TflApiPresentationEntitiesJourneyPlannerJourneyFare totalCost(Integer totalCost) {
    this.totalCost = totalCost;
    return this;
  }

   /**
   * Get totalCost
   * @return totalCost
  **/
  @ApiModelProperty(value = "")
  public Integer getTotalCost() {
    return totalCost;
  }

  public void setTotalCost(Integer totalCost) {
    this.totalCost = totalCost;
  }

  public TflApiPresentationEntitiesJourneyPlannerJourneyFare fares(List<TflApiPresentationEntitiesJourneyPlannerFare> fares) {
    this.fares = fares;
    return this;
  }

  public TflApiPresentationEntitiesJourneyPlannerJourneyFare addFaresItem(TflApiPresentationEntitiesJourneyPlannerFare faresItem) {
    if (this.fares == null) {
      this.fares = new ArrayList<TflApiPresentationEntitiesJourneyPlannerFare>();
    }
    this.fares.add(faresItem);
    return this;
  }

   /**
   * Get fares
   * @return fares
  **/
  @ApiModelProperty(value = "")
  public List<TflApiPresentationEntitiesJourneyPlannerFare> getFares() {
    return fares;
  }

  public void setFares(List<TflApiPresentationEntitiesJourneyPlannerFare> fares) {
    this.fares = fares;
  }

  public TflApiPresentationEntitiesJourneyPlannerJourneyFare caveats(List<TflApiPresentationEntitiesJourneyPlannerFareCaveat> caveats) {
    this.caveats = caveats;
    return this;
  }

  public TflApiPresentationEntitiesJourneyPlannerJourneyFare addCaveatsItem(TflApiPresentationEntitiesJourneyPlannerFareCaveat caveatsItem) {
    if (this.caveats == null) {
      this.caveats = new ArrayList<TflApiPresentationEntitiesJourneyPlannerFareCaveat>();
    }
    this.caveats.add(caveatsItem);
    return this;
  }

   /**
   * Get caveats
   * @return caveats
  **/
  @ApiModelProperty(value = "")
  public List<TflApiPresentationEntitiesJourneyPlannerFareCaveat> getCaveats() {
    return caveats;
  }

  public void setCaveats(List<TflApiPresentationEntitiesJourneyPlannerFareCaveat> caveats) {
    this.caveats = caveats;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TflApiPresentationEntitiesJourneyPlannerJourneyFare tflApiPresentationEntitiesJourneyPlannerJourneyFare = (TflApiPresentationEntitiesJourneyPlannerJourneyFare) o;
    return Objects.equals(this.totalCost, tflApiPresentationEntitiesJourneyPlannerJourneyFare.totalCost) &&
        Objects.equals(this.fares, tflApiPresentationEntitiesJourneyPlannerJourneyFare.fares) &&
        Objects.equals(this.caveats, tflApiPresentationEntitiesJourneyPlannerJourneyFare.caveats);
  }

  @Override
  public int hashCode() {
    return Objects.hash(totalCost, fares, caveats);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TflApiPresentationEntitiesJourneyPlannerJourneyFare {\n");

    sb.append("    totalCost: ").append(toIndentedString(totalCost)).append("\n");
    sb.append("    fares: ").append(toIndentedString(fares)).append("\n");
    sb.append("    caveats: ").append(toIndentedString(caveats)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

