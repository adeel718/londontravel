/*
 * Transport for London Unified API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package uk.co.digifymedia.swagger.client.model;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

import io.swagger.annotations.ApiModelProperty;

/**
 * TflApiPresentationEntitiesRedirect
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-06-12T23:31:24.944+01:00")
public class TflApiPresentationEntitiesRedirect {
  @SerializedName("shortUrl")
  private String shortUrl = null;

  @SerializedName("longUrl")
  private String longUrl = null;

  @SerializedName("active")
  private Boolean active = null;

  public TflApiPresentationEntitiesRedirect shortUrl(String shortUrl) {
    this.shortUrl = shortUrl;
    return this;
  }

   /**
   * Get shortUrl
   * @return shortUrl
  **/
  @ApiModelProperty(value = "")
  public String getShortUrl() {
    return shortUrl;
  }

  public void setShortUrl(String shortUrl) {
    this.shortUrl = shortUrl;
  }

  public TflApiPresentationEntitiesRedirect longUrl(String longUrl) {
    this.longUrl = longUrl;
    return this;
  }

   /**
   * Get longUrl
   * @return longUrl
  **/
  @ApiModelProperty(value = "")
  public String getLongUrl() {
    return longUrl;
  }

  public void setLongUrl(String longUrl) {
    this.longUrl = longUrl;
  }

  public TflApiPresentationEntitiesRedirect active(Boolean active) {
    this.active = active;
    return this;
  }

   /**
   * Get active
   * @return active
  **/
  @ApiModelProperty(value = "")
  public Boolean isActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TflApiPresentationEntitiesRedirect tflApiPresentationEntitiesRedirect = (TflApiPresentationEntitiesRedirect) o;
    return Objects.equals(this.shortUrl, tflApiPresentationEntitiesRedirect.shortUrl) &&
        Objects.equals(this.longUrl, tflApiPresentationEntitiesRedirect.longUrl) &&
        Objects.equals(this.active, tflApiPresentationEntitiesRedirect.active);
  }

  @Override
  public int hashCode() {
    return Objects.hash(shortUrl, longUrl, active);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TflApiPresentationEntitiesRedirect {\n");

    sb.append("    shortUrl: ").append(toIndentedString(shortUrl)).append("\n");
    sb.append("    longUrl: ").append(toIndentedString(longUrl)).append("\n");
    sb.append("    active: ").append(toIndentedString(active)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

