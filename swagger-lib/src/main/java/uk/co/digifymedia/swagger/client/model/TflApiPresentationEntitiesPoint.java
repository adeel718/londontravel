/*
 * Transport for London Unified API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package uk.co.digifymedia.swagger.client.model;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Represents a point located at a latitude and longitude using the WGS84 co-ordinate system.
 */
@ApiModel(description = "Represents a point located at a latitude and longitude using the WGS84 co-ordinate system.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-06-12T23:31:24.944+01:00")
public class TflApiPresentationEntitiesPoint {
  @SerializedName("naptanId")
  private String naptanId = null;

  @SerializedName("platformName")
  private String platformName = null;

  @SerializedName("icsCode")
  private String icsCode = null;

  @SerializedName("commonName")
  private String commonName = null;

  @SerializedName("placeType")
  private String placeType = null;

  @SerializedName("lat")
  private Double lat = null;

  @SerializedName("lon")
  private Double lon = null;

  /**
   * Get naptanId
   * @return naptanId
   **/
  @ApiModelProperty(value = "")
  public String getNaptanId() {
    return naptanId;
  }

  public void setNaptanId(String naptanId) {
    this.naptanId = naptanId;
  }

  public TflApiPresentationEntitiesPoint platformName(String platformName) {
    this.platformName = platformName;
    return this;
  }

  /**
   * Get platformName
   * @return platformName
   **/
  @ApiModelProperty(value = "")
  public String getPlatformName() {
    return platformName;
  }

  public void setPlatformName(String platformName) {
    this.platformName = platformName;
  }

  public TflApiPresentationEntitiesPoint icsCode(String icsCode) {
    this.icsCode = icsCode;
    return this;
  }

  /**
   * Get icsCode
   * @return icsCode
   **/
  @ApiModelProperty(value = "")
  public String getIcsCode() {
    return icsCode;
  }

  public void setIcsCode(String icsCode) {
    this.icsCode = icsCode;
  }

  public TflApiPresentationEntitiesPoint commonName(String commonName) {
    this.commonName = commonName;
    return this;
  }

  /**
   * A human readable name.
   * @return commonName
   **/
  @ApiModelProperty(value = "A human readable name.")
  public String getCommonName() {
    return commonName;
  }

  public void setCommonName(String commonName) {
    this.commonName = commonName;
  }

  /**
   * The type of Place. See /Place/Meta/placeTypes for possible values.
   * @return placeType
   **/
  @ApiModelProperty(value = "The type of Place. See /Place/Meta/placeTypes for possible values.")
  public String getPlaceType() {
    return placeType;
  }

  public void setPlaceType(String placeType) {
    this.placeType = placeType;
  }

  public TflApiPresentationEntitiesPoint lat(Double lat) {
    this.lat = lat;
    return this;
  }

   /**
   * WGS84 latitude of the location.
   * @return lat
  **/
  @ApiModelProperty(value = "WGS84 latitude of the location.")
  public Double getLat() {
    return lat;
  }

  public void setLat(Double lat) {
    this.lat = lat;
  }

  public TflApiPresentationEntitiesPoint lon(Double lon) {
    this.lon = lon;
    return this;
  }

   /**
   * WGS84 longitude of the location.
   * @return lon
  **/
  @ApiModelProperty(value = "WGS84 longitude of the location.")
  public Double getLon() {
    return lon;
  }

  public void setLon(Double lon) {
    this.lon = lon;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TflApiPresentationEntitiesPoint tflApiPresentationEntitiesPoint = (TflApiPresentationEntitiesPoint) o;
    return Objects.equals(this.lat, tflApiPresentationEntitiesPoint.lat) &&
        Objects.equals(this.lon, tflApiPresentationEntitiesPoint.lon);
  }

  @Override
  public int hashCode() {
    return Objects.hash(lat, lon);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TflApiPresentationEntitiesPoint {\n");

    sb.append("    lat: ").append(toIndentedString(lat)).append("\n");
    sb.append("    lon: ").append(toIndentedString(lon)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

