/*
 * Transport for London Unified API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package uk.co.digifymedia.swagger.client.model;

import com.google.gson.annotations.SerializedName;

import org.threeten.bp.OffsetDateTime;

import java.util.Objects;

import io.swagger.annotations.ApiModelProperty;

/**
 * TflApiPresentationEntitiesAdditionalProperties
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-06-12T23:31:24.944+01:00")
public class TflApiPresentationEntitiesAdditionalProperties {
  @SerializedName("category")
  private String category = null;

  @SerializedName("key")
  private String key = null;

  @SerializedName("sourceSystemKey")
  private String sourceSystemKey = null;

  @SerializedName("value")
  private String value = null;

  @SerializedName("modified")
  private OffsetDateTime modified = null;

  public TflApiPresentationEntitiesAdditionalProperties category(String category) {
    this.category = category;
    return this;
  }

   /**
   * Get category
   * @return category
  **/
  @ApiModelProperty(value = "")
  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public TflApiPresentationEntitiesAdditionalProperties key(String key) {
    this.key = key;
    return this;
  }

   /**
   * Get key
   * @return key
  **/
  @ApiModelProperty(value = "")
  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public TflApiPresentationEntitiesAdditionalProperties sourceSystemKey(String sourceSystemKey) {
    this.sourceSystemKey = sourceSystemKey;
    return this;
  }

   /**
   * Get sourceSystemKey
   * @return sourceSystemKey
  **/
  @ApiModelProperty(value = "")
  public String getSourceSystemKey() {
    return sourceSystemKey;
  }

  public void setSourceSystemKey(String sourceSystemKey) {
    this.sourceSystemKey = sourceSystemKey;
  }

  public TflApiPresentationEntitiesAdditionalProperties value(String value) {
    this.value = value;
    return this;
  }

   /**
   * Get value
   * @return value
  **/
  @ApiModelProperty(value = "")
  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public TflApiPresentationEntitiesAdditionalProperties modified(OffsetDateTime modified) {
    this.modified = modified;
    return this;
  }

   /**
   * Get modified
   * @return modified
  **/
  @ApiModelProperty(value = "")
  public OffsetDateTime getModified() {
    return modified;
  }

  public void setModified(OffsetDateTime modified) {
    this.modified = modified;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TflApiPresentationEntitiesAdditionalProperties tflApiPresentationEntitiesAdditionalProperties = (TflApiPresentationEntitiesAdditionalProperties) o;
    return Objects.equals(this.category, tflApiPresentationEntitiesAdditionalProperties.category) &&
        Objects.equals(this.key, tflApiPresentationEntitiesAdditionalProperties.key) &&
        Objects.equals(this.sourceSystemKey, tflApiPresentationEntitiesAdditionalProperties.sourceSystemKey) &&
        Objects.equals(this.value, tflApiPresentationEntitiesAdditionalProperties.value) &&
        Objects.equals(this.modified, tflApiPresentationEntitiesAdditionalProperties.modified);
  }

  @Override
  public int hashCode() {
    return Objects.hash(category, key, sourceSystemKey, value, modified);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TflApiPresentationEntitiesAdditionalProperties {\n");

    sb.append("    category: ").append(toIndentedString(category)).append("\n");
    sb.append("    key: ").append(toIndentedString(key)).append("\n");
    sb.append("    sourceSystemKey: ").append(toIndentedString(sourceSystemKey)).append("\n");
    sb.append("    value: ").append(toIndentedString(value)).append("\n");
    sb.append("    modified: ").append(toIndentedString(modified)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

