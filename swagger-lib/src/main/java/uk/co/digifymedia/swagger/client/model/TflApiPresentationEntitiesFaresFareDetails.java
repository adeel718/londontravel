/*
 * Transport for London Unified API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package uk.co.digifymedia.swagger.client.model;

import com.google.gson.annotations.SerializedName;

import org.threeten.bp.OffsetDateTime;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import io.swagger.annotations.ApiModelProperty;

/**
 * TflApiPresentationEntitiesFaresFareDetails
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-06-12T23:31:24.944+01:00")
public class TflApiPresentationEntitiesFaresFareDetails {
  @SerializedName("boundsId")
  private Integer boundsId = null;

  @SerializedName("startDate")
  private OffsetDateTime startDate = null;

  @SerializedName("endDate")
  private OffsetDateTime endDate = null;

  @SerializedName("mode")
  private String mode = null;

  @SerializedName("passengerType")
  private String passengerType = null;

  @SerializedName("from")
  private String from = null;

  @SerializedName("to")
  private String to = null;

  @SerializedName("fromStation")
  private String fromStation = null;

  @SerializedName("toStation")
  private String toStation = null;

  @SerializedName("via")
  private String via = null;

  @SerializedName("routeCode")
  private String routeCode = null;

  @SerializedName("displayName")
  private String displayName = null;

  @SerializedName("displayOrder")
  private Integer displayOrder = null;

  @SerializedName("routeDescription")
  private String routeDescription = null;

  @SerializedName("validatorInformation")
  private String validatorInformation = null;

  @SerializedName("operator")
  private String operator = null;

  @SerializedName("specialFare")
  private Boolean specialFare = null;

  @SerializedName("throughFare")
  private Boolean throughFare = null;

  @SerializedName("isTour")
  private Boolean isTour = null;

  @SerializedName("ticketsAvailable")
  private List<TflApiPresentationEntitiesFaresTicket> ticketsAvailable = null;

  @SerializedName("messages")
  private List<TflApiPresentationEntitiesMessage> messages = null;

  public TflApiPresentationEntitiesFaresFareDetails boundsId(Integer boundsId) {
    this.boundsId = boundsId;
    return this;
  }

   /**
   * Get boundsId
   * @return boundsId
  **/
  @ApiModelProperty(value = "")
  public Integer getBoundsId() {
    return boundsId;
  }

  public void setBoundsId(Integer boundsId) {
    this.boundsId = boundsId;
  }

  public TflApiPresentationEntitiesFaresFareDetails startDate(OffsetDateTime startDate) {
    this.startDate = startDate;
    return this;
  }

   /**
   * Get startDate
   * @return startDate
  **/
  @ApiModelProperty(value = "")
  public OffsetDateTime getStartDate() {
    return startDate;
  }

  public void setStartDate(OffsetDateTime startDate) {
    this.startDate = startDate;
  }

  public TflApiPresentationEntitiesFaresFareDetails endDate(OffsetDateTime endDate) {
    this.endDate = endDate;
    return this;
  }

   /**
   * Get endDate
   * @return endDate
  **/
  @ApiModelProperty(value = "")
  public OffsetDateTime getEndDate() {
    return endDate;
  }

  public void setEndDate(OffsetDateTime endDate) {
    this.endDate = endDate;
  }

  public TflApiPresentationEntitiesFaresFareDetails mode(String mode) {
    this.mode = mode;
    return this;
  }

   /**
   * Get mode
   * @return mode
  **/
  @ApiModelProperty(value = "")
  public String getMode() {
    return mode;
  }

  public void setMode(String mode) {
    this.mode = mode;
  }

  public TflApiPresentationEntitiesFaresFareDetails passengerType(String passengerType) {
    this.passengerType = passengerType;
    return this;
  }

   /**
   * Get passengerType
   * @return passengerType
  **/
  @ApiModelProperty(value = "")
  public String getPassengerType() {
    return passengerType;
  }

  public void setPassengerType(String passengerType) {
    this.passengerType = passengerType;
  }

  public TflApiPresentationEntitiesFaresFareDetails from(String from) {
    this.from = from;
    return this;
  }

   /**
   * Get from
   * @return from
  **/
  @ApiModelProperty(value = "")
  public String getFrom() {
    return from;
  }

  public void setFrom(String from) {
    this.from = from;
  }

  public TflApiPresentationEntitiesFaresFareDetails to(String to) {
    this.to = to;
    return this;
  }

   /**
   * Get to
   * @return to
  **/
  @ApiModelProperty(value = "")
  public String getTo() {
    return to;
  }

  public void setTo(String to) {
    this.to = to;
  }

  public TflApiPresentationEntitiesFaresFareDetails fromStation(String fromStation) {
    this.fromStation = fromStation;
    return this;
  }

   /**
   * Get fromStation
   * @return fromStation
  **/
  @ApiModelProperty(value = "")
  public String getFromStation() {
    return fromStation;
  }

  public void setFromStation(String fromStation) {
    this.fromStation = fromStation;
  }

  public TflApiPresentationEntitiesFaresFareDetails toStation(String toStation) {
    this.toStation = toStation;
    return this;
  }

   /**
   * Get toStation
   * @return toStation
  **/
  @ApiModelProperty(value = "")
  public String getToStation() {
    return toStation;
  }

  public void setToStation(String toStation) {
    this.toStation = toStation;
  }

  public TflApiPresentationEntitiesFaresFareDetails via(String via) {
    this.via = via;
    return this;
  }

   /**
   * Get via
   * @return via
  **/
  @ApiModelProperty(value = "")
  public String getVia() {
    return via;
  }

  public void setVia(String via) {
    this.via = via;
  }

  public TflApiPresentationEntitiesFaresFareDetails routeCode(String routeCode) {
    this.routeCode = routeCode;
    return this;
  }

   /**
   * Get routeCode
   * @return routeCode
  **/
  @ApiModelProperty(value = "")
  public String getRouteCode() {
    return routeCode;
  }

  public void setRouteCode(String routeCode) {
    this.routeCode = routeCode;
  }

  public TflApiPresentationEntitiesFaresFareDetails displayName(String displayName) {
    this.displayName = displayName;
    return this;
  }

   /**
   * Get displayName
   * @return displayName
  **/
  @ApiModelProperty(value = "")
  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }

  public TflApiPresentationEntitiesFaresFareDetails displayOrder(Integer displayOrder) {
    this.displayOrder = displayOrder;
    return this;
  }

   /**
   * Get displayOrder
   * @return displayOrder
  **/
  @ApiModelProperty(value = "")
  public Integer getDisplayOrder() {
    return displayOrder;
  }

  public void setDisplayOrder(Integer displayOrder) {
    this.displayOrder = displayOrder;
  }

  public TflApiPresentationEntitiesFaresFareDetails routeDescription(String routeDescription) {
    this.routeDescription = routeDescription;
    return this;
  }

   /**
   * Get routeDescription
   * @return routeDescription
  **/
  @ApiModelProperty(value = "")
  public String getRouteDescription() {
    return routeDescription;
  }

  public void setRouteDescription(String routeDescription) {
    this.routeDescription = routeDescription;
  }

  public TflApiPresentationEntitiesFaresFareDetails validatorInformation(String validatorInformation) {
    this.validatorInformation = validatorInformation;
    return this;
  }

   /**
   * Get validatorInformation
   * @return validatorInformation
  **/
  @ApiModelProperty(value = "")
  public String getValidatorInformation() {
    return validatorInformation;
  }

  public void setValidatorInformation(String validatorInformation) {
    this.validatorInformation = validatorInformation;
  }

  public TflApiPresentationEntitiesFaresFareDetails operator(String operator) {
    this.operator = operator;
    return this;
  }

   /**
   * Get operator
   * @return operator
  **/
  @ApiModelProperty(value = "")
  public String getOperator() {
    return operator;
  }

  public void setOperator(String operator) {
    this.operator = operator;
  }

  public TflApiPresentationEntitiesFaresFareDetails specialFare(Boolean specialFare) {
    this.specialFare = specialFare;
    return this;
  }

   /**
   * Get specialFare
   * @return specialFare
  **/
  @ApiModelProperty(value = "")
  public Boolean isSpecialFare() {
    return specialFare;
  }

  public void setSpecialFare(Boolean specialFare) {
    this.specialFare = specialFare;
  }

  public TflApiPresentationEntitiesFaresFareDetails throughFare(Boolean throughFare) {
    this.throughFare = throughFare;
    return this;
  }

   /**
   * Get throughFare
   * @return throughFare
  **/
  @ApiModelProperty(value = "")
  public Boolean isThroughFare() {
    return throughFare;
  }

  public void setThroughFare(Boolean throughFare) {
    this.throughFare = throughFare;
  }

  public TflApiPresentationEntitiesFaresFareDetails isTour(Boolean isTour) {
    this.isTour = isTour;
    return this;
  }

   /**
   * Get isTour
   * @return isTour
  **/
  @ApiModelProperty(value = "")
  public Boolean isIsTour() {
    return isTour;
  }

  public void setIsTour(Boolean isTour) {
    this.isTour = isTour;
  }

  public TflApiPresentationEntitiesFaresFareDetails ticketsAvailable(List<TflApiPresentationEntitiesFaresTicket> ticketsAvailable) {
    this.ticketsAvailable = ticketsAvailable;
    return this;
  }

  public TflApiPresentationEntitiesFaresFareDetails addTicketsAvailableItem(TflApiPresentationEntitiesFaresTicket ticketsAvailableItem) {
    if (this.ticketsAvailable == null) {
      this.ticketsAvailable = new ArrayList<TflApiPresentationEntitiesFaresTicket>();
    }
    this.ticketsAvailable.add(ticketsAvailableItem);
    return this;
  }

   /**
   * Get ticketsAvailable
   * @return ticketsAvailable
  **/
  @ApiModelProperty(value = "")
  public List<TflApiPresentationEntitiesFaresTicket> getTicketsAvailable() {
    return ticketsAvailable;
  }

  public void setTicketsAvailable(List<TflApiPresentationEntitiesFaresTicket> ticketsAvailable) {
    this.ticketsAvailable = ticketsAvailable;
  }

  public TflApiPresentationEntitiesFaresFareDetails messages(List<TflApiPresentationEntitiesMessage> messages) {
    this.messages = messages;
    return this;
  }

  public TflApiPresentationEntitiesFaresFareDetails addMessagesItem(TflApiPresentationEntitiesMessage messagesItem) {
    if (this.messages == null) {
      this.messages = new ArrayList<TflApiPresentationEntitiesMessage>();
    }
    this.messages.add(messagesItem);
    return this;
  }

   /**
   * Get messages
   * @return messages
  **/
  @ApiModelProperty(value = "")
  public List<TflApiPresentationEntitiesMessage> getMessages() {
    return messages;
  }

  public void setMessages(List<TflApiPresentationEntitiesMessage> messages) {
    this.messages = messages;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TflApiPresentationEntitiesFaresFareDetails tflApiPresentationEntitiesFaresFareDetails = (TflApiPresentationEntitiesFaresFareDetails) o;
    return Objects.equals(this.boundsId, tflApiPresentationEntitiesFaresFareDetails.boundsId) &&
        Objects.equals(this.startDate, tflApiPresentationEntitiesFaresFareDetails.startDate) &&
        Objects.equals(this.endDate, tflApiPresentationEntitiesFaresFareDetails.endDate) &&
        Objects.equals(this.mode, tflApiPresentationEntitiesFaresFareDetails.mode) &&
        Objects.equals(this.passengerType, tflApiPresentationEntitiesFaresFareDetails.passengerType) &&
        Objects.equals(this.from, tflApiPresentationEntitiesFaresFareDetails.from) &&
        Objects.equals(this.to, tflApiPresentationEntitiesFaresFareDetails.to) &&
        Objects.equals(this.fromStation, tflApiPresentationEntitiesFaresFareDetails.fromStation) &&
        Objects.equals(this.toStation, tflApiPresentationEntitiesFaresFareDetails.toStation) &&
        Objects.equals(this.via, tflApiPresentationEntitiesFaresFareDetails.via) &&
        Objects.equals(this.routeCode, tflApiPresentationEntitiesFaresFareDetails.routeCode) &&
        Objects.equals(this.displayName, tflApiPresentationEntitiesFaresFareDetails.displayName) &&
        Objects.equals(this.displayOrder, tflApiPresentationEntitiesFaresFareDetails.displayOrder) &&
        Objects.equals(this.routeDescription, tflApiPresentationEntitiesFaresFareDetails.routeDescription) &&
        Objects.equals(this.validatorInformation, tflApiPresentationEntitiesFaresFareDetails.validatorInformation) &&
        Objects.equals(this.operator, tflApiPresentationEntitiesFaresFareDetails.operator) &&
        Objects.equals(this.specialFare, tflApiPresentationEntitiesFaresFareDetails.specialFare) &&
        Objects.equals(this.throughFare, tflApiPresentationEntitiesFaresFareDetails.throughFare) &&
        Objects.equals(this.isTour, tflApiPresentationEntitiesFaresFareDetails.isTour) &&
        Objects.equals(this.ticketsAvailable, tflApiPresentationEntitiesFaresFareDetails.ticketsAvailable) &&
        Objects.equals(this.messages, tflApiPresentationEntitiesFaresFareDetails.messages);
  }

  @Override
  public int hashCode() {
    return Objects.hash(boundsId, startDate, endDate, mode, passengerType, from, to, fromStation, toStation, via, routeCode, displayName, displayOrder, routeDescription, validatorInformation, operator, specialFare, throughFare, isTour, ticketsAvailable, messages);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TflApiPresentationEntitiesFaresFareDetails {\n");

    sb.append("    boundsId: ").append(toIndentedString(boundsId)).append("\n");
    sb.append("    startDate: ").append(toIndentedString(startDate)).append("\n");
    sb.append("    endDate: ").append(toIndentedString(endDate)).append("\n");
    sb.append("    mode: ").append(toIndentedString(mode)).append("\n");
    sb.append("    passengerType: ").append(toIndentedString(passengerType)).append("\n");
    sb.append("    from: ").append(toIndentedString(from)).append("\n");
    sb.append("    to: ").append(toIndentedString(to)).append("\n");
    sb.append("    fromStation: ").append(toIndentedString(fromStation)).append("\n");
    sb.append("    toStation: ").append(toIndentedString(toStation)).append("\n");
    sb.append("    via: ").append(toIndentedString(via)).append("\n");
    sb.append("    routeCode: ").append(toIndentedString(routeCode)).append("\n");
    sb.append("    displayName: ").append(toIndentedString(displayName)).append("\n");
    sb.append("    displayOrder: ").append(toIndentedString(displayOrder)).append("\n");
    sb.append("    routeDescription: ").append(toIndentedString(routeDescription)).append("\n");
    sb.append("    validatorInformation: ").append(toIndentedString(validatorInformation)).append("\n");
    sb.append("    operator: ").append(toIndentedString(operator)).append("\n");
    sb.append("    specialFare: ").append(toIndentedString(specialFare)).append("\n");
    sb.append("    throughFare: ").append(toIndentedString(throughFare)).append("\n");
    sb.append("    isTour: ").append(toIndentedString(isTour)).append("\n");
    sb.append("    ticketsAvailable: ").append(toIndentedString(ticketsAvailable)).append("\n");
    sb.append("    messages: ").append(toIndentedString(messages)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

