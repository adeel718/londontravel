/*
 * Transport for London Unified API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package uk.co.digifymedia.swagger.client.model;

import com.google.gson.annotations.SerializedName;

import org.threeten.bp.OffsetDateTime;

import java.util.Objects;

import io.swagger.annotations.ApiModelProperty;

/**
 * TflApiPresentationEntitiesCycleSuperhighway
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-06-12T23:31:24.944+01:00")
public class TflApiPresentationEntitiesCycleSuperhighway {
  @SerializedName("id")
  private String id = null;

  @SerializedName("label")
  private String label = null;

  @SerializedName("labelShort")
  private String labelShort = null;

  @SerializedName("geography")
  private SystemDataSpatialDbGeography geography = null;

  @SerializedName("segmented")
  private Boolean segmented = null;

  @SerializedName("modified")
  private OffsetDateTime modified = null;

  public TflApiPresentationEntitiesCycleSuperhighway id(String id) {
    this.id = id;
    return this;
  }

   /**
   * The Id
   * @return id
  **/
  @ApiModelProperty(value = "The Id")
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public TflApiPresentationEntitiesCycleSuperhighway label(String label) {
    this.label = label;
    return this;
  }

   /**
   * The long label to show on maps when zoomed in
   * @return label
  **/
  @ApiModelProperty(value = "The long label to show on maps when zoomed in")
  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public TflApiPresentationEntitiesCycleSuperhighway labelShort(String labelShort) {
    this.labelShort = labelShort;
    return this;
  }

   /**
   * The short label to show on maps
   * @return labelShort
  **/
  @ApiModelProperty(value = "The short label to show on maps")
  public String getLabelShort() {
    return labelShort;
  }

  public void setLabelShort(String labelShort) {
    this.labelShort = labelShort;
  }

  public TflApiPresentationEntitiesCycleSuperhighway geography(SystemDataSpatialDbGeography geography) {
    this.geography = geography;
    return this;
  }

   /**
   * A LineString or MultiLineString that forms the route of the highway
   * @return geography
  **/
  @ApiModelProperty(value = "A LineString or MultiLineString that forms the route of the highway")
  public SystemDataSpatialDbGeography getGeography() {
    return geography;
  }

  public void setGeography(SystemDataSpatialDbGeography geography) {
    this.geography = geography;
  }

  public TflApiPresentationEntitiesCycleSuperhighway segmented(Boolean segmented) {
    this.segmented = segmented;
    return this;
  }

   /**
   * True if the route is split into segments
   * @return segmented
  **/
  @ApiModelProperty(value = "True if the route is split into segments")
  public Boolean isSegmented() {
    return segmented;
  }

  public void setSegmented(Boolean segmented) {
    this.segmented = segmented;
  }

  public TflApiPresentationEntitiesCycleSuperhighway modified(OffsetDateTime modified) {
    this.modified = modified;
    return this;
  }

   /**
   * When the data was last updated
   * @return modified
  **/
  @ApiModelProperty(value = "When the data was last updated")
  public OffsetDateTime getModified() {
    return modified;
  }

  public void setModified(OffsetDateTime modified) {
    this.modified = modified;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TflApiPresentationEntitiesCycleSuperhighway tflApiPresentationEntitiesCycleSuperhighway = (TflApiPresentationEntitiesCycleSuperhighway) o;
    return Objects.equals(this.id, tflApiPresentationEntitiesCycleSuperhighway.id) &&
        Objects.equals(this.label, tflApiPresentationEntitiesCycleSuperhighway.label) &&
        Objects.equals(this.labelShort, tflApiPresentationEntitiesCycleSuperhighway.labelShort) &&
        Objects.equals(this.geography, tflApiPresentationEntitiesCycleSuperhighway.geography) &&
        Objects.equals(this.segmented, tflApiPresentationEntitiesCycleSuperhighway.segmented) &&
        Objects.equals(this.modified, tflApiPresentationEntitiesCycleSuperhighway.modified);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, label, labelShort, geography, segmented, modified);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TflApiPresentationEntitiesCycleSuperhighway {\n");

    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    label: ").append(toIndentedString(label)).append("\n");
    sb.append("    labelShort: ").append(toIndentedString(labelShort)).append("\n");
    sb.append("    geography: ").append(toIndentedString(geography)).append("\n");
    sb.append("    segmented: ").append(toIndentedString(segmented)).append("\n");
    sb.append("    modified: ").append(toIndentedString(modified)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

